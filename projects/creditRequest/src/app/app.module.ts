import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { MaterializeModule } from 'angular2-materialize';

import 'materialize-css';
import { ClientComponent } from './components/client/client.component';
import { CreditIpotecarComponent } from './components/credit-ipotecar/credit-ipotecar.component';
import { CreditListComponent } from './components/credit-list/credit-list.component';
import { ServiceClient } from './services/client.service';
import { ServiceCredit } from './services/credit.service';

@NgModule({
  declarations: [
    AppComponent,
    ClientComponent,
    CreditIpotecarComponent,
    CreditListComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    JsonpModule,
    ReactiveFormsModule,
    MaterializeModule
  ],
  providers: [
    ServiceClient,
    ServiceCredit

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

@NgModule({})
export class App4SharedModule{
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModule,
      providers: [

      ]
    };
  }
}
