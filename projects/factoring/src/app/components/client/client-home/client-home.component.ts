import { Component, OnInit } from '@angular/core';
import { ServiceLogin } from './../../../services/login.service';
import { UserModel } from './../../../models/user.model';
import { CompanyModel } from './../../../models/companies.model';
import { ServiceCompanies } from './../../../services/companies.service';
import { InvoiceModel } from './../../../models/invoice.model';

@Component({
  selector: 'app-client-home',
  templateUrl: './client-home.component.html',
  styleUrls: ['./client-home.component.scss']
})
export class ClientHomeComponent implements OnInit {
  user: UserModel;
  company: CompanyModel;
  image: string;
  invoices: InvoiceModel[];
  pendingNo: Number;
  financedNo: Number;
  refusedNo: Number;
  reimbursedNo: Number;
  size: Number;
  rangeValue: any;
  appTitle: String;
  logoImage: String='https://uipath509.westeurope.cloudapp.azure.com/api/v1/getLogo?logoName=';

  constructor(
    private srvLogin: ServiceLogin,
    private srvCompany: ServiceCompanies) { }

  ngOnInit() {

    this.user = this.srvLogin.getUser();
    this.srvCompany.getCompanyById(this.user.companyId)
      .then((result: CompanyModel) => {

        this.srvCompany.setComapny(result);
        this.company = this.srvCompany.getCompany();
        this.image = '../../../../assets/images/factoring/' + this.company.companyLogo;
        this.appTitle = 'Customer Factoring App';
      });
  }

  getLoadedInvoices(lists: InvoiceModel[]) {
    this.invoices = lists;
    this.pendingNo = this.invoices.filter(res => res.status === 'Pending').length;
    this.financedNo = this.invoices.filter(res => res.status === 'Financed').length;
    this.refusedNo = this.invoices.filter(res => res.status === 'Refused').length;
    this.reimbursedNo = this.invoices.filter(res => res.status === 'Reimbursed').length;

    for (let i = 0; i < this.invoices.length; i++) {
      if (this.invoices[i].status === 'Financed') {
        this.rangeValue = +this.invoices[i].financedAmount;
      }
    }

    this.rangeValue = this.rangeValue / parseInt(this.company.companyLimit) * 100;
  }


  disconnectUser() {
    this.srvLogin.disconnectUser();
  }

}
