import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { HttpParams } from "@angular/common/http";
import { CreditModel } from "../models/credit.model";


@Injectable()
export class ServiceCredit {
  url = 'https://uipath509.westeurope.cloudapp.azure.com/api/v1/credit_list/';
  credits: CreditModel[] = null;

  constructor(private http: HttpClient) { }

  // getCreditsList() {
  //   if (this.credits){

  //     return this.credits;
  //   } else {
  //     this.getCreditListDB().then(result => {

  //       this.credits = result;
  //       return this.credits;
  //     });
  //   }
  // }


  public setCreditStorage(credits) {
    this.credits = credits;
    localStorage.setItem("credits", JSON.stringify(this.credits));
  }

  public getCreditsStorage() {
    if (this.credits) {
      return this.credits;
    } else {
      let credits : CreditModel[] =JSON.parse(localStorage.getItem("credits"));
      return credits;
    }
  }

  getCreditsList() {
    this.getCreditsStorage();
    console.log(this.credits)
    return this.http
      .get(this.url)
      .toPromise()
      .then((resp:CreditModel[]) => {
        if (this.credits) {
          return this.credits;
        }
        this.credits = resp;
        return this.credits;
      })
      .catch(this.handleError);
  }

  // deleteCredit(credit) {
  //   const params = new HttpParams().set("_id", credit._id);
  //   return this.http
  //     .delete(this.url, { params })
  //     .toPromise()
  //     .then(resp => {
  //       return resp;
  //     })
  //     .catch(this.handleError);
  // }

    public deleteCreditLocal(credit: CreditModel){
      const idx = this.credits.findIndex(x => x.creditId === credit.creditId);
      this.credits.splice(idx, 1);
      this.setCreditStorage(this.credits);
    }

  // public uploadCredit(credit: CreditModel) {
  //   return this.http.post(this.url, {
  //     creditId: credit.creditId,
  //     clientName: credit.clientName,
  //     income: credit.income,
  //     personsNo: credit.personsNo,
  //     incomeType: credit.incomeType,
  //     taxes: credit.taxes,
  //     period: credit.period,
  //     totalSum: credit.totalSum,
  //     status: credit.status,
  //     analysisFee: credit.analysisFee,
  //     currencyCode: credit.currencyCode,
  //     insuranceRate: credit.insuranceRate,
  //     dae: credit.dae,
  //     monthlyPayment: credit.monthlyPayment,
  //     otherFees: credit.otherFees,
  //     totalPaymentAmount: credit.totalPaymentAmount,
  //     totalReceivedAmount: credit.totalReceivedAmount,
  //   }).toPromise()
  //     .then(resp => {
  //       return resp;
  //     })
  //     .catch(this.handleError);
  // }

  public uploadCreditLocal(credit){
    this.getCreditsList().then(
      resp=> {
        this.credits.push(credit);
        this.setCreditStorage(this.credits);
      }
    );
  }

  public updateCreditLocal(credit: CreditModel){
    const idx = this.credits.findIndex(x => x.creditId === credit.creditId);
    this.credits[idx] = credit;
  }

  // public updateCredit(credit: CreditModel) {
  //   console.log(credit)
  //   const params = new HttpParams().set("id", credit._id);
  //   return this.http.put(this.url,
  //     {
  //       creditId: credit.creditId,
  //       clientName: credit.clientName,
  //       income: credit.income,
  //       personsNo: credit.personsNo,
  //       incomeType: credit.incomeType,
  //       taxes: credit.taxes,
  //       period: credit.period,
  //       totalSum: credit.totalSum,
  //       status: credit.status,
  //       analysisFee: credit.analysisFee,
  //       currencyCode: credit.currencyCode,
  //       insuranceRate: credit.insuranceRate,
  //       dae: credit.dae,
  //       monthlyPayment: credit.monthlyPayment,
  //       otherFees: credit.otherFees,
  //       totalPaymentAmount: credit.totalPaymentAmount,
  //       totalReceivedAmount: credit.totalReceivedAmount,
  //     }, { params }
  //   ).toPromise()
  //     .then(resp => {
  //       return resp;
  //     })
  //     .catch(this.handleError);
  // }


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
