import { Component, OnInit, EventEmitter } from '@angular/core';
import { ServiceClient } from './../../services/client.service';
import { ClientModel } from './../../models/client.model';
import { MaterializeAction } from 'angular2-materialize';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CreditModel } from './../../models/credit.model';
import { ServiceCredit } from './../../services/credit.service';

@Component({
  selector: 'app-credit-ipotecar',
  templateUrl: './credit-ipotecar.component.html',
  styleUrls: ['./credit-ipotecar.component.scss']
})
export class CreditIpotecarComponent implements OnInit {
  searchStr = '';
  client: ClientModel;

  creditRequest: CreditModel = new CreditModel('','', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');

  showApproval: Boolean = false;
  check1: Boolean = false;
  check2: Boolean = false;
  check3: Boolean = false;
  showError = false;
  modalActionsUpdate = new EventEmitter<string | MaterializeAction>();
  modalActionsCredit = new EventEmitter<string | MaterializeAction>();
  public inputForm: FormGroup;
  public creditForm: FormGroup;
  showCredit = false;

  selectOptions: String[] = [
    'Retired',
    'Determined period employment',
    'Undetermined period employment'
  ];

  constructor(
    private srvClient: ServiceClient,
    private srvCredit: ServiceCredit) { }

  ngOnInit() {

    this.inputForm = new FormGroup({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null, Validators.required),
      city: new FormControl(null, Validators.required),
      address: new FormControl(null, Validators.required),
      phone: new FormControl(null, Validators.required),
      email: new FormControl(null, Validators.required),
      serialNo: new FormControl(null, Validators.required),
      validity: new FormControl(null, Validators.required),
      taxIdNo: new FormControl(null, Validators.required)
    });
    this.creditRequest.creditId=(Math.floor(Math.random() * Math.floor(5000))).toString();

    this.creditForm = new FormGroup({
      creditId: new FormControl(null, Validators.required),
      income: new FormControl(null, Validators.required),
      personsNo: new FormControl(null, Validators.required),
      currencyCode: new FormControl(null, Validators.required),
      incomeType: new FormControl(null, Validators.required),
      taxes: new FormControl(null, Validators.required),
      period: new FormControl(null, Validators.required),
      totalSum: new FormControl(null, Validators.required)
    });
  }

  searchClient() {
    this.showApproval = false;
    this.srvClient.getClient(this.searchStr).subscribe((res: ClientModel) => {
      this.client = res[0];
      if (this.client) {
        this.srvClient.setClient(this.client);
        this.showError = false;
        this.check1 = false;
        this.check2 = false;
        this.check3 = false;
      } else {
        this.showError = true;
        this.check1 = false;
        this.check2 = false;
        this.check3 = false;
        this.searchStr = '';
      }
    });
  }

  onSubmit() {
    this.srvClient.setClient(
      new ClientModel(
        this.client._id,
        this.inputForm.controls['lastName'].value,
        this.inputForm.controls['firstName'].value,
        this.inputForm.controls['city'].value,
        this.inputForm.controls['address'].value,
        this.inputForm.controls['phone'].value,
        this.inputForm.controls['email'].value,
        this.inputForm.controls['taxIdNo'].value,
        this.inputForm.controls['serialNo'].value,
        this.inputForm.controls['validity'].value,
        this.client.accountRon,
        this.client.accountEur,
        this.client.maximumAllowed
      )
    );

    this.client = this.srvClient.getSavedClient();
    this.closeModalUpdate();
  }

  submitNewCreditRequest() {
    this.showCredit = true;
    this.creditRequest.clientName = this.client.lastName;
    this.creditRequest.creditId = this.creditForm.controls['creditId'].value;
    this.creditRequest.income = this.creditForm.controls['income'].value;
    this.creditRequest.personsNo = this.creditForm.controls['personsNo'].value;
    this.creditRequest.incomeType = this.creditForm.controls['incomeType'].value;
    this.creditRequest.currencyCode = this.creditForm.controls['currencyCode'].value;
    this.creditRequest.taxes = this.creditForm.controls['taxes'].value;
    this.creditRequest.period = this.creditForm.controls['period'].value;
    this.creditRequest.totalSum = this.creditForm.controls['totalSum'].value;
    this.creditRequest.status = 'Pending';

    // this.srvCredit.uploadCredit(this.creditRequest);
    this.srvCredit.uploadCreditLocal(this.creditRequest);
    this.closeModalCredit();
  }

  requestCredit() {
    this.openModalUpdate();
  }

  creditRequst() {
    this.showApproval = true;
  }

  openModalCredit() {
    this.modalActionsCredit.emit({ action: 'modal', params: ['open'] });
  }

  closeModalCredit() {
    this.modalActionsCredit.emit({ action: 'modal', params: ['close'] });
  }

  openModalUpdate() {
    this.modalActionsUpdate.emit({ action: 'modal', params: ['open'] });
  }

  closeModalUpdate() {
    this.modalActionsUpdate.emit({ action: 'modal', params: ['close'] });
  }

  printGDPR() {
    window.print();
  }
}
