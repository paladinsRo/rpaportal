export class CreditOfferModel {

  analysisFee: string;
  insuranceRate: string;
  dae: string;
  otherFees: string;

  constructor(
    analysisFee: string,
    insuranceRate: string,
    dae: string,
    otherFees: string
  ) {

    this.analysisFee = analysisFee;
    this.insuranceRate = insuranceRate;
    this.dae = dae;
    this.otherFees = otherFees;
  }
}
