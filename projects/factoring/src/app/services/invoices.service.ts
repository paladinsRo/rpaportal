import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Jsonp } from '@angular/http';

import { map } from 'rxjs/operators';
import { CompanyModel } from '../models/companies.model';
import { InvoiceModel } from '../models/invoice.model';
import { Observable } from 'rxjs';

@Injectable()
export class ServiceInvoices {
  url = 'https://uipath509.westeurope.cloudapp.azure.com:443/api/v1/invoices_list';


  constructor(private http: HttpClient, private _jsonp: Jsonp) { }

  public getInvoicesByCompany(companyName) {
    const params = new HttpParams()
      .set("companyName", companyName);
    return this.http
      .get(this.url, { params })
      .toPromise()
      .then(resp => {
        return resp;
      })
      .catch(this.handleError);
  }


  public getAllInvoices() {
    return this.http
      .get(this.url+"s")
      .toPromise()
      .then(resp => {
        return resp;
      })
      .catch(this.handleError);
  }

  public deleteInvoice(invoice: InvoiceModel){
    const params = new HttpParams()
      .set("_id", invoice._id);
    return this.http
      .delete(this.url, { params })
      .toPromise()
      .then(resp => {
        return resp;
      })
      .catch(this.handleError);
  }

  public postInvoiceByCompany(invoice: InvoiceModel) {
    return this.http.post(this.url, {
      _id: invoice._id,
      companyId: invoice.companyId,
      companyName: invoice.companyName,
      sellerName: invoice.sellerName,
      buyerName: invoice.buyerName,
      requestDate: invoice.requestDate,
      invoiceDate: invoice.invoiceDate,
      invoiceNo: invoice.invoiceNo,
      invoiceAmount: invoice.invoiceAmount,
      financedAmount: invoice.financedAmount,
      status: invoice.status,
      buyerRisk: invoice.buyerRisk,
      duplicity: invoice.duplicity,
      creditBureau: invoice.creditBureau,
      comment: invoice.comment,
      invoiceFile: invoice.invoiceFile,
    }).toPromise()
      .then(resp => {
        return resp;
      })
      .catch(this.handleError);

  }

  postFile(fileToUpload: File) {
    const endpoint = 'https://uipath509.westeurope.cloudapp.azure.com:443/api/v1/uploadFile';
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);

    return this.http
      .post(endpoint, formData)
      .toPromise()
      .then(resp => {
      });
}

updateInvoice(invoice:InvoiceModel){
  const params = new HttpParams().set("_id", invoice._id);
  return this.http.put(this.url,
    {
      companyId : invoice.companyId,
      companyName : invoice.companyName,
      sellerName : invoice.sellerName,
      buyerName : invoice.buyerName,
      requestDate : invoice.requestDate,
      invoiceDate : invoice.invoiceDate,
      invoiceNo : invoice.invoiceNo,
      invoiceAmount : invoice.invoiceAmount,
      financedAmount : invoice.financedAmount,
      status : invoice.status,
      duplicity: invoice.duplicity,
      buyerRisk: invoice.buyerRisk,
      creditBureau: invoice.creditBureau,
      comment : invoice.comment,
      invoiceFile : invoice.invoiceFile,
    }, { params }
  ).toPromise()
    .then(resp => {
      return resp;
    })
    .catch(this.handleError);
}


  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }
}
