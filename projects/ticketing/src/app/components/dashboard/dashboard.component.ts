import { Component, OnInit } from '@angular/core';
import { TicketModel } from '../../models/ticket.model';
import { TicketService } from '../../services/ticket.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private ticketService: TicketService) { }
  tickets: TicketModel[];

  ngOnInit() {
    this.tickets=this.ticketService.getAllTickets();
    
  }

}
