import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';


import { App3SharedModule } from '../../projects/factoring/src/app/app.module';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { App4SharedModule } from '../../projects/creditRequest/src/app/app.module';
import { App5SharedModule } from '../../projects/ticketing/src/app/app.module';

import { MaterializeModule } from 'angular2-materialize';



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterializeModule,
    App3SharedModule.forRoot(),
    App4SharedModule.forRoot(),
    App5SharedModule.forRoot()

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
