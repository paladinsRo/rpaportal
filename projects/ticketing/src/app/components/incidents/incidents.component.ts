import { Component, OnInit, EventEmitter } from '@angular/core';
import { TicketService } from '../../services/ticket.service';
import { TicketModel } from '../../models/ticket.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MaterializeAction } from 'angular2-materialize';

@Component({
  selector: 'app-incidents',
  templateUrl: './incidents.component.html',
  styleUrls: ['./incidents.component.css']
})
export class IncidentsComponent implements OnInit {

  constructor(private ticketService: TicketService) { }
  tickets: TicketModel[];
  selectedTicket : TicketModel;
  newTicket : TicketModel;
  inputForm: FormGroup;
  modalActions = new EventEmitter<string | MaterializeAction>();
  newModalActions = new EventEmitter<string | MaterializeAction>();
  newTicketID;
  
  ngOnInit() {
    this.newTicketID= Date.now();
    this.newTicket= new TicketModel(this.newTicketID,"","","",0,"","")

    this.tickets=this.ticketService.getAllTickets();
    this.inputForm = new FormGroup({
      dueDate:  new FormControl(null, Validators.required),
      status:  new FormControl(null, Validators.required),
      item: new FormControl(null, Validators.required),
      quantity:  new FormControl(null, Validators.required),
      employeeEmail :  new FormControl(null, Validators.required),
      description:  new FormControl(null, Validators.required),
     
    });
  }

  onMoreDetails(ticket: TicketModel){
    this.selectedTicket= ticket;
    this.modalActions.emit({ action: 'modal', params: ['open'] });
  }


  
  onNewTicket(){
    this.newTicketID= Date.now();
  //  this.newTicket= new TicketModel(this.newTicketID,"","","",0,"","")
   
    this.newModalActions.emit({ action: 'modal2', params: ['open'] });
  }

  closeModal() {
    this.modalActions.emit({ action: 'modal', params: ['close'] });
  }

}
