export class ClientModel {
  _id: string;
  lastName: string;
  firstName: string;
  city: string;
  address: string;
  phone: string;
  email: string;
  taxIdNo: string;
  serialNo: string;
  validity: string;
  accountRon: string;
  accountEur: string;
  maximumAllowed: string;

  constructor(
    _id: string,
    lastName: string,
    firstName: string,
    city: string,
    address: string,
    phone: string,
    email: string,
    taxIdNo: string,
    serialNo: string,
    validity: string,
    accountRon: string,
    accountEur: string,
    maximumAllowed: string,
  ) {
    this._id = _id;
    this.lastName = lastName;
    this.firstName = firstName;
    this.city = city;
    this.address = address;
    this.phone = phone;
    this.email = email;
    this.taxIdNo = taxIdNo;
    this.serialNo = serialNo;
    this.validity = validity;
    this.accountRon = accountRon;
    this.accountEur = accountEur;
    this.maximumAllowed = maximumAllowed;

  }
}

