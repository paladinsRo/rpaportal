import { Component, OnInit, Output } from '@angular/core';
import { EventEmitter } from '@angular/core';
import { MaterializeAction } from 'angular2-materialize';
import { FormGroup, FormControl, Validators, } from '@angular/forms';
import { InvoiceModel } from './../../../models/invoice.model';
import { ServiceInvoices } from './../../../services/invoices.service';
import { ServiceLogin } from './../../../services/login.service';
import { UserModel } from './../../../models/user.model';
import { CompanyModel } from './../../../models/companies.model';
import { ServiceCompanies } from './../../../services/companies.service';

@Component({
  selector: 'app-invoices-list',
  templateUrl: './invoices-list.component.html',
  styleUrls: ['./invoices-list.component.css']
})
export class InvoicesListComponent implements OnInit {

  modalActions = new EventEmitter<string | MaterializeAction>();
  public inputForm: FormGroup;
  private user: UserModel;
  newInvoice: InvoiceModel;
  company: CompanyModel;
  invoices: InvoiceModel[];
  today: number = Date.now();
  fileUploaded: Boolean = false;
  fileToUpload: File;

  @Output() invoicesLoaded = new EventEmitter();

  constructor(
    private srvInvoices: ServiceInvoices,
    private srvUser: ServiceLogin,
    private srvCompany: ServiceCompanies
  ) { }

  ngOnInit() {
    this.user = this.srvUser.getUser();

    this.srvCompany.getCompanyById(this.user.companyId)
      .then(
        result => {
          this.company = result;
          this.initNewInvoice();
          this.getCompanyInvoices();
        });

    this.inputForm = new FormGroup({
      invoiceAmount: new FormControl(null, Validators.required),
      comment: new FormControl(null, Validators.required),

    });
  }

  getLoadedInvoices(lists) {
    this.invoicesLoaded.emit(lists);
  }

  getCompanyInvoices() {
    this.srvInvoices.getInvoicesByCompany(this.company.companyName).then(result => {
      this.invoices = result;
      this.getLoadedInvoices(this.invoices);
    });
  }

  initNewInvoice() {
    this.newInvoice = new InvoiceModel(
      this.user.companyId,
      this.company.companyName,
      '',
      '',
      this.today.toString(),
      '',
      '',
      '',
      '',
      'Pending',
      'No comment',
      false,
      false,
      false,
      ''
    )

  }

  openModal() {
    this.initNewInvoice();
    this.modalActions.emit({ action: 'modal', params: ['open'] });
  }
  closeModal() {
    this.modalActions.emit({ action: 'modal', params: ['close'] });
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.fileUploaded = true;
  }

  onSubmit() {
    this.newInvoice.invoiceFile = this.fileToUpload.name;
    this.newInvoice.invoiceAmount = this.inputForm.controls['invoiceAmount'].value;
    this.newInvoice.comment = this.inputForm.controls['comment'].value;
    this.invoices.push(this.newInvoice);
    this.srvInvoices.postInvoiceByCompany(this.newInvoice);
    // this.getCompanyInvoices();
    this.srvInvoices.postFile(this.fileToUpload);

    this.closeModal();
  }

  deleteInvoice(invoice){
    this.srvInvoices.deleteInvoice(invoice);
    const idx= this.invoices.findIndex(x => x._id === invoice._id);
    this.invoices.splice(idx,1);
  }

}

