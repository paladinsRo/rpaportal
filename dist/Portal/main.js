(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./projects/creditRequest/src/app/app-routing.module.ts":
/*!**************************************************************!*\
  !*** ./projects/creditRequest/src/app/app-routing.module.ts ***!
  \**************************************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_client_client_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/client/client.component */ "./projects/creditRequest/src/app/components/client/client.component.ts");
/* harmony import */ var _components_credit_ipotecar_credit_ipotecar_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/credit-ipotecar/credit-ipotecar.component */ "./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.ts");
/* harmony import */ var _components_credit_list_credit_list_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/credit-list/credit-list.component */ "./projects/creditRequest/src/app/components/credit-list/credit-list.component.ts");






var routes = [
    { path: 'creditRequest/client', component: _components_client_client_component__WEBPACK_IMPORTED_MODULE_3__["ClientComponent"] },
    { path: 'creditRequest', component: _components_client_client_component__WEBPACK_IMPORTED_MODULE_3__["ClientComponent"] },
    { path: 'creditRequest/ipotecar', component: _components_credit_ipotecar_credit_ipotecar_component__WEBPACK_IMPORTED_MODULE_4__["CreditIpotecarComponent"] },
    { path: 'creditRequest/creditList', component: _components_credit_list_credit_list_component__WEBPACK_IMPORTED_MODULE_5__["CreditListComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/app.component.css":
/*!**********************************************************!*\
  !*** ./projects/creditRequest/src/app/app.component.css ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9qZWN0cy9jcmVkaXRSZXF1ZXN0L3NyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MifQ== */"

/***/ }),

/***/ "./projects/creditRequest/src/app/app.component.html":
/*!***********************************************************!*\
  !*** ./projects/creditRequest/src/app/app.component.html ***!
  \***********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./projects/creditRequest/src/app/app.component.ts":
/*!*********************************************************!*\
  !*** ./projects/creditRequest/src/app/app.component.ts ***!
  \*********************************************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'creditRequest';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./projects/creditRequest/src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./projects/creditRequest/src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/app.module.ts":
/*!******************************************************!*\
  !*** ./projects/creditRequest/src/app/app.module.ts ***!
  \******************************************************/
/*! exports provided: AppModule, App4SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "App4SharedModule", function() { return App4SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./projects/creditRequest/src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./projects/creditRequest/src/app/app.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var angular2_materialize__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular2-materialize */ "./node_modules/angular2-materialize/dist/index.js");
/* harmony import */ var materialize_css__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! materialize-css */ "./node_modules/materialize-css/dist/js/materialize.js");
/* harmony import */ var materialize_css__WEBPACK_IMPORTED_MODULE_9___default = /*#__PURE__*/__webpack_require__.n(materialize_css__WEBPACK_IMPORTED_MODULE_9__);
/* harmony import */ var _components_client_client_component__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./components/client/client.component */ "./projects/creditRequest/src/app/components/client/client.component.ts");
/* harmony import */ var _components_credit_ipotecar_credit_ipotecar_component__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./components/credit-ipotecar/credit-ipotecar.component */ "./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.ts");
/* harmony import */ var _components_credit_list_credit_list_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/credit-list/credit-list.component */ "./projects/creditRequest/src/app/components/credit-list/credit-list.component.ts");
/* harmony import */ var _services_client_service__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./services/client.service */ "./projects/creditRequest/src/app/services/client.service.ts");
/* harmony import */ var _services_credit_service__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./services/credit.service */ "./projects/creditRequest/src/app/services/credit.service.ts");















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _components_client_client_component__WEBPACK_IMPORTED_MODULE_10__["ClientComponent"],
                _components_credit_ipotecar_credit_ipotecar_component__WEBPACK_IMPORTED_MODULE_11__["CreditIpotecarComponent"],
                _components_credit_list_credit_list_component__WEBPACK_IMPORTED_MODULE_12__["CreditListComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_7__["JsonpModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                angular2_materialize__WEBPACK_IMPORTED_MODULE_8__["MaterializeModule"]
            ],
            providers: [
                _services_client_service__WEBPACK_IMPORTED_MODULE_13__["ServiceClient"],
                _services_credit_service__WEBPACK_IMPORTED_MODULE_14__["ServiceCredit"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());

var App4SharedModule = /** @class */ (function () {
    function App4SharedModule() {
    }
    App4SharedModule.forRoot = function () {
        return {
            ngModule: AppModule,
            providers: []
        };
    };
    App4SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({})
    ], App4SharedModule);
    return App4SharedModule;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/components/client/client.component.html":
/*!********************************************************************************!*\
  !*** ./projects/creditRequest/src/app/components/client/client.component.html ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<div class=\"full-body\">\n  <div class=\"z-depth-2 row loginForm\">\n    <div class=\"row\">\n      <div class=\"col s12 m12 l12\">\n        <div class=\"row center-align\"> <h4 style=\"font-weight: bold; \">Choose a type of loan</h4> </div><br>\n        <div class=\"row\"></div>\n      </div>\n\n      <div class=\"col s112 m12 l12\">\n          <div class=\"row\">\n              <div class=\"col s3 m3 l3\">\n                <div class=\"card\">\n                  <div class=\"card-image\">\n                    <img src=\"assets/images/factoring/credit_personal.jpg\">\n                    <span class=\"card-title card-text\" >Personal Loan</span>\n                  </div>\n                </div>\n              </div>\n              <div class=\"col s3 m3 l3\">\n                  <div class=\"card\">\n                    <div class=\"card-image\">\n                      <img src=\"assets/images/factoring/credit_imobiliar.jpg\" >\n                      <span class=\"card-title card-text \">Home Loan</span>\n                    </div>\n                  </div>\n                </div>\n                <div class=\"col s3 m3 l3\">\n                    <div class=\"card\">\n                      <div class=\"card-image\" (click)=\"onClick()\">\n                        <img src=\"assets/images/factoring/credit_ipotecar.jpg\" >\n                        <span class=\"card-title card-text\">Mortgage</span>\n                      </div>\n                    </div>\n                  </div>\n                  <div class=\"col s3 m3 l3\">\n                      <div class=\"card\">\n                        <div class=\"card-image\">\n                          <img src=\"assets/images/factoring/credit_card.jpg\">\n                          <span class=\"card-title card-text\">Credit Card</span>\n                        </div>\n                      </div>\n                    </div>\n            </div>\n      </div>\n    </div>\n  </div>\n"

/***/ }),

/***/ "./projects/creditRequest/src/app/components/client/client.component.scss":
/*!********************************************************************************!*\
  !*** ./projects/creditRequest/src/app/components/client/client.component.scss ***!
  \********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-body {\n  padding-top: 20px;\n  height: 80%;\n  overflow: auto; }\n\n.loginForm {\n  padding: 30px;\n  max-width: 95%; }\n\n.row {\n  margin-bottom: 0px; }\n\n.card-text {\n  padding-bottom: 15px !important; }\n\n.brand-logo {\n  padding-left: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2NyZWRpdFJlcXVlc3Qvc3JjL2FwcC9jb21wb25lbnRzL2NsaWVudC9DOlxcVXNlcnNcXG1paGFpLmFuZHJlaVxcT25lRHJpdmUgLSBVaVBhdGhcXFVpUGF0aFxccHJvamVjdHNcXF9JTlRFUk5BTF9SUEFQb3J0YWxcXFBvcnRhbC9wcm9qZWN0c1xcY3JlZGl0UmVxdWVzdFxcc3JjXFxhcHBcXGNvbXBvbmVudHNcXGNsaWVudFxcY2xpZW50LmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLFdBQVU7RUFDVixjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsYUFBWTtFQUNaLGNBQWEsRUFBQTs7QUFHZjtFQUNFLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLCtCQUErQixFQUFBOztBQUdqQztFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InByb2plY3RzL2NyZWRpdFJlcXVlc3Qvc3JjL2FwcC9jb21wb25lbnRzL2NsaWVudC9jbGllbnQuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmZ1bGwtYm9keXtcclxuICBwYWRkaW5nLXRvcDogMjBweDtcclxuICBoZWlnaHQ6ODAlO1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG59XHJcblxyXG4ubG9naW5Gb3JtIHtcclxuICBwYWRkaW5nOjMwcHg7XHJcbiAgbWF4LXdpZHRoOjk1JTtcclxufVxyXG5cclxuLnJvdyB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG59XHJcblxyXG4uY2FyZC10ZXh0e1xyXG4gIHBhZGRpbmctYm90dG9tOiAxNXB4ICFpbXBvcnRhbnQ7XHJcbn1cclxuXHJcbi5icmFuZC1sb2dve1xyXG4gIHBhZGRpbmctbGVmdDogMjBweDtcclxuXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./projects/creditRequest/src/app/components/client/client.component.ts":
/*!******************************************************************************!*\
  !*** ./projects/creditRequest/src/app/components/client/client.component.ts ***!
  \******************************************************************************/
/*! exports provided: ClientComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientComponent", function() { return ClientComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");



var ClientComponent = /** @class */ (function () {
    function ClientComponent(router) {
        this.router = router;
    }
    ClientComponent.prototype.ngOnInit = function () {
    };
    ClientComponent.prototype.onClick = function () {
        this.router.navigateByUrl('creditRequest/ipotecar');
    };
    ClientComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-client',
            template: __webpack_require__(/*! ./client.component.html */ "./projects/creditRequest/src/app/components/client/client.component.html"),
            styles: [__webpack_require__(/*! ./client.component.scss */ "./projects/creditRequest/src/app/components/client/client.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], ClientComponent);
    return ClientComponent;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.html":
/*!**************************************************************************************************!*\
  !*** ./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.html ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header \">\n  <nav>\n    <div class=\"nav-wrapper uiColorSecondary\">\n      <a class=\"brand-logo\">Credit Request</a>\n      <ul id=\"nav-mobile\" class=\"right hide-on-med-and-down\">\n          <li ><a [routerLink]=\"['../../creditRequest/creditList']\"> Check Credits</a></li>\n        <li>\n          <a [routerLink]=\"['../../dashboard/']\"> <i class=\"material-icons\">home</i></a>\n        </li>\n      </ul>\n    </div>\n  </nav>\n</div>\n\n<div class=\"full-body\">\n  <div class=\"z-depth-2 row loginForm\">\n    <div class=\"row\">\n      <div class=\"col s8 m8 l7\">\n        <div class=\"row center-align\">\n          <form>\n            <input type=\"text\" class=\"form-control col s8 m8 l9\" [(ngModel)]=\"searchStr\" name=\"name\"\n              placeholder=\"Search client by last name\">\n            <button class=\"col s3 m3 l3\" (click)=\"searchClient()\" class=\"uiColorPrimary btn btn-primary btn-lg\">Search</button>\n          </form>\n\n        </div>\n        <div class=\"row\"></div>\n        <div *ngIf=\"showError\" style=\"color:red\"> user does not exist</div>\n        <br>\n      </div>\n      <div *ngIf=\"client\" class=\"\">\n        <div class=\" col s12 m12 l6\">\n          <div class=\"row\">\n            <h5>Client Details</h5>\n          </div>\n          <div class=\"row\">\n            <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">First Name: </span> {{client.firstName}}</span>\n            <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Last Name: </span>{{client.lastName}}</span>\n          </div>\n          <div class=\"row\">\n            <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">City: </span> {{client.city}}</span>\n            <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Address: </span>{{client.address}}</span>\n          </div>\n          <div class=\"row\">\n            <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Phone: </span>{{client.phone}}</span>\n            <span class=\"col s6 m6 l6\"><span style=\"font-weight: bold\">Email: </span>{{client.email}}</span>\n          </div>\n\n          <div class=\"row\">\n            <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Id Number: </span>{{client.serialNo}}</span>\n            <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Id Validity: </span>{{client.validity}}</span>\n          </div>\n\n          <div class=\"row\">\n            <span class=\"col s8 m8 l8\"><span style=\"font-weight: bold\">CNP: </span>{{client.taxIdNo}}</span>\n\n          </div>\n          <br><br>\n\n          <div class=\"row\">\n            <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Account RON: </span>{{client.accountRon | number : '1.2'}}</span>\n            <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Account EUR: </span>{{client.accountEur | number : '1.2'}}</span>\n          </div>\n          <br>\n          <div class=\"row\">\n            <button (click)=\"requestCredit()\" class=\"uiColorPrimary btn btn-primary btn-lg\">Update client data\n              <i class=\"material-icons right\">edit</i></button>\n          </div>\n        </div>\n        <div class=\"row col s12 m12 l6\">\n          <div class=\"row\">\n            <h5>Credits</h5>\n\n          </div>\n          <div *ngIf=\"!showCredit\" class=\"row\">\n            <span style=\"color:green\"> No credits assigned to this client</span>\n          </div>\n          <div *ngIf=\"showCredit\" class=\"row\">\n            <span style=\"color:green\">New Credit submited for review</span>\n            <br><br>\n            <div class=\"row\">\n              <h5>Credit Details</h5>\n            </div>\n            <div class=\"row\">\n              <table class=\"striped\">\n                <thead>\n                  <tr>\n                    <th>Credit ID</th>\n                    <th>Requested Amount</th>\n                    <th>Credit Period</th>\n                    <th>Status</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr>\n                    <th>{{creditRequest.creditId}}</th>\n                    <th>{{creditRequest.totalSum}}</th>\n                    <th>{{creditRequest.period}}</th>\n                    <th>{{creditRequest.status}}</th>\n                  </tr>\n                </tbody>\n              </table>\n            </div>\n\n          </div><br>\n          <div class=\"row\" *ngIf=\"showApproval && !showCredit\">\n            <input type=\"checkbox\" id=\"check1\" [checked]=\"check1\" (change)=\"check1 = !check1\" />\n            <label for=\"check1\" class=\"black-text flow-text\"> The user read the information regarding the processing of\n              personal data </label>\n\n            <div class=\"row\"></div><br>\n            <span style=\"font-weight: bold\">The user agrees with:</span>\n            <div class=\"row\"></div>\n            <input type=\"checkbox\" id=\"check2\" [checked]=\"check2\" (change)=\"check2 = !check2\" />\n            <label for=\"check2\" class=\"black-text flow-text\">The processing of personal data by the Credit\n              Bureau</label>\n            <div class=\"row\"></div>\n            <input type=\"checkbox\" id=\"check31\" [checked]=\"check3\" (change)=\"check3 = !check3\" />\n            <label for=\"check31\" class=\"black-text flow-text\"> Checking and processing his income data from ANAF </label>\n\n            <div class=\"row\"></div>\n          </div>\n\n          <button *ngIf=\"!showApproval && !showCredit\" (click)=\"creditRequst()\"\n            class=\"uiColorPrimary btn btn-primary btn-lg\">New credit request</button>\n          <div class=\"row\"></div><br>\n\n          <button *ngIf=\"showApproval && !(check1 && check2 && check3) && !showCredit\" disabled=\"true\"\n            (click)=\"creditRequst()\" class=\"uiColorPrimary btn btn-primary btn-lg\">New credit request</button>\n          &nbsp;&nbsp;\n          <button *ngIf=\"!check1 && showApproval && !showCredit \" disabled=\"true\"\n            class=\"uiColorPrimary  btn btn-lg  btn-primary\">Print GDPR</button>\n\n\n          <button *ngIf=\"check1 && check2 && check3 && !showCredit \" (click)=\"openModalCredit()\"\n            class=\"uiColorPrimary btn btn-primary btn-lg\">New credit request</button> &nbsp;&nbsp;\n          <button *ngIf=\"check1 && !showCredit \" class=\"uiColorPrimary btn btn-primary btn-lg\" (click)=\"printGDPR()\">Print GDPR</button>\n          <br>\n        </div>\n      </div>\n    </div>\n\n    <div id=\"modal2\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalActionsCredit\">\n      <div class=\"modal-content\">\n        <div style=\"padding-bottom: 20px\">\n          <span style=\"font-weight:bold;font-size:24px;padding: 10px; \">New credit request</span>\n          <i class=\"material-icons right modal-action modal-close \" (click)=\"closeModalUpdate()\">close</i>\n        </div>\n        <div class=\"row\">\n          <div [formGroup]=\"creditForm\">\n            <div class=\"col s12 m12 l12\">\n                <div class=\"col s12 m12 l5\">\n                    <label for=\"creditId\">Credit ID</label>\n                    <input name=\"creditId\" [(ngModel)]=\"creditRequest.creditId\" value='{{creditRequest.creditId}}' formControlName=\"creditId\">\n                  </div>\n                  <div class=\"col s12 m12 l1\"></div>\n                  <div class=\"col s12 m12 l5\">\n                    <label for=\"currencyCode\">Currency Code:</label>\n                    <input name=\"currencyCode\" [ngModel]=\"creditRequest.currencyCode\" formControlName=\"currencyCode\">\n                  </div>\n              <div class=\"col s12 m12 l5\">\n                <label for=\"income\">Monthly Income</label>\n                <input name=\"income\" [ngModel]=\"creditRequest.income\" formControlName=\"income\">\n              </div>\n              <div class=\"col s12 m12 l1\"></div>\n              <div class=\"col s12 m12 l5\">\n                <label for=\"personsNo\">Number of persons</label>\n                <input name=\"personsNo\" [ngModel]=\"creditRequest.personsNo\" formControlName=\"personsNo\">\n              </div>\n              <div class=\"col s12 m12 l5\">\n                <label for=\"selectItem\">Type of income</label>\n                <select name=\"selectItem\" materialize=\"material_select\">\n                  <option *ngFor=\"let option of selectOptions\" [value]=\"creditRequest.incomeType\">{{option}}</option>\n                </select>\n              </div>\n              <div class=\"col s12 m12 l1\"></div>\n              <div class=\"col s12 m12 l5\">\n                <label for=\"taxes\">Monthly Taxes</label>\n                <input name=\"taxes\" [ngModel]=\"creditRequest.taxes\" formControlName=\"taxes\">\n              </div>\n\n              <div class=\"col s12 m12 l5\">\n                <label for=\"totalSum\">Sum requested - EUR</label>\n                <input name=\"totalSum\" [ngModel]=\"creditRequest.totalSum\" formControlName=\"totalSum\">\n              </div>\n              <div class=\"col s12 m12 l1\"></div>\n              <div class=\"col s12 m12 l5\">\n                <label for=\"period\">Period(years)</label>\n                <input name=\"period\" [ngModel]=\"creditRequest.period\" formControlName=\"period\">\n              </div>\n\n            </div>\n            <div class=\"row\"><br></div>\n            <div class=\"row\">\n              <button (click)=\"submitNewCreditRequest()\" class=\"uiColorPrimary btn btn-primary btn-lg\">Submit</button>\n            </div>\n\n          </div>\n        </div>\n      </div>\n    </div>\n\n    <div *ngIf=\"client\" id=\"modal1\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalActionsUpdate\">\n      <div class=\"modal-content\">\n        <div style=\"padding-bottom: 20px\">\n          <span style=\"font-weight:bold;font-size:24px;padding: 10px; \">Update user's details</span>\n          <i class=\"material-icons right modal-action modal-close \" (click)=\"closeModalUpdate()\">close</i>\n        </div>\n        <div class=\"row\">\n          <div [formGroup]=\"inputForm\">\n            <div class=\"col s12 m12 l4\">\n              <label for=\"firstName\">First Name</label>\n              <input name=\"firstName\" [ngModel]=\"client.firstName\" formControlName=\"firstName\">\n            </div>\n            <div class=\"col s2 m2 l2\"></div>\n            <div class=\"col s12 m12 l5\">\n              <label for=\"lastName\">Last Name</label>\n              <input name=\"lastName\" [ngModel]=\"client.lastName\" formControlName=\"lastName\">\n            </div>\n\n            <div class=\"col s12 m12 l4\">\n              <label for=\"city\">City</label>\n              <input name=\"city\" [ngModel]=\"client.city\" formControlName=\"city\">\n            </div>\n            <div class=\"col s2 m2 l2\"></div>\n            <div class=\"col s12 m12 l5\">\n              <label for=\"address\">Address:</label>\n              <input name=\"address\" [ngModel]=\"client.address\" formControlName=\"address\">\n            </div>\n\n            <div class=\"col s12 m12 l4\">\n              <label for=\"phone\">Phone</label>\n              <input name=\"phone\" [ngModel]=\"client.phone\" formControlName=\"phone\">\n            </div>\n            <div class=\"col s2 m2 l2\"></div>\n            <div class=\"col s12 m12 l5\">\n              <label for=\"email\">Email:</label>\n              <input name=\"email\" [ngModel]=\"client.email\" formControlName=\"email\">\n            </div>\n\n            <div class=\"col s12 m12 l4\">\n              <label for=\"serialNo\">ID number</label>\n              <input name=\"serialNo\" [ngModel]=\"client.serialNo\" formControlName=\"serialNo\">\n            </div>\n            <div class=\"col s2 m2 l2\"></div>\n            <div class=\"col s12 m12 l5\">\n              <label for=\"validity\">ID validity:</label>\n              <input name=\"validity\" type=\"text\" [ngModel]=\"client.validity\" formControlName=\"validity\">\n            </div>\n\n            <div class=\"col s12 m12 l5\">\n              <label for=\"taxIdNo\">CNP</label>\n              <input name=\"taxIdNo\" [ngModel]=\"client.taxIdNo\" formControlName=\"taxIdNo\">\n            </div>\n            <div class=\"row\"></div>\n            <input class=\"btn uiColorPrimary\" (click)=\"onSubmit()\" type=\"submit\" value=\"Submit\">\n          </div>\n        </div>\n      </div>\n    </div>\n\n  </div>\n"

/***/ }),

/***/ "./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.scss":
/*!**************************************************************************************************!*\
  !*** ./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.scss ***!
  \**************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-body {\n  padding-top: 20px;\n  height: 90%;\n  overflow: auto; }\n\n.loginForm {\n  padding: 30px;\n  max-width: 95%; }\n\n.row {\n  margin-bottom: 0px; }\n\n.button-space {\n  padding: 10px; }\n\n.brand-logo {\n  padding-left: 20px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2NyZWRpdFJlcXVlc3Qvc3JjL2FwcC9jb21wb25lbnRzL2NyZWRpdC1pcG90ZWNhci9DOlxcVXNlcnNcXG1paGFpLmFuZHJlaVxcT25lRHJpdmUgLSBVaVBhdGhcXFVpUGF0aFxccHJvamVjdHNcXF9JTlRFUk5BTF9SUEFQb3J0YWxcXFBvcnRhbC9wcm9qZWN0c1xcY3JlZGl0UmVxdWVzdFxcc3JjXFxhcHBcXGNvbXBvbmVudHNcXGNyZWRpdC1pcG90ZWNhclxcY3JlZGl0LWlwb3RlY2FyLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0UsaUJBQWlCO0VBQ2pCLFdBQVU7RUFDVixjQUFjLEVBQUE7O0FBR2hCO0VBQ0UsYUFBWTtFQUNaLGNBQWEsRUFBQTs7QUFHZjtFQUNFLGtCQUFrQixFQUFBOztBQUdwQjtFQUNFLGFBQWEsRUFBQTs7QUFHZjtFQUNFLGtCQUFrQixFQUFBIiwiZmlsZSI6InByb2plY3RzL2NyZWRpdFJlcXVlc3Qvc3JjL2FwcC9jb21wb25lbnRzL2NyZWRpdC1pcG90ZWNhci9jcmVkaXQtaXBvdGVjYXIuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuXHJcbi5mdWxsLWJvZHl7XHJcbiAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgaGVpZ2h0OjkwJTtcclxuICBvdmVyZmxvdzogYXV0bztcclxufVxyXG5cclxuLmxvZ2luRm9ybSB7XHJcbiAgcGFkZGluZzozMHB4O1xyXG4gIG1heC13aWR0aDo5NSU7XHJcbn1cclxuXHJcbi5yb3cge1xyXG4gIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG5cclxuLmJ1dHRvbi1zcGFjZXtcclxuICBwYWRkaW5nOiAxMHB4O1xyXG59XHJcblxyXG4uYnJhbmQtbG9nb3tcclxuICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcblxyXG59XHJcbiJdfQ== */"

/***/ }),

/***/ "./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.ts":
/*!************************************************************************************************!*\
  !*** ./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.ts ***!
  \************************************************************************************************/
/*! exports provided: CreditIpotecarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreditIpotecarComponent", function() { return CreditIpotecarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_client_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/client.service */ "./projects/creditRequest/src/app/services/client.service.ts");
/* harmony import */ var _models_client_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../models/client.model */ "./projects/creditRequest/src/app/models/client.model.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models_credit_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../models/credit.model */ "./projects/creditRequest/src/app/models/credit.model.ts");
/* harmony import */ var _services_credit_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../services/credit.service */ "./projects/creditRequest/src/app/services/credit.service.ts");







var CreditIpotecarComponent = /** @class */ (function () {
    function CreditIpotecarComponent(srvClient, srvCredit) {
        this.srvClient = srvClient;
        this.srvCredit = srvCredit;
        this.searchStr = '';
        this.creditRequest = new _models_credit_model__WEBPACK_IMPORTED_MODULE_5__["CreditModel"]('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
        this.showApproval = false;
        this.check1 = false;
        this.check2 = false;
        this.check3 = false;
        this.showError = false;
        this.modalActionsUpdate = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.modalActionsCredit = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.showCredit = false;
        this.selectOptions = [
            'Retired',
            'Determined period employment',
            'Undetermined period employment'
        ];
    }
    CreditIpotecarComponent.prototype.ngOnInit = function () {
        this.inputForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            firstName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            lastName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            city: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            address: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            phone: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            email: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            serialNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            validity: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            taxIdNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required)
        });
        this.creditRequest.creditId = (Math.floor(Math.random() * Math.floor(5000))).toString();
        this.creditForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            creditId: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            income: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            personsNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            currencyCode: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            incomeType: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            taxes: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            period: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            totalSum: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required)
        });
    };
    CreditIpotecarComponent.prototype.searchClient = function () {
        var _this = this;
        this.showApproval = false;
        this.srvClient.getClient(this.searchStr).subscribe(function (res) {
            _this.client = res[0];
            if (_this.client) {
                _this.srvClient.setClient(_this.client);
                _this.showError = false;
                _this.check1 = false;
                _this.check2 = false;
                _this.check3 = false;
            }
            else {
                _this.showError = true;
                _this.check1 = false;
                _this.check2 = false;
                _this.check3 = false;
                _this.searchStr = '';
            }
        });
    };
    CreditIpotecarComponent.prototype.onSubmit = function () {
        this.srvClient.setClient(new _models_client_model__WEBPACK_IMPORTED_MODULE_3__["ClientModel"](this.client._id, this.inputForm.controls['lastName'].value, this.inputForm.controls['firstName'].value, this.inputForm.controls['city'].value, this.inputForm.controls['address'].value, this.inputForm.controls['phone'].value, this.inputForm.controls['email'].value, this.inputForm.controls['taxIdNo'].value, this.inputForm.controls['serialNo'].value, this.inputForm.controls['validity'].value, this.client.accountRon, this.client.accountEur, this.client.maximumAllowed));
        this.client = this.srvClient.getSavedClient();
        this.closeModalUpdate();
    };
    CreditIpotecarComponent.prototype.submitNewCreditRequest = function () {
        this.showCredit = true;
        this.creditRequest.clientName = this.client.lastName;
        this.creditRequest.creditId = this.creditForm.controls['creditId'].value;
        this.creditRequest.income = this.creditForm.controls['income'].value;
        this.creditRequest.personsNo = this.creditForm.controls['personsNo'].value;
        this.creditRequest.incomeType = this.creditForm.controls['incomeType'].value;
        this.creditRequest.currencyCode = this.creditForm.controls['currencyCode'].value;
        this.creditRequest.taxes = this.creditForm.controls['taxes'].value;
        this.creditRequest.period = this.creditForm.controls['period'].value;
        this.creditRequest.totalSum = this.creditForm.controls['totalSum'].value;
        this.creditRequest.status = 'Pending';
        // this.srvCredit.uploadCredit(this.creditRequest);
        this.srvCredit.uploadCreditLocal(this.creditRequest);
        this.closeModalCredit();
    };
    CreditIpotecarComponent.prototype.requestCredit = function () {
        this.openModalUpdate();
    };
    CreditIpotecarComponent.prototype.creditRequst = function () {
        this.showApproval = true;
    };
    CreditIpotecarComponent.prototype.openModalCredit = function () {
        this.modalActionsCredit.emit({ action: 'modal', params: ['open'] });
    };
    CreditIpotecarComponent.prototype.closeModalCredit = function () {
        this.modalActionsCredit.emit({ action: 'modal', params: ['close'] });
    };
    CreditIpotecarComponent.prototype.openModalUpdate = function () {
        this.modalActionsUpdate.emit({ action: 'modal', params: ['open'] });
    };
    CreditIpotecarComponent.prototype.closeModalUpdate = function () {
        this.modalActionsUpdate.emit({ action: 'modal', params: ['close'] });
    };
    CreditIpotecarComponent.prototype.printGDPR = function () {
        window.print();
    };
    CreditIpotecarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-credit-ipotecar',
            template: __webpack_require__(/*! ./credit-ipotecar.component.html */ "./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.html"),
            styles: [__webpack_require__(/*! ./credit-ipotecar.component.scss */ "./projects/creditRequest/src/app/components/credit-ipotecar/credit-ipotecar.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_client_service__WEBPACK_IMPORTED_MODULE_2__["ServiceClient"],
            _services_credit_service__WEBPACK_IMPORTED_MODULE_6__["ServiceCredit"]])
    ], CreditIpotecarComponent);
    return CreditIpotecarComponent;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/components/credit-list/credit-list.component.css":
/*!*****************************************************************************************!*\
  !*** ./projects/creditRequest/src/app/components/credit-list/credit-list.component.css ***!
  \*****************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.full-body{\r\n  padding-top: 20px;\r\n  padding-bottom: 20px;\r\n  height:90%;\r\n  overflow: auto;\r\n}\r\n\r\n.loginForm {\r\n  padding:30px;\r\n  max-width:95%;\r\n}\r\n\r\n.row {\r\n  margin-bottom: 0px;\r\n}\r\n\r\nspan{\r\n  font-size: 18px;\r\n}\r\n\r\n.brand-logo{\r\n  padding-left: 20px;\r\n\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2NyZWRpdFJlcXVlc3Qvc3JjL2FwcC9jb21wb25lbnRzL2NyZWRpdC1saXN0L2NyZWRpdC1saXN0LmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUNBO0VBQ0UsaUJBQWlCO0VBQ2pCLG9CQUFvQjtFQUNwQixVQUFVO0VBQ1YsY0FBYztBQUNoQjs7QUFFQTtFQUNFLFlBQVk7RUFDWixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxlQUFlO0FBQ2pCOztBQUVBO0VBQ0Usa0JBQWtCOztBQUVwQiIsImZpbGUiOiJwcm9qZWN0cy9jcmVkaXRSZXF1ZXN0L3NyYy9hcHAvY29tcG9uZW50cy9jcmVkaXQtbGlzdC9jcmVkaXQtbGlzdC5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi5mdWxsLWJvZHl7XHJcbiAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgcGFkZGluZy1ib3R0b206IDIwcHg7XHJcbiAgaGVpZ2h0OjkwJTtcclxuICBvdmVyZmxvdzogYXV0bztcclxufVxyXG5cclxuLmxvZ2luRm9ybSB7XHJcbiAgcGFkZGluZzozMHB4O1xyXG4gIG1heC13aWR0aDo5NSU7XHJcbn1cclxuXHJcbi5yb3cge1xyXG4gIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG5cclxuc3BhbntcclxuICBmb250LXNpemU6IDE4cHg7XHJcbn1cclxuXHJcbi5icmFuZC1sb2dve1xyXG4gIHBhZGRpbmctbGVmdDogMjBweDtcclxuXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./projects/creditRequest/src/app/components/credit-list/credit-list.component.html":
/*!******************************************************************************************!*\
  !*** ./projects/creditRequest/src/app/components/credit-list/credit-list.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"header \">\n  <nav>\n    <div class=\"nav-wrapper uiColorSecondary\">\n      <a class=\"brand-logo\" [routerLink]=\"['../../creditRequest']\" >Credit Request</a>\n      <ul id=\"nav-mobile\" class=\"right hide-on-med-and-down\">\n          <li ><a [routerLink]=\"['../../creditRequest/creditList']\"> Check Credits</a></li>\n        <li>\n          <a [routerLink]=\"['../../dashboard']\"> <i class=\"material-icons\">home</i></a>\n        </li>\n      </ul>\n    </div>\n  </nav>\n</div>\n<div class=\"full-body\">\n  <div class=\"z-depth-2 row loginForm\">\n    <div class=\"row\">\n      <div class=\"col s12 m12 l12\">\n        <div class=\"row\" *ngIf=\"creditOffer\">\n          <div class=\"row \">\n            <h5 style=\"font-weight: bold; \">Global Tax Values</h5>\n          </div>\n          <div class=\"row col s12 m12 l12\">\n            <div [formGroup]=\"inputForm\">\n              <div class=\"col s3 m3 l3\">\n                <label for=\"analysisFee\">Analysis Fee</label>\n                <input name=\"analysisFee\" [(ngModel)]=\"creditOffer.analysisFee\" value='{{creditOffer.analysisFee}}'\n                  formControlName=\"analysisFee\">\n              </div>\n              <div class=\"col s3 m3 l3\">\n                <label for=\"dae\">DAE</label>\n                <input name=\"dae\" [(ngModel)]=\"creditOffer.dae\" value='{{creditOffer.dae}}' formControlName=\"dae\">\n              </div>\n              <div class=\"col s3 m3 l3\">\n                <label for=\"insuranceRate\">Insurance Rate</label>\n                <input name=\"insuranceRate\" [(ngModel)]=\"creditOffer.insuranceRate\"\n                  value='{{creditOffer.insuranceRate}}' formControlName=\"insuranceRate\">\n              </div>\n              <div class=\"col s3 m3 l3\">\n                <label for=\"otherFees\">Other Fees</label>\n                <input name=\"otherFees\" [(ngModel)]=\"creditOffer.otherFees\" value='{{creditOffer.otherFees}}'\n                  formControlName=\"otherFees\">\n              </div>\n            </div>\n          </div>\n          <div class=\"row\"></div><br>\n          <div class=\"row \">\n            <h5 style=\"font-weight: bold; \">List of Credit Requests </h5>\n          </div>\n          <div *ngIf=\"creditList\"></div>\n            <div class=\"row\">\n              <table class=\"striped\">\n                <thead>\n                  <tr>\n                    <th>Credit ID</th>\n                    <th>Client Name</th>\n                    <th>Client Income</th>\n                    <th>Period</th>\n                    <th>Sum Requested</th>\n                    <th>Status</th>\n                  </tr>\n                </thead>\n                <tbody>\n                  <tr *ngFor=\"let credit of creditList\">\n                    <th>\n                      {{credit.creditId}}\n                    </th>\n                    <th>\n                      {{credit.clientName}}\n                    </th>\n                    <th>\n                      {{credit.income}}\n                    </th>\n\n\n                    <th>\n                      {{credit.period}}\n                    </th>\n                    <th>\n                      {{credit.totalSum}}\n                    </th>\n                    <th>\n                      <span *ngIf=\"credit.status=='Pending'\" style=\"color: orange\"> {{credit.status}}</span>\n                      <span *ngIf=\"credit.status=='Offer made'\" style=\"color:blue\"> {{credit.status}}</span>\n                      <span *ngIf=\"credit.status=='Offer accepted'\" style=\"color:green\"> {{credit.status}}</span>\n                    </th>\n                    <th>\n                      <a  *ngIf=\"credit.status!=='Offer accepted'\" (click)=\"deleteCredit(credit)\"> <i class=\"material-icons left\">delete</i> </a>\n\n                    </th>\n                    <th>\n                      <a *ngIf=\"credit.status=='Offer made'\" (click)=\"acceptCredit(credit)\"> <i\n                        class=\"material-icons right\">done</i> </a>\n                    </th>\n                    <th>\n                      <button class=\"btn uiColorPrimary right\" (click)=\"openModalOffer(credit)\">See more\n                        details</button>\n                    </th>\n\n                  </tr>\n                </tbody>\n              </table>\n\n              <div *ngIf=\"selectedCredit\" id=\"modal1\" class=\"modal\" materialize=\"modal\"\n                [materializeActions]=\"modalActionsOffer\">\n                <div class=\"modal-content\">\n                  <div style=\"padding-bottom: 20px\">\n                    <span style=\"font-weight:bold;font-size:24px;padding: 13px; \">Create client Offer</span>\n                    <i class=\"material-icons right modal-action modal-close \" (click)=\"closeModalOffer()\">close</i>\n                  </div>\n                  <div [formGroup]=\"offerForm\" class=\"row\">\n                    <div class=\"col s12 m12 l12\">\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Client Name:\n                        </span>{{selectedCredit.clientName}}</span>\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Income:\n                        </span>{{selectedCredit.income}}</span>\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Sum Requested:\n                        </span>{{selectedCredit.totalSum}}</span>\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Number of persons:\n                        </span>{{selectedCredit.personsNo}}</span>\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Credit Period:\n                        </span>{{selectedCredit.period}}</span>\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Currency:\n                        </span>{{selectedCredit.currencyCode}}</span>\n\n                      <div class=\"row\"></div>\n                      <div class=\"row\"></div>\n                      <div class=\"row\"></div><hr>\n                      <span class=\"col s12 m12 l12\" style=\"font-weight:bold;font-size:24px;padding: 13px; \">Taxes</span>\n                      <br>\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Analysis Fees:\n                        </span>{{selectedCredit.analysisFee}}</span>\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Insurance Rate:\n                        </span>{{selectedCredit.insuranceRate}}</span>\n                      <div class=\"row\"></div>\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">DAE:\n                      </span>{{selectedCredit.dae}}</span>\n                    <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Other fees:\n                      </span>{{selectedCredit.otherFees}}</span>\n                      <br>\n                      <div class=\"row\"></div>\n                      <div class=\"row\"></div>\n                      <div class=\"row\"></div><hr><br>\n\n                      <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Monthly Payment:\n                      </span>{{selectedCredit.monthlyPayment | number : '1.0'}}</span>\n                    <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold\">Total Payment Amount:\n                      </span>{{selectedCredit.totalPaymentAmount | number : '1.0'}}</span>\n\n                      <br>\n                      <div class=\"row\">\n                        <span class=\"col s5 m5 l12  \"><span style=\"font-weight: bold\">Total Received Amount:\n                        </span>{{selectedCredit.totalReceivedAmount | number : '1.0'}}</span>\n                      </div>\n\n                    </div>\n                  </div><br>\n                  <div class=\"row\"></div>\n                  <div class=\"row\"></div>\n                  <div class=\"row\"></div>\n                  <div class=\"row\">\n                    <input class=\"btn uiColorPrimary \" (click)=\"createOffer()\" type=\"submit\" value=\"Submit\">\n                  </div>\n                </div>\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n"

/***/ }),

/***/ "./projects/creditRequest/src/app/components/credit-list/credit-list.component.ts":
/*!****************************************************************************************!*\
  !*** ./projects/creditRequest/src/app/components/credit-list/credit-list.component.ts ***!
  \****************************************************************************************/
/*! exports provided: CreditListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreditListComponent", function() { return CreditListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_credit_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/credit.service */ "./projects/creditRequest/src/app/services/credit.service.ts");
/* harmony import */ var _models_credit_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../models/credit.model */ "./projects/creditRequest/src/app/models/credit.model.ts");
/* harmony import */ var _models_creditOffer_model__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../models/creditOffer.model */ "./projects/creditRequest/src/app/models/creditOffer.model.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");






var CreditListComponent = /** @class */ (function () {
    function CreditListComponent(srvCredit) {
        this.srvCredit = srvCredit;
        this.selectedCredit = new _models_credit_model__WEBPACK_IMPORTED_MODULE_3__["CreditModel"]('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
        this.creditOffer = new _models_creditOffer_model__WEBPACK_IMPORTED_MODULE_4__["CreditOfferModel"]('200', '10', '5', '150');
        this.modalActionsOffer = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    ;
    CreditListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.srvCredit.getCreditsList().then(function (result) {
            _this.creditList = result;
        });
        console.log(this.creditList);
        this.inputForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
            analysisFee: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            insuranceRate: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            dae: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            otherFees: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required)
        });
        this.offerForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
            analysisFee: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            insuranceRate: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            dae: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            otherFees: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            monthlyPayment: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            totalPaymentAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            totalReceivedAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
        });
    };
    CreditListComponent.prototype.acceptCredit = function (credit) {
        credit.status = 'Offer accepted';
        this.srvCredit.updateCreditLocal(credit);
        var idx = this.creditList.findIndex(function (x) { return x.creditId === credit.creditId; });
        this.creditList[idx].status = 'Offer accepted';
    };
    CreditListComponent.prototype.deleteCredit = function (credit) {
        this.srvCredit.deleteCreditLocal(credit);
        this.creditList = this.srvCredit.getCreditsStorage();
    };
    CreditListComponent.prototype.openModalOffer = function (credit) {
        this.selectedCredit = credit;
        this.selectedCredit.dae = this.creditOffer.dae;
        this.selectedCredit.insuranceRate = this.creditOffer.insuranceRate;
        this.selectedCredit.dae = this.creditOffer.dae;
        this.selectedCredit.otherFees = this.creditOffer.otherFees;
        this.selectedCredit.analysisFee = this.creditOffer.analysisFee;
        this.selectedCredit.totalReceivedAmount = Math.round((parseInt(this.selectedCredit.totalSum) - parseInt(this.selectedCredit.analysisFee) - parseInt(this.selectedCredit.otherFees))).toString();
        var mRate = Math.round((parseInt(this.selectedCredit.totalSum) / (parseInt(this.selectedCredit.period) * 12)));
        var commision = Math.round(mRate * (parseInt(this.selectedCredit.dae) / 100));
        this.selectedCredit.monthlyPayment = mRate;
        this.selectedCredit.totalPaymentAmount = Math.round(((parseInt(mRate) + commision) * (parseInt(this.selectedCredit.period) * 12) + parseInt(this.selectedCredit.analysisFee) + parseInt(this.selectedCredit.otherFees))).toString();
        this.modalActionsOffer.emit({ action: 'modal', params: ['open'] });
    };
    CreditListComponent.prototype.closeModalOffer = function () {
        this.modalActionsOffer.emit({ action: 'modal', params: ['close'] });
    };
    CreditListComponent.prototype.createOffer = function () {
        var _this = this;
        this.selectedCredit.analysisFee = this.offerForm.controls['analysisFee'].value;
        this.selectedCredit.dae = this.offerForm.controls['dae'].value;
        this.selectedCredit.otherFees = this.offerForm.controls['otherFees'].value;
        this.selectedCredit.insuranceRate = this.offerForm.controls['insuranceRate'].value;
        this.selectedCredit.monthlyPayment = this.offerForm.controls['monthlyPayment'].value;
        this.selectedCredit.totalPaymentAmount = this.offerForm.controls['totalPaymentAmount'].value;
        this.selectedCredit.totalReceivedAmount = this.offerForm.controls['totalReceivedAmount'].value;
        this.selectedCredit.status = 'Offer made';
        //this.srvCredit.updateCredit(this.selectedCredit);
        var idx = this.creditList.findIndex(function (x) { return x.creditId === _this.selectedCredit.creditId; });
        this.creditList[idx].status = 'Offer made';
        this.closeModalOffer();
    };
    CreditListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-credit-list',
            template: __webpack_require__(/*! ./credit-list.component.html */ "./projects/creditRequest/src/app/components/credit-list/credit-list.component.html"),
            styles: [__webpack_require__(/*! ./credit-list.component.css */ "./projects/creditRequest/src/app/components/credit-list/credit-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_credit_service__WEBPACK_IMPORTED_MODULE_2__["ServiceCredit"]])
    ], CreditListComponent);
    return CreditListComponent;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/models/client.model.ts":
/*!***************************************************************!*\
  !*** ./projects/creditRequest/src/app/models/client.model.ts ***!
  \***************************************************************/
/*! exports provided: ClientModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientModel", function() { return ClientModel; });
var ClientModel = /** @class */ (function () {
    function ClientModel(_id, lastName, firstName, city, address, phone, email, taxIdNo, serialNo, validity, accountRon, accountEur, maximumAllowed) {
        this._id = _id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.city = city;
        this.address = address;
        this.phone = phone;
        this.email = email;
        this.taxIdNo = taxIdNo;
        this.serialNo = serialNo;
        this.validity = validity;
        this.accountRon = accountRon;
        this.accountEur = accountEur;
        this.maximumAllowed = maximumAllowed;
    }
    return ClientModel;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/models/credit.model.ts":
/*!***************************************************************!*\
  !*** ./projects/creditRequest/src/app/models/credit.model.ts ***!
  \***************************************************************/
/*! exports provided: CreditModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreditModel", function() { return CreditModel; });
var CreditModel = /** @class */ (function () {
    function CreditModel(_id, creditId, clientName, income, personsNo, incomeType, taxes, analysisFee, currencyCode, insuranceRate, dae, monthlyPayment, otherFees, period, quotationId, totalPaymentAmount, totalReceivedAmount, status) {
        this._id = _id;
        this.creditId = creditId;
        this.clientName = clientName;
        this.income = income;
        this.personsNo = personsNo;
        this.incomeType = incomeType;
        this.taxes = taxes;
        this.period = period;
        this.totalSum = this.totalSum;
        this.status = this.status;
        this.analysisFee = analysisFee;
        this.currencyCode = currencyCode;
        this.insuranceRate = insuranceRate;
        this.dae = dae;
        this.monthlyPayment = monthlyPayment;
        this.otherFees = otherFees;
        this.totalPaymentAmount = totalPaymentAmount;
        this.totalReceivedAmount = totalReceivedAmount;
    }
    return CreditModel;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/models/creditOffer.model.ts":
/*!********************************************************************!*\
  !*** ./projects/creditRequest/src/app/models/creditOffer.model.ts ***!
  \********************************************************************/
/*! exports provided: CreditOfferModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CreditOfferModel", function() { return CreditOfferModel; });
var CreditOfferModel = /** @class */ (function () {
    function CreditOfferModel(analysisFee, insuranceRate, dae, otherFees) {
        this.analysisFee = analysisFee;
        this.insuranceRate = insuranceRate;
        this.dae = dae;
        this.otherFees = otherFees;
    }
    return CreditOfferModel;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/services/client.service.ts":
/*!*******************************************************************!*\
  !*** ./projects/creditRequest/src/app/services/client.service.ts ***!
  \*******************************************************************/
/*! exports provided: ServiceClient */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceClient", function() { return ServiceClient; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");




var ServiceClient = /** @class */ (function () {
    function ServiceClient(http) {
        this.http = http;
        this.urlClient = "https://uipath509.westeurope.cloudapp.azure.com/api/v1/credit_client/";
    }
    ServiceClient.prototype.setClient = function (client) {
        this.client = client;
    };
    ServiceClient.prototype.getClient = function (lastName) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set("lastName", lastName);
        return this.http.get(this.urlClient, { params: params });
    };
    ServiceClient.prototype.getSavedClient = function () {
        return this.client;
    };
    ServiceClient.prototype.updateClient = function (client) {
        console.log(client);
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set("id", client._id);
        return this.http.put(this.urlClient, {
            lastName: client.lastName,
            firstName: client.firstName,
            city: client.city,
            address: client.address,
            phone: client.phone,
            email: client.email,
            taxIdNo: client.taxIdNo,
            serialNo: client.serialNo,
            validity: client.validity,
            accountRon: client.accountRon,
            accountEur: client.accountEur,
            maximumAllowed: client.maximumAllowed
        }, { params: params }).toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceClient.prototype.handleError = function (error) {
        console.error("An error occurred", error);
        return Promise.reject(error.message || error);
    };
    ServiceClient = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ServiceClient);
    return ServiceClient;
}());



/***/ }),

/***/ "./projects/creditRequest/src/app/services/credit.service.ts":
/*!*******************************************************************!*\
  !*** ./projects/creditRequest/src/app/services/credit.service.ts ***!
  \*******************************************************************/
/*! exports provided: ServiceCredit */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceCredit", function() { return ServiceCredit; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");



var ServiceCredit = /** @class */ (function () {
    function ServiceCredit(http) {
        this.http = http;
        this.url = 'https://uipath509.westeurope.cloudapp.azure.com/api/v1/credit_list/';
        this.credits = null;
    }
    // getCreditsList() {
    //   if (this.credits){
    //     return this.credits;
    //   } else {
    //     this.getCreditListDB().then(result => {
    //       this.credits = result;
    //       return this.credits;
    //     });
    //   }
    // }
    ServiceCredit.prototype.setCreditStorage = function (credits) {
        this.credits = credits;
        localStorage.setItem("credits", JSON.stringify(this.credits));
    };
    ServiceCredit.prototype.getCreditsStorage = function () {
        if (this.credits) {
            return this.credits;
        }
        else {
            var credits = JSON.parse(localStorage.getItem("credits"));
            return credits;
        }
    };
    ServiceCredit.prototype.getCreditsList = function () {
        var _this = this;
        this.getCreditsStorage();
        console.log(this.credits);
        return this.http
            .get(this.url)
            .toPromise()
            .then(function (resp) {
            if (_this.credits) {
                return _this.credits;
            }
            _this.credits = resp;
            return _this.credits;
        })
            .catch(this.handleError);
    };
    // deleteCredit(credit) {
    //   const params = new HttpParams().set("_id", credit._id);
    //   return this.http
    //     .delete(this.url, { params })
    //     .toPromise()
    //     .then(resp => {
    //       return resp;
    //     })
    //     .catch(this.handleError);
    // }
    ServiceCredit.prototype.deleteCreditLocal = function (credit) {
        var idx = this.credits.findIndex(function (x) { return x.creditId === credit.creditId; });
        this.credits.splice(idx, 1);
        this.setCreditStorage(this.credits);
    };
    // public uploadCredit(credit: CreditModel) {
    //   return this.http.post(this.url, {
    //     creditId: credit.creditId,
    //     clientName: credit.clientName,
    //     income: credit.income,
    //     personsNo: credit.personsNo,
    //     incomeType: credit.incomeType,
    //     taxes: credit.taxes,
    //     period: credit.period,
    //     totalSum: credit.totalSum,
    //     status: credit.status,
    //     analysisFee: credit.analysisFee,
    //     currencyCode: credit.currencyCode,
    //     insuranceRate: credit.insuranceRate,
    //     dae: credit.dae,
    //     monthlyPayment: credit.monthlyPayment,
    //     otherFees: credit.otherFees,
    //     totalPaymentAmount: credit.totalPaymentAmount,
    //     totalReceivedAmount: credit.totalReceivedAmount,
    //   }).toPromise()
    //     .then(resp => {
    //       return resp;
    //     })
    //     .catch(this.handleError);
    // }
    ServiceCredit.prototype.uploadCreditLocal = function (credit) {
        var _this = this;
        this.getCreditsList().then(function (resp) {
            _this.credits.push(credit);
            _this.setCreditStorage(_this.credits);
        });
    };
    ServiceCredit.prototype.updateCreditLocal = function (credit) {
        var idx = this.credits.findIndex(function (x) { return x.creditId === credit.creditId; });
        this.credits[idx] = credit;
    };
    // public updateCredit(credit: CreditModel) {
    //   console.log(credit)
    //   const params = new HttpParams().set("id", credit._id);
    //   return this.http.put(this.url,
    //     {
    //       creditId: credit.creditId,
    //       clientName: credit.clientName,
    //       income: credit.income,
    //       personsNo: credit.personsNo,
    //       incomeType: credit.incomeType,
    //       taxes: credit.taxes,
    //       period: credit.period,
    //       totalSum: credit.totalSum,
    //       status: credit.status,
    //       analysisFee: credit.analysisFee,
    //       currencyCode: credit.currencyCode,
    //       insuranceRate: credit.insuranceRate,
    //       dae: credit.dae,
    //       monthlyPayment: credit.monthlyPayment,
    //       otherFees: credit.otherFees,
    //       totalPaymentAmount: credit.totalPaymentAmount,
    //       totalReceivedAmount: credit.totalReceivedAmount,
    //     }, { params }
    //   ).toPromise()
    //     .then(resp => {
    //       return resp;
    //     })
    //     .catch(this.handleError);
    // }
    ServiceCredit.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    ServiceCredit = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"]])
    ], ServiceCredit);
    return ServiceCredit;
}());



/***/ }),

/***/ "./projects/factoring/src/app/app-routing.module.ts":
/*!**********************************************************!*\
  !*** ./projects/factoring/src/app/app-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/login/login.component */ "./projects/factoring/src/app/components/login/login.component.ts");
/* harmony import */ var _components_client_client_home_client_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/client/client-home/client-home.component */ "./projects/factoring/src/app/components/client/client-home/client-home.component.ts");
/* harmony import */ var _components_admin_admin_home_admin_home_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/admin/admin-home/admin-home.component */ "./projects/factoring/src/app/components/admin/admin-home/admin-home.component.ts");
/* harmony import */ var _components_bank_bank_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/bank/bank.component */ "./projects/factoring/src/app/components/bank/bank.component.ts");







var routes = [
    { path: 'factoring/login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'factoring', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'factoring/client', component: _components_client_client_home_client_home_component__WEBPACK_IMPORTED_MODULE_4__["ClientHomeComponent"] },
    { path: 'factoring/admin', component: _components_admin_admin_home_admin_home_component__WEBPACK_IMPORTED_MODULE_5__["AdminHomeComponent"] },
    { path: 'factoring/bank', component: _components_bank_bank_component__WEBPACK_IMPORTED_MODULE_6__["BankComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./projects/factoring/src/app/app.component.css":
/*!******************************************************!*\
  !*** ./projects/factoring/src/app/app.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9qZWN0cy9mYWN0b3Jpbmcvc3JjL2FwcC9hcHAuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./projects/factoring/src/app/app.component.html":
/*!*******************************************************!*\
  !*** ./projects/factoring/src/app/app.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./projects/factoring/src/app/app.component.ts":
/*!*****************************************************!*\
  !*** ./projects/factoring/src/app/app.component.ts ***!
  \*****************************************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'factoring';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./projects/factoring/src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./projects/factoring/src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./projects/factoring/src/app/app.module.ts":
/*!**************************************************!*\
  !*** ./projects/factoring/src/app/app.module.ts ***!
  \**************************************************/
/*! exports provided: AppModule, App3SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "App3SharedModule", function() { return App3SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app-routing.module */ "./projects/factoring/src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app.component */ "./projects/factoring/src/app/app.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var angular2_materialize__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! angular2-materialize */ "./node_modules/angular2-materialize/dist/index.js");
/* harmony import */ var _services_companies_service__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./services/companies.service */ "./projects/factoring/src/app/services/companies.service.ts");
/* harmony import */ var _services_invoices_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./services/invoices.service */ "./projects/factoring/src/app/services/invoices.service.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! ./services/login.service */ "./projects/factoring/src/app/services/login.service.ts");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./components/login/login.component */ "./projects/factoring/src/app/components/login/login.component.ts");
/* harmony import */ var _components_client_client_home_client_home_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./components/client/client-home/client-home.component */ "./projects/factoring/src/app/components/client/client-home/client-home.component.ts");
/* harmony import */ var _components_admin_admin_home_admin_home_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./components/admin/admin-home/admin-home.component */ "./projects/factoring/src/app/components/admin/admin-home/admin-home.component.ts");
/* harmony import */ var _components_admin_manage_companies_manage_companies_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./components/admin/manage-companies/manage-companies.component */ "./projects/factoring/src/app/components/admin/manage-companies/manage-companies.component.ts");
/* harmony import */ var _components_bank_bank_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./components/bank/bank.component */ "./projects/factoring/src/app/components/bank/bank.component.ts");
/* harmony import */ var _components_nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_17__ = __webpack_require__(/*! ./components/nav-bar/nav-bar.component */ "./projects/factoring/src/app/components/nav-bar/nav-bar.component.ts");
/* harmony import */ var _components_client_invoices_list_invoices_list_component__WEBPACK_IMPORTED_MODULE_18__ = __webpack_require__(/*! ./components/client/invoices-list/invoices-list.component */ "./projects/factoring/src/app/components/client/invoices-list/invoices-list.component.ts");
/* harmony import */ var materialize_css__WEBPACK_IMPORTED_MODULE_19__ = __webpack_require__(/*! materialize-css */ "./node_modules/materialize-css/dist/js/materialize.js");
/* harmony import */ var materialize_css__WEBPACK_IMPORTED_MODULE_19___default = /*#__PURE__*/__webpack_require__.n(materialize_css__WEBPACK_IMPORTED_MODULE_19__);




















var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_12__["LoginComponent"],
                _components_client_client_home_client_home_component__WEBPACK_IMPORTED_MODULE_13__["ClientHomeComponent"],
                _components_admin_admin_home_admin_home_component__WEBPACK_IMPORTED_MODULE_14__["AdminHomeComponent"],
                _components_admin_manage_companies_manage_companies_component__WEBPACK_IMPORTED_MODULE_15__["ManageCompaniesComponent"],
                _components_bank_bank_component__WEBPACK_IMPORTED_MODULE_16__["BankComponent"],
                _components_nav_bar_nav_bar_component__WEBPACK_IMPORTED_MODULE_17__["NavBarComponent"],
                _components_client_invoices_list_invoices_list_component__WEBPACK_IMPORTED_MODULE_18__["InvoicesListComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_3__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_5__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["FormsModule"],
                _angular_http__WEBPACK_IMPORTED_MODULE_7__["JsonpModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_6__["ReactiveFormsModule"],
                angular2_materialize__WEBPACK_IMPORTED_MODULE_8__["MaterializeModule"]
            ],
            providers: [
                _services_companies_service__WEBPACK_IMPORTED_MODULE_9__["ServiceCompanies"],
                _services_invoices_service__WEBPACK_IMPORTED_MODULE_10__["ServiceInvoices"],
                _services_login_service__WEBPACK_IMPORTED_MODULE_11__["ServiceLogin"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_4__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());

var App3SharedModule = /** @class */ (function () {
    function App3SharedModule() {
    }
    App3SharedModule.forRoot = function () {
        return {
            ngModule: AppModule,
            providers: [
                _services_companies_service__WEBPACK_IMPORTED_MODULE_9__["ServiceCompanies"],
                _services_invoices_service__WEBPACK_IMPORTED_MODULE_10__["ServiceInvoices"],
                _services_login_service__WEBPACK_IMPORTED_MODULE_11__["ServiceLogin"]
            ]
        };
    };
    App3SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({})
    ], App3SharedModule);
    return App3SharedModule;
}());



/***/ }),

/***/ "./projects/factoring/src/app/components/admin/admin-home/admin-home.component.html":
/*!******************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/admin/admin-home/admin-home.component.html ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n<app-nav-bar *ngIf=\"appTitle\" [title]=\"appTitle\"></app-nav-bar>\n<div class=\"full-body\">\n    <app-manage-companies></app-manage-companies>\n</div>\n\n"

/***/ }),

/***/ "./projects/factoring/src/app/components/admin/admin-home/admin-home.component.scss":
/*!******************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/admin/admin-home/admin-home.component.scss ***!
  \******************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-body {\n  padding-top: 20px;\n  height: 90%;\n  overflow: auto;\n  padding: 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2ZhY3RvcmluZy9zcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vYWRtaW4taG9tZS9DOlxcVXNlcnNcXG1paGFpLmFuZHJlaVxcT25lRHJpdmUgLSBVaVBhdGhcXFVpUGF0aFxccHJvamVjdHNcXF9JTlRFUk5BTF9SUEFQb3J0YWxcXFBvcnRhbC9wcm9qZWN0c1xcZmFjdG9yaW5nXFxzcmNcXGFwcFxcY29tcG9uZW50c1xcYWRtaW5cXGFkbWluLWhvbWVcXGFkbWluLWhvbWUuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxpQkFBaUI7RUFDakIsV0FBVTtFQUNWLGNBQWM7RUFDZCxhQUFZLEVBQUEiLCJmaWxlIjoicHJvamVjdHMvZmFjdG9yaW5nL3NyYy9hcHAvY29tcG9uZW50cy9hZG1pbi9hZG1pbi1ob21lL2FkbWluLWhvbWUuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnVsbC1ib2R5e1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICBoZWlnaHQ6OTAlO1xyXG4gICAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgICBwYWRkaW5nOjMwcHg7XHJcbiAgfVxyXG4gIFxyXG4gIFxyXG4gICJdfQ== */"

/***/ }),

/***/ "./projects/factoring/src/app/components/admin/admin-home/admin-home.component.ts":
/*!****************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/admin/admin-home/admin-home.component.ts ***!
  \****************************************************************************************/
/*! exports provided: AdminHomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminHomeComponent", function() { return AdminHomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../services/login.service */ "./projects/factoring/src/app/services/login.service.ts");
/* harmony import */ var _services_companies_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../services/companies.service */ "./projects/factoring/src/app/services/companies.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");





var AdminHomeComponent = /** @class */ (function () {
    function AdminHomeComponent(srvLogin, srvCompany) {
        this.srvLogin = srvLogin;
        this.srvCompany = srvCompany;
        this.appTitle = "Admin Control Room";
    }
    AdminHomeComponent.prototype.ngOnInit = function () {
        this.user = this.srvLogin.getUser();
        this.srvCompany.getAllCompanies();
        this.inputForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            userName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            companyName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
        });
    };
    AdminHomeComponent.prototype.disconnectUser = function () {
        this.srvLogin.disconnectUser();
    };
    AdminHomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-admin-home',
            template: __webpack_require__(/*! ./admin-home.component.html */ "./projects/factoring/src/app/components/admin/admin-home/admin-home.component.html"),
            styles: [__webpack_require__(/*! ./admin-home.component.scss */ "./projects/factoring/src/app/components/admin/admin-home/admin-home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_login_service__WEBPACK_IMPORTED_MODULE_2__["ServiceLogin"],
            _services_companies_service__WEBPACK_IMPORTED_MODULE_3__["ServiceCompanies"]])
    ], AdminHomeComponent);
    return AdminHomeComponent;
}());



/***/ }),

/***/ "./projects/factoring/src/app/components/admin/manage-companies/manage-companies.component.html":
/*!******************************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/admin/manage-companies/manage-companies.component.html ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\n  <p>\n    List of Available Companies\n  </p>\n  <div *ngFor=\"let company of companies\">\n    <a (click)=\"openCompany(company)\">{{company.companyName}}</a>\n  </div>\n  <br>\n  <a class=\"waves-effect col s12 m12 l12 btn uiColorButton\" (click)=\"openModal()\">Add a Company</a>\n\n  <div id=\"modal\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalActions\">\n    <div class=\"modal-content\">\n      <div style=\"padding-bottom: 20px\">\n        <span style=\"font-weight:bold;font-size:24px;padding: 10px; \">Create a new Company</span>\n        <i class=\"material-icons right modal-action modal-close\" (click)=\"closeModal()\">close</i>\n      </div>\n\n      <div [formGroup]=\"inputCompanyForm\">\n        <label class=\"col s5 m5 l5\">Company Name: <input name=\"companyName\" formControlName=\"companyName\"></label>\n        <div class=\"row\">\n          <label class=\"col s5 m5 l5\">Company limit: <input name=\"companyLimit\" formControlName=\"companyLimit\"></label>\n          <div class=\"col s2 m2 l2\"></div>\n          <label class=\"col s5 m5 l5\">Buyer Risk (%): <input name=\"buyerRisk\" formControlName=\"buyerRisk\"></label>\n        </div>\n        \n        <input type=\"checkbox\" [checked]=\"labBank\" id=\"labBank\" name=\"labBank\" />\n        <label for=\"labBank\">Is Immersion Lab bank</label>\n\n        <div class=\" file-field input-field col s12 m12 l6 \">\n          <div *ngIf=\"fileUploaded==false\" class=\"btn right uiColorSecondary\">\n            \n            <span>Upload Logo</span>\n            <input (change)=\"handleFileInput($event.target.files)\" type=\"file\">\n          </div>\n\n          <div *ngIf=\"fileUploaded\" class=\"row\" style=\"padding-top: 20px;\"><br><br>\n            <a class=\"waves-effect col s12 m12 l12 btn uiColorButton\" (click)=\"createCompany()\">Submit</a>\n          </div>\n\n          <div *ngIf=\"!fileUploaded\" class=\"row\" style=\"padding-top: 20px;\"><br><br>\n            <a class=\"waves-effect col s12 m12 l12 btn uiColorButton disabled\" (click)=\"createCompany()\">Submit</a>\n          </div>\n\n          <span *ngIf=\"companyExists\" style=\"color:red;\">A company with the same name already exists</span>\n        </div>\n      </div>\n\n\n    </div>\n  </div>\n\n  <div *ngIf=\"selectedCompany\" id=\"modal1\" class=\"modal\" materialize=\"modal\"\n    [materializeActions]=\"updateCompanyActions\">\n    <div class=\"modal-content\">\n      <div style=\"padding-bottom: 20px\">\n        <span style=\"font-weight:bold;font-size:24px;padding: 10px; \">Update company {{selectedCompany.companyName}}\n        </span>\n        <i class=\"material-icons right modal-action modal-close\" (click)=\"closeModal()\">close</i>\n        <i class=\"material-icons right modal-action modal-close\" (click)=\"deleteCompany()\">delete</i>\n        <div class=\"row class col s4 m4 l4\"><img src=\"{{logoImage}}{{selectedCompany.companyLogo}}\"\n            style=\"width:7rem; padding-top: 2px;\" /></div>\n\n      </div>\n      <span>ID: {{selectedCompany._id}}</span>\n      <div style=\"padding-top: 10px;\" [formGroup]=\"inputCompanyForm\">\n        <label class=\"col s5 m5 l5\">Company Name: <input name=\"companyName\" [(ngModel)]=\"selectedCompany.companyName\"\n            value='{{selectedCompany.companyName}}' formControlName=\"companyName\"></label>\n        <div class=\"row\">\n          <label class=\"col s5 m5 l5\">Company limit: <input name=\"companyLimit\"\n              [(ngModel)]=\"selectedCompany.companyLimit\" value='{{selectedCompany.companyLimit}}'\n              formControlName=\"companyLimit\"></label>\n          <div class=\"col s2 m2 l2\"></div>\n          <label class=\"col s5 m5 l5\">Buyer Risk (%): <input name=\"buyerRisk\" [(ngModel)]=\"selectedCompany.buyerRisk\"\n              value='{{selectedCompany.buyerRisk}}' formControlName=\"buyerRisk\"></label>\n        </div>\n\n        <div class=\" file-field input-field col s12 m12 l6 \">\n          <div *ngIf=\"fileUploaded===false\" class=\"btn right uiColorSecondary\">\n            <span>Upload Logo</span>\n            <input (change)=\"handleFileInput($event.target.files)\" type=\"file\">\n          </div>\n\n\n          <div class=\"row\" style=\"padding-top: 20px;\"><br><br>\n            <a class=\"waves-effect col s12 m12 l12 btn uiColorButton\" (click)=\"updateCompany()\">Update</a>\n          </div>\n          <span *ngIf=\"companyExists\" style=\"color:red;\">A company with the same name already exists</span>\n        </div>\n      </div>\n\n\n    </div>\n  </div>\n\n\n\n  <!-- <div [formGroup]=\"inputCompanyName\">\n    <label class=\"col s5 m5 l5\">User Name: <input name=\"userName\" formControlName=\"userName\"></label>\n    <div class=\"row\"></div>\n    <label class=\"col s5 m5 l5\">Password <input type=\"password\" [(ngModel)]=\"password\"\n        [ngModelOptions]=\"{standalone: true}\"></label>\n    <div class=\"col s2 m2 l2\"></div>\n    <label class=\"col s5 m5 l5\">Confirm your Password <input type=\"password\" [(ngModel)]=\"confirmPassword\"\n        [ngModelOptions]=\"{standalone: true}\"></label>\n    <div *ngIf=\"password!=confirmPassword\" class=\"row\">\n      <span style=\"color:red\">passwords do not match</span>\n    </div>\n  </div> -->"

/***/ }),

/***/ "./projects/factoring/src/app/components/admin/manage-companies/manage-companies.component.scss":
/*!******************************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/admin/manage-companies/manage-companies.component.scss ***!
  \******************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".instructionsText {\n  color: aliceblue;\n  font-size: 16px;\n  padding-top: 20px; }\n\n.instructionsDiv {\n  height: 110vh;\n  background-size: 100%; }\n\n.uiColorButton {\n  background-color: RGB(254, 80, 0); }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2ZhY3RvcmluZy9zcmMvYXBwL2NvbXBvbmVudHMvYWRtaW4vbWFuYWdlLWNvbXBhbmllcy9DOlxcVXNlcnNcXG1paGFpLmFuZHJlaVxcT25lRHJpdmUgLSBVaVBhdGhcXFVpUGF0aFxccHJvamVjdHNcXF9JTlRFUk5BTF9SUEFQb3J0YWxcXFBvcnRhbC9wcm9qZWN0c1xcZmFjdG9yaW5nXFxzcmNcXGFwcFxcY29tcG9uZW50c1xcYWRtaW5cXG1hbmFnZS1jb21wYW5pZXNcXG1hbmFnZS1jb21wYW5pZXMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQ0E7RUFDRSxnQkFBZ0I7RUFDaEIsZUFBYztFQUNkLGlCQUFpQixFQUFBOztBQUduQjtFQUNFLGFBQWE7RUFDYixxQkFBcUIsRUFBQTs7QUFHdkI7RUFDRSxpQ0FBK0IsRUFBQSIsImZpbGUiOiJwcm9qZWN0cy9mYWN0b3Jpbmcvc3JjL2FwcC9jb21wb25lbnRzL2FkbWluL21hbmFnZS1jb21wYW5pZXMvbWFuYWdlLWNvbXBhbmllcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxyXG4uaW5zdHJ1Y3Rpb25zVGV4dHtcclxuICBjb2xvcjogYWxpY2VibHVlO1xyXG4gIGZvbnQtc2l6ZToxNnB4O1xyXG4gIHBhZGRpbmctdG9wOiAyMHB4O1xyXG59XHJcblxyXG4uaW5zdHJ1Y3Rpb25zRGl2e1xyXG4gIGhlaWdodDogMTEwdmg7XHJcbiAgYmFja2dyb3VuZC1zaXplOiAxMDAlO1xyXG59XHJcblxyXG4udWlDb2xvckJ1dHRvbntcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiBSR0IoMjU0LDgwLDApO1xyXG59XHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./projects/factoring/src/app/components/admin/manage-companies/manage-companies.component.ts":
/*!****************************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/admin/manage-companies/manage-companies.component.ts ***!
  \****************************************************************************************************/
/*! exports provided: ManageCompaniesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ManageCompaniesComponent", function() { return ManageCompaniesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_companies_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../services/companies.service */ "./projects/factoring/src/app/services/companies.service.ts");
/* harmony import */ var _models_companies_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../models/companies.model */ "./projects/factoring/src/app/models/companies.model.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models_user_model__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../../models/user.model */ "./projects/factoring/src/app/models/user.model.ts");







var ManageCompaniesComponent = /** @class */ (function () {
    function ManageCompaniesComponent(srvCompanies) {
        this.srvCompanies = srvCompanies;
        this.modalActions = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.updateCompanyActions = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.selectedCompany = new _models_companies_model__WEBPACK_IMPORTED_MODULE_3__["CompanyModel"]('', '', '', '', '', '');
        this.labBank = false;
        this.password = '';
        this.confirmPassword = '';
        this.companyExists = false;
        this.fileUploaded = false;
        this.logoImage = 'https://uipath509.westeurope.cloudapp.azure.com/api/v1/getLogo?logoName=';
    }
    ManageCompaniesComponent.prototype.ngOnInit = function () {
        this.getAllCompanies();
        this.inputCompanyForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            companyName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            companyLogo: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            companyLimit: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            buyerRisk: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            bankId: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
        });
        this.inputUserForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            userName: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
        });
    };
    ManageCompaniesComponent.prototype.getAllCompanies = function () {
        var _this = this;
        this.srvCompanies.getAllCompanies().then(function (result) {
            _this.companies = result;
        });
    };
    ManageCompaniesComponent.prototype.openCompany = function (company) {
        this.selectedCompany = company;
        this.fileUploaded = false;
        this.updateCompanyActions.emit({ action: 'modal', params: ['open'] });
    };
    ManageCompaniesComponent.prototype.closeModalUpdate = function () {
        this.updateCompanyActions.emit({ action: 'modal', params: ['close'] });
    };
    ManageCompaniesComponent.prototype.openModal = function () {
        this.labBank = false;
        this.inputCompanyForm.reset();
        this.fileUploaded = false;
        this.modalActions.emit({ action: 'modal', params: ['open'] });
    };
    ManageCompaniesComponent.prototype.closeModal = function () {
        this.modalActions.emit({ action: 'modal', params: ['close'] });
    };
    ManageCompaniesComponent.prototype.handleFileInput = function (files) {
        this.fileToUpload = files.item(0);
        this.fileUploaded = true;
    };
    ManageCompaniesComponent.prototype.createCompany = function () {
        var _this = this;
        var compName = this.inputCompanyForm.controls['companyName'].value;
        if (this.companies.filter(function (x) { return x.companyName == compName; }).length > 0) {
            this.companyExists = true;
        }
        else {
            this.companyExists = false;
            var compLogo = 'default.png';
            if (this.fileUploaded)
                compLogo = this.fileToUpload.name;
            this.srvCompanies.postLogo(this.fileToUpload);
            var bankId = 0;
            if (this.labBank) {
                bankId = 1;
            }
            this.srvCompanies.postCompany(new _models_companies_model__WEBPACK_IMPORTED_MODULE_3__["CompanyModel"](compName, '', compLogo, this.inputCompanyForm.controls['companyLimit'].value, this.inputCompanyForm.controls['buyerRisk'].value, bankId.toString())).then(function (res) {
                _this.getAllCompanies();
                _this.inputCompanyForm.reset();
                _this.closeModal();
                _this.fileUploaded = false;
                _this.srvCompanies.getCompanyByName(compName).then(function (res) {
                    var user = new _models_user_model__WEBPACK_IMPORTED_MODULE_5__["UserModel"](compName, compName, res._id, 'client');
                    console.log(user);
                    _this.srvCompanies.postUser(user);
                });
            });
        }
    };
    ManageCompaniesComponent.prototype.deleteCompany = function () {
        this.srvCompanies.deleteCompany(this.selectedCompany);
        this.getAllCompanies();
    };
    ManageCompaniesComponent.prototype.updateCompany = function () {
        if (this.fileUploaded) {
            var compLogo = this.fileToUpload.name;
            this.selectedCompany.companyLogo = compLogo;
            this.srvCompanies.postLogo(this.fileToUpload);
        }
        this.closeModalUpdate();
        this.srvCompanies.updateCompany(this.selectedCompany);
    };
    ManageCompaniesComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-manage-companies',
            template: __webpack_require__(/*! ./manage-companies.component.html */ "./projects/factoring/src/app/components/admin/manage-companies/manage-companies.component.html"),
            styles: [__webpack_require__(/*! ./manage-companies.component.scss */ "./projects/factoring/src/app/components/admin/manage-companies/manage-companies.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_companies_service__WEBPACK_IMPORTED_MODULE_2__["ServiceCompanies"]])
    ], ManageCompaniesComponent);
    return ManageCompaniesComponent;
}());



/***/ }),

/***/ "./projects/factoring/src/app/components/bank/bank.component.html":
/*!************************************************************************!*\
  !*** ./projects/factoring/src/app/components/bank/bank.component.html ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-nav-bar [title]=\"appTitle\" [type]=\"clientType\"></app-nav-bar>\n\n<div class=\"full-body\">\n  <div class=\"z-depth-2 row loginForm header\">\n    <div class=\"row\">\n\n      <div *ngIf=\"invoices && companies\" class=\"col s12 m12 l2 \">\n        <div class=\"row \"><span style=\"font-weight: bold\">Number of Companies: </span>\n          <span>{{companies.length}}</span>\n        </div>\n        <div><span style=\"font-weight: bold\">Number of Invoices: </span>\n          <span id=\"totalInvoices\">{{serverInvoices.length}}</span>\n        </div><br>\n        <a (click)=\"sortInvoiceList('Pending')\" class=\"row \"><span\n            style=\"font-weight: bold; color: orange; padding-right:5px;\">Pending:</span>\n          <span id=\"pendingNumber\">{{pendingNo}}</span>\n        </a>\n\n        <a (click)=\"sortInvoiceList('Financed')\" class=\"row\"><span\n            style=\"font-weight: bold; color: green; padding-right:5px;\">Financed:</span>\n          <span id=\"financedNumber\">{{financedNo}}</span>\n        </a>\n       \n\n        <a (click)=\"sortInvoiceList('Reimbursed')\" class=\"row \">\n          <span style=\"font-weight: bold; color: blue; padding-right:5px;\">Reimbursed:</span>\n          <span id=\"reimbursedNo\">{{reimbursedNo}}</span>\n        </a>\n        <a (click)=\"sortInvoiceList('ForApproval')\" class=\"row\"><span\n          style=\"font-weight: bold; color: teal; padding-right:5px;\">For Approval:</span>\n        <span id=\"financedNumber\">{{forApproval}}</span>\n       </a>\n        <a (click)=\"sortInvoiceList('Refused')\" class=\"row \"><span\n            style=\"font-weight: bold; color: red; padding-right:5px;\">Refused:</span>\n          <span id=\"refusedNo\">{{refusedNo}}</span></a>\n      </div>\n\n      <div class=\"col s10 m10 l10 companies\">\n        <div *ngFor=\"let company of companies; let i=index \" class=\"col companyCard z-depth-3\">\n          <div class=\"row class col s4 m4 l4\"><img src=\"{{logoImage}}{{company.companyLogo}}\"\n              style=\"width:7rem; padding-top: 2px;\" /></div>\n\n          <div class=\"row class col s7 m7 l7\" id=\"{{i}}\" style=\"padding-left:15px;\">\n            <a (click)=\"searchCompany(company.companyName)\"><span class=\"right\"\n                style=\"font-weight: bold; font-size: 20px;\">{{company.companyName}}</span></a>\n            <div class=\"row\" style=\"padding-top:35px;\">\n              <span style=\"font-weight: bold; font-size: 15px;\">Company Limit: </span>\n              <span style=\" font-size: 15px; \">{{company.companyLimit}}</span>\n            </div>\n            <div class=\"row\">\n              <span style=\"font-weight: bold; font-size: 15px;\">Risk per Buyer: </span>\n              <span style=\"font-size: 15px; \" id=\"'buyerRisk'+{{i}}\">{{company.buyerRisk}}%</span>\n            </div>\n\n          </div>\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n  <div style=\"padding-top:20px\"></div>\n  <div class=\"center-align z-depth-2 row loginForm\">\n    <div class=\"col s12 m12 l12 center-align\" style=\"padding-bottom: 12px;\">\n      <div class=\"col s4 m4 l4\"></div>\n      <form class=\"col s8 m8 l8\">\n        <input type=\"text\" class=\" col s4 m4 l4\" name=\"name\" #searchCompanyValue placeholder=\"Search a company\">\n        <button (click)=\"searchCompany(searchCompanyValue.value)\" style=\"margin-left:25px;\"\n          class=\" left uiColorPrimary btn btn-primary btn-small\">Search</button>\n        <button style=\"margin-left: 10px;\" (click)=\"getAllInvoices()\"\n          class=\" uiColorPrimary left btn btn-primary btn-small\">Get All Invoices</button>\n      </form>\n\n    </div>\n    <div class=\"row\">\n      <div class=\"col s12 m12 l12\">\n        <table class=\"striped\" id=\"invoiceTable\">\n          <thead>\n            <tr>\n              <th>ID</th>\n              <th>Company Name</th>\n              <th>Seller Name</th>\n              <th>Buyer Name</th>\n              <th>Request Date</th>\n              <th>Invoice No</th>\n              <th>Invoice Amount</th>\n              <th>Financed Amount</th>\n              <th>Risk Per Buyer</th>\n              <th>Credit Bureau</th>\n              <th>Duplicity Check</th>\n              <th>Status</th>\n              <th>Comment</th>\n            </tr>\n          </thead>\n          <tbody>\n            <tr *ngFor=\"let invoice of invoices ; let i=index\">\n              <th>\n                {{i + 1}}\n              </th>\n              <th>\n                {{invoice.companyName}}\n              </th>\n              <th>\n                {{invoice.sellerName}}\n              </th>\n              <th>\n                {{invoice.buyerName}}\n              </th>\n              <th>\n                {{invoice.requestDate | date}}\n              </th>\n              <th>\n                {{invoice.invoiceNo}}\n              </th>\n              <th>\n                {{invoice.invoiceAmount}}\n              </th>\n              <th>\n                {{invoice.financedAmount}}\n              </th>\n              <th class=\"center-align\">\n                <input class=\"center-align\" type=\"checkbox\" id=\"'risk'+{{i}}\" [checked]=\"invoice.buyerRisk\"\n                  (change)=\"invoice.buyerRisk = !invoice.buyerRisk\" />\n                <label for=\"'risk'+{{i}}\" class=\"black-text flow-text\"> </label>\n              </th>\n              <th class=\"center-align\">\n                <input class=\"center-align\" type=\"checkbox\" id=\"'creditBureau'+{{i}}\" [checked]=\"invoice.creditBureau\"\n                  (change)=\"invoice.creditBureau = !invoice.creditBureau\" />\n                <label for=\"'creditBureau'+{{i}}\" class=\"black-text flow-text\"> </label>\n              </th>\n              <th class=\"center-align\">\n                <input type=\"checkbox\" id=\"{{invoice._id}}\" [checked]=\"invoice.duplicity\"\n                  (change)=\"invoice.duplicity = !invoice.duplicity\" />\n                <label for=\"{{invoice._id}}\" class=\"black-text flow-text\"> </label>\n              </th>\n              <th>\n                {{invoice.status}}\n              </th>\n              <th>\n                {{invoice.comment}}\n              </th>\n              <th>\n                <a *ngIf=\"invoice.status==='Pending' || invoice.status==='ForApproval'\" (click)=\"editInvoice(invoice)\"><i class=\"material-icons\"\n                    style=\"color: blue\">edit</i></a>\n                <a *ngIf=\"invoice.status==='Financed'\" (click)=\"reimbursedOFfer(invoice)\"><i class=\"material-icons\"\n                    style=\"color: green\">done</i></a>\n              </th>\n            </tr>\n          </tbody>\n        </table>\n      </div>\n    </div>\n  </div>\n</div>\n\n\n\n\n<div id=\"modal1\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalActions\">\n  <div class=\"modal-content\">\n    <div style=\"padding-bottom: 20px\">\n      <span style=\"font-weight:bold;font-size:24px;padding: 13px; \">Create Client Offer for -\n        {{selectedInvoice.companyName}}</span>\n\n      <i class=\"material-icons right modal-action modal-close \" (click)=\"closeModal()\">close</i>\n      <i class=\"material-icons right modal-action modal-close \" (click)=\"saveChanges()\">save</i>\n      <i class=\"material-icons right modal-action modal-close\"  (click)=\"sentToApproval()\">account_circle</i>\n\n    </div>\n    <div [formGroup]=\"inputForm\" class=\"row\">\n      <div class=\"col s12 m12 l12\">\n        <div class=\"row col s12 m12 l12\">\n          <div class=\"col s5 m5 l5\">\n            <span style=\"font-weight: bold; padding-right: 15px;\">Credit Limit Per Company:</span> <span\n              id=\"creditLimit\">{{companyLimit}}</span>\n          </div>\n          <div class=\"col s1 m1 l1\"></div>\n          <span class=\"col s5 m5 l5\"><span style=\"font-weight: bold; padding-right: 15px; \">Request made on:\n            </span>{{selectedInvoice.requestDate | date }}</span>\n        </div>\n\n        <div class=\"col s12 m12 l12\" style=\"padding-top: 40px; padding-bottom: 25px;\">\n          <span class=\"col s4 m4 l4 left\" style=\"font-weight:bold;font-size:16px; \">Invoice Details </span>\n        </div>\n        <div class=\"col s12\">\n          <label for=\"companyName\">Seller Name</label>\n          <input name=\"companyName\" [(ngModel)]=\"selectedInvoice.sellerName\" value='{{selectedInvoice.sellerName}}'\n            formControlName=\"sellerName\">\n        </div>\n\n        <div class=\"col s12 m12 l4\">\n          <label for=\"invoiceNo\">Invoice Number</label>\n          <input name=\"invoiceNo\" [(ngModel)]=\"selectedInvoice.invoiceNo\" value='{{selectedInvoice.invoiceNo}}'\n            formControlName=\"invoiceNo\">\n        </div>\n\n        <div class=\"col s12 m12 l4\">\n          <label for=\"buyerName\">Buyer Name</label>\n          <input name=\"buyerName\" [(ngModel)]=\"selectedInvoice.buyerName\" value='{{selectedInvoice.buyerName}}'\n            formControlName=\"buyerName\">\n        </div>\n\n\n        <div class=\"col s12 m12 l4\">\n          <label for=\"invoiceDate\">Invoice Date</label>\n          <input name=\"invoiceDate\" [(ngModel)]=\"selectedInvoice.invoiceDate\" value='{{selectedInvoice.invoiceDate}}'\n            formControlName=\"invoiceDate\">\n        </div>\n\n        <div class=\"col s12 m12 l4\">\n          <label for=\"invoiceAmount\">Requested Amount</label>\n          <input name=\"invoiceAmount\" [(ngModel)]=\"selectedInvoice.invoiceAmount\"\n            value='{{selectedInvoice.invoiceAmount}}' formControlName=\"invoiceAmount\">\n\n        </div>\n\n        <div class=\"col s12 m12 l4\">\n          <label for=\"financedAmount\">Financed Amount</label>\n          <input name=\"financedAmount\" [(ngModel)]=\"selectedInvoice.financedAmount\"\n            value='{{selectedInvoice.financedAmount}}' formControlName=\"financedAmount\">\n        </div>\n\n        <div class=\"col s12 m12 l4\">\n          <label for=\"comment\">Comment</label>\n          <input name=\"comment\" [(ngModel)]=\"selectedInvoice.comment\" value='{{selectedInvoice.comment}}'\n            formControlName=\"comment\">\n        </div>\n\n        <div class=\"row col s12 m12 l12\" style=\"padding-top:20px;\">\n          <div class=\"col s3 m3 l3\">\n            <input class=\"center-align\" type=\"checkbox\" id=\"{{selectedInvoice.comment}}\"\n              [checked]=\"selectedInvoice.buyerRisk\" (change)=\"selectedInvoice.buyerRisk = !selectedInvoice.buyerRisk\" />\n            <label for=\"{{selectedInvoice.comment}}\" class=\"black-text flow-text\">Buyer Risk</label>\n          </div>\n          <div class=\"col s3 m3 l3\">\n            <input class=\"center-align\" type=\"checkbox\" id=\"{{selectedInvoice.invoiceFile}}\"\n              [checked]=\"selectedInvoice.creditBureau\"\n              (change)=\"selectedInvoice.creditBureau = !selectedInvoice.creditBureau\" />\n            <label for=\"{{selectedInvoice.invoiceFile}}\" class=\"black-text flow-text\">Credit Bureau Check </label>\n          </div>\n          <div class=\"col s3 m3 l3\">\n            <input type=\"checkbox\" id=\"{{selectedInvoice._id}}\" [checked]=\"selectedInvoice.duplicity\"\n              (change)=\"selectedInvoice.duplicity = !selectedInvoice.duplicity\" />\n            <label for=\"{{selectedInvoice._id}}\" class=\"black-text flow-text\">Duplicity Check</label>\n          </div>\n        </div>\n\n\n\n        <div class=\"col s12 m12 l12\" style=\"margin-top: 30px;\">\n          <div class=\"col s12 m12 l3\">\n            <a href=\"https://uipath509.westeurope.cloudapp.azure.com:443/api/v1/downloadFile/?fileName={{selectedInvoice.invoiceFile}}\"\n              class=\"uiColorSecondary btn btn-primary btn-lg\">Download Invoice</a>\n          </div>\n\n          <div class=\"col s12 m12 l3\">\n            <a class=\"uiColorPrimary btn btn-primary btn\" (click)=\"submitOffer()\">Submit Offer</a>\n          </div>\n\n\n\n          <div class=\"col s12 m12 l6\">\n            <a class=\"uiColorPrimary right btn btn-primary btn\" (click)=\"cancelOffer()\">Refuse Offer</a>\n          </div>\n        </div>\n\n      </div>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./projects/factoring/src/app/components/bank/bank.component.scss":
/*!************************************************************************!*\
  !*** ./projects/factoring/src/app/components/bank/bank.component.scss ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-body {\n  padding-top: 20px;\n  height: 90%;\n  overflow: auto; }\n\n.loginForm {\n  padding: 30px;\n  max-width: 95%; }\n\n.row {\n  margin-bottom: 0px; }\n\n.companyCard {\n  margin-left: 20px;\n  min-width: 400px;\n  padding-bottom: 30px;\n  min-height: 150px; }\n\n.companies {\n  padding-top: 10px;\n  display: -webkit-box;\n  display: flex;\n  overflow-x: auto;\n  padding-bottom: 30px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2ZhY3RvcmluZy9zcmMvYXBwL2NvbXBvbmVudHMvYmFuay9DOlxcVXNlcnNcXG1paGFpLmFuZHJlaVxcT25lRHJpdmUgLSBVaVBhdGhcXFVpUGF0aFxccHJvamVjdHNcXF9JTlRFUk5BTF9SUEFQb3J0YWxcXFBvcnRhbC9wcm9qZWN0c1xcZmFjdG9yaW5nXFxzcmNcXGFwcFxcY29tcG9uZW50c1xcYmFua1xcYmFuay5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFpQjtFQUNqQixXQUFVO0VBQ1YsY0FBYyxFQUFBOztBQUdoQjtFQUNFLGFBQVk7RUFDWixjQUFhLEVBQUE7O0FBR2Y7RUFDRSxrQkFBa0IsRUFBQTs7QUFHcEI7RUFDRSxpQkFBaUI7RUFDakIsZ0JBQWdCO0VBQ2hCLG9CQUFvQjtFQUNwQixpQkFBaUIsRUFBQTs7QUFLbkI7RUFDRSxpQkFBaUI7RUFDakIsb0JBQWE7RUFBYixhQUFhO0VBQ2IsZ0JBQWdCO0VBQ2hCLG9CQUFvQixFQUFBIiwiZmlsZSI6InByb2plY3RzL2ZhY3RvcmluZy9zcmMvYXBwL2NvbXBvbmVudHMvYmFuay9iYW5rLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLmZ1bGwtYm9keXtcclxuICBwYWRkaW5nLXRvcDogMjBweDtcclxuICBoZWlnaHQ6OTAlO1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG59XHJcblxyXG4ubG9naW5Gb3JtIHtcclxuICBwYWRkaW5nOjMwcHg7XHJcbiAgbWF4LXdpZHRoOjk1JTtcclxufVxyXG5cclxuLnJvdyB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG59XHJcblxyXG4uY29tcGFueUNhcmR7XHJcbiAgbWFyZ2luLWxlZnQ6IDIwcHg7XHJcbiAgbWluLXdpZHRoOiA0MDBweDtcclxuICBwYWRkaW5nLWJvdHRvbTogMzBweDtcclxuICBtaW4taGVpZ2h0OiAxNTBweDtcclxuXHJcbn1cclxuXHJcblxyXG4uY29tcGFuaWVze1xyXG4gIHBhZGRpbmctdG9wOiAxMHB4O1xyXG4gIGRpc3BsYXk6IGZsZXg7XHJcbiAgb3ZlcmZsb3cteDogYXV0bztcclxuICBwYWRkaW5nLWJvdHRvbTogMzBweDtcclxuXHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./projects/factoring/src/app/components/bank/bank.component.ts":
/*!**********************************************************************!*\
  !*** ./projects/factoring/src/app/components/bank/bank.component.ts ***!
  \**********************************************************************/
/*! exports provided: BankComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "BankComponent", function() { return BankComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_invoices_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/invoices.service */ "./projects/factoring/src/app/services/invoices.service.ts");
/* harmony import */ var _models_invoice_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../models/invoice.model */ "./projects/factoring/src/app/models/invoice.model.ts");
/* harmony import */ var _services_companies_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../services/companies.service */ "./projects/factoring/src/app/services/companies.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../services/login.service */ "./projects/factoring/src/app/services/login.service.ts");







var BankComponent = /** @class */ (function () {
    function BankComponent(srvInvoices, srvCompany, srvLogin) {
        this.srvInvoices = srvInvoices;
        this.srvCompany = srvCompany;
        this.srvLogin = srvLogin;
        this.clientType = 'bank';
        this.searchStr = '';
        this.showError = false;
        this.modalActions = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.selectedInvoice = new _models_invoice_model__WEBPACK_IMPORTED_MODULE_3__["InvoiceModel"]('', '', '', '', '', '', '', '', '', '', '', false, false, false, '');
        this.appTitle = 'Your Banking Solution';
        this.logoImage = 'https://uipath509.westeurope.cloudapp.azure.com/api/v1/getLogo?logoName=';
    }
    BankComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.srvLogin.getUser();
        this.srvCompany.getCompaniesByBank(this.user.companyId).then(function (res) {
            _this.allCompanies = res;
            _this.companies = _this.allCompanies;
            _this.getAllInvoices();
        });
        this.inputForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormGroup"]({
            invoiceNo: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            buyerName: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            sellerName: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            invoiceAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            financedAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            comment: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
            invoiceDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_5__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_5__["Validators"].required),
        });
    };
    BankComponent.prototype.searchCompany = function (searchCompanyValue) {
        var _this = this;
        this.selectedCompanyName = searchCompanyValue;
        this.invoices = this.invoices.filter(function (x) { return x.companyName === _this.selectedCompanyName; });
        this.searchStr = '';
        //this.companies = this.allCompanies;
        this.getInvoicesStatus();
        this.companies = this.companies.filter(function (x) { return x.companyName === searchCompanyValue; });
    };
    BankComponent.prototype.getAllInvoices = function () {
        var _this = this;
        var filteredInvoices = [new _models_invoice_model__WEBPACK_IMPORTED_MODULE_3__["InvoiceModel"]('', '', '', '', '', '', '', '', '', '', '', false, false, false, '')];
        this.srvInvoices.getAllInvoices()
            .then(function (result) {
            var _loop_1 = function (i) {
                filteredInvoices = filteredInvoices.concat(result.filter(function (x) {
                    return x.companyName === _this.allCompanies[i].companyName;
                }));
            };
            for (var i = 0; i < _this.allCompanies.length; i++) {
                _loop_1(i);
            }
            if (filteredInvoices[0].companyName.length < 1)
                filteredInvoices.splice(0, 1);
            _this.serverInvoices = filteredInvoices;
            _this.invoices = _this.serverInvoices;
            _this.companies = _this.allCompanies;
            _this.getInvoicesStatus();
            _this.searchStr = '';
        });
    };
    BankComponent.prototype.sortInvoiceList = function (sortStatus) {
        var _this = this;
        this.invoices = this.serverInvoices;
        if (this.selectedCompanyName)
            this.invoices = this.invoices.filter(function (x) { return x.companyName === _this.selectedCompanyName; });
        this.invoices = this.invoices.filter(function (x) { return x.status === sortStatus; });
    };
    BankComponent.prototype.getInvoicesStatus = function () {
        this.pendingNo = this.serverInvoices.filter(function (res) { return res.status === 'Pending'; }).length;
        this.financedNo = this.serverInvoices.filter(function (res) { return res.status === 'Financed'; }).length;
        this.refusedNo = this.serverInvoices.filter(function (res) { return res.status === 'Refused'; }).length;
        this.reimbursedNo = this.serverInvoices.filter(function (res) { return res.status === 'Reimbursed'; }).length;
        this.forApproval = this.serverInvoices.filter(function (res) { return res.status === 'ForApproval'; }).length;
    };
    BankComponent.prototype.editInvoice = function (invoice) {
        var _this = this;
        this.srvCompany.getCompanyById(invoice.companyId).then(function (res) {
            _this.companyLimit = res.companyLimit;
            _this.modalActions.emit({ action: 'modal', params: ['open'] });
            _this.selectedInvoice = invoice;
        });
    };
    BankComponent.prototype.closeModal = function () {
        this.modalActions.emit({ action: 'modal', params: ['close'] });
        this.getInvoicesStatus();
    };
    BankComponent.prototype.submitOffer = function () {
        this.selectedInvoice.status = 'Financed';
        this.srvInvoices.updateInvoice(this.selectedInvoice);
        this.closeModal();
    };
    BankComponent.prototype.saveChanges = function () {
        this.srvInvoices.updateInvoice(this.selectedInvoice);
        this.closeModal();
    };
    BankComponent.prototype.reimbursedOFfer = function (invoice) {
        invoice.status = 'Reimbursed';
        this.srvInvoices.updateInvoice(invoice);
        this.getInvoicesStatus();
    };
    BankComponent.prototype.cancelOffer = function () {
        this.selectedInvoice.status = 'Refused';
        this.srvInvoices.updateInvoice(this.selectedInvoice);
        this.closeModal();
    };
    BankComponent.prototype.sentToApproval = function () {
        this.selectedInvoice.status = 'ForApproval';
        this.srvInvoices.updateInvoice(this.selectedInvoice);
        this.closeModal();
    };
    BankComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-bank',
            template: __webpack_require__(/*! ./bank.component.html */ "./projects/factoring/src/app/components/bank/bank.component.html"),
            styles: [__webpack_require__(/*! ./bank.component.scss */ "./projects/factoring/src/app/components/bank/bank.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_invoices_service__WEBPACK_IMPORTED_MODULE_2__["ServiceInvoices"],
            _services_companies_service__WEBPACK_IMPORTED_MODULE_4__["ServiceCompanies"],
            _services_login_service__WEBPACK_IMPORTED_MODULE_6__["ServiceLogin"]])
    ], BankComponent);
    return BankComponent;
}());



/***/ }),

/***/ "./projects/factoring/src/app/components/client/client-home/client-home.component.html":
/*!*********************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/client/client-home/client-home.component.html ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<app-nav-bar *ngIf=\"appTitle\" [title]=\"appTitle\"></app-nav-bar>\n<div *ngIf=\"company\" class=\"full-body\">\n  <div class=\"z-depth-2 row loginForm\">\n    <div class=\"row\">\n      <div class=\"col s2 m2 l2 \">\n        <span class=\"row \" style=\"margin-bottom: 22px; padding-left: 20px;\">Welcome {{user.userName}}!</span>\n        <div class=\"row\"><img src=\"{{logoImage}}{{company.companyLogo}}\" style=\"width:9rem; vertical-align:bottom;\" /></div>\n      </div>\n\n      <div *ngIf=\"invoices\" class=\"col s10 m10 l10\">\n        <div class=\"row col s12 m12 l12\">\n          <span class=\"col s3 m3 l3\"><span style=\"font-weight: bold\">Total Number of Invoices: </span>\n            {{invoices.length}}</span>\n          <span class=\"col s2 m2 l2\"><span style=\"font-weight: bold; color: orange\">Pending: </span>{{pendingNo}}</span>\n          <span class=\"col s2 m2 l2\"><span style=\"font-weight: bold; color: green\">Financed:\n            </span>{{financedNo}}</span>\n          <span class=\"col s2 m2 l2\"><span style=\"font-weight: bold; color: blue\">Reimbursed:\n            </span>{{reimbursedNo}}</span>\n          <span class=\"col s2 m2 l2\"><span style=\"font-weight: bold; color: red\">Refused: </span>{{refusedNo}}</span>\n        </div>\n        <br>\n        <div class=\"row col s12 m12 l12\" style='margin-top:20px;'>\n          <span class=\"row col s12 m12 l12 center\">Approved credit limit of: {{company.companyLimit}}</span><br><br>\n          <div class=\"row col s1 m1 l1 valign-wrapper\"></div>\n          <div class=\"row col s10 m10 l10 center progress\">\n            <div class=\"determinate\" [style.width.%]=\"rangeValue\"></div>\n          </div>\n          <div class=\"row col s1 m1 l1 right valign-wrapper\"></div>\n        </div>\n      </div>\n    </div>\n  </div>\n\n  <div style=\"padding-top:20px;\"></div>\n  <div class=\"center-align z-depth-2 row loginForm\">\n    <div class=\"row\">\n      <div class=\"col s12 m12 l12\">\n        <app-invoices-list (invoicesLoaded)=\"getLoadedInvoices($event)\"></app-invoices-list>\n      </div>\n    </div>\n  </div>\n</div>\n"

/***/ }),

/***/ "./projects/factoring/src/app/components/client/client-home/client-home.component.scss":
/*!*********************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/client/client-home/client-home.component.scss ***!
  \*********************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-body {\n  padding-top: 20px;\n  height: 90%;\n  overflow: auto; }\n\n.loginForm {\n  padding: 30px;\n  max-width: 95%; }\n\n.row {\n  margin-bottom: 0px; }\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2ZhY3RvcmluZy9zcmMvYXBwL2NvbXBvbmVudHMvY2xpZW50L2NsaWVudC1ob21lL0M6XFxVc2Vyc1xcbWloYWkuYW5kcmVpXFxPbmVEcml2ZSAtIFVpUGF0aFxcVWlQYXRoXFxwcm9qZWN0c1xcX0lOVEVSTkFMX1JQQVBvcnRhbFxcUG9ydGFsL3Byb2plY3RzXFxmYWN0b3JpbmdcXHNyY1xcYXBwXFxjb21wb25lbnRzXFxjbGllbnRcXGNsaWVudC1ob21lXFxjbGllbnQtaG9tZS5jb21wb25lbnQuc2NzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGlCQUFpQjtFQUNqQixXQUFVO0VBQ1YsY0FBYyxFQUFBOztBQUdoQjtFQUNFLGFBQVk7RUFDWixjQUFhLEVBQUE7O0FBR2Y7RUFDRSxrQkFBa0IsRUFBQSIsImZpbGUiOiJwcm9qZWN0cy9mYWN0b3Jpbmcvc3JjL2FwcC9jb21wb25lbnRzL2NsaWVudC9jbGllbnQtaG9tZS9jbGllbnQtaG9tZS5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi5mdWxsLWJvZHl7XHJcbiAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgaGVpZ2h0OjkwJTtcclxuICBvdmVyZmxvdzogYXV0bztcclxufVxyXG5cclxuLmxvZ2luRm9ybSB7XHJcbiAgcGFkZGluZzozMHB4O1xyXG4gIG1heC13aWR0aDo5NSU7XHJcbn1cclxuXHJcbi5yb3cge1xyXG4gIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG5cclxuIl19 */"

/***/ }),

/***/ "./projects/factoring/src/app/components/client/client-home/client-home.component.ts":
/*!*******************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/client/client-home/client-home.component.ts ***!
  \*******************************************************************************************/
/*! exports provided: ClientHomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ClientHomeComponent", function() { return ClientHomeComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../../services/login.service */ "./projects/factoring/src/app/services/login.service.ts");
/* harmony import */ var _services_companies_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../services/companies.service */ "./projects/factoring/src/app/services/companies.service.ts");




var ClientHomeComponent = /** @class */ (function () {
    function ClientHomeComponent(srvLogin, srvCompany) {
        this.srvLogin = srvLogin;
        this.srvCompany = srvCompany;
        this.logoImage = 'https://uipath509.westeurope.cloudapp.azure.com/api/v1/getLogo?logoName=';
    }
    ClientHomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.srvLogin.getUser();
        this.srvCompany.getCompanyById(this.user.companyId)
            .then(function (result) {
            _this.srvCompany.setComapny(result);
            _this.company = _this.srvCompany.getCompany();
            _this.image = '../../../../assets/images/factoring/' + _this.company.companyLogo;
            _this.appTitle = 'Customer Factoring App';
        });
    };
    ClientHomeComponent.prototype.getLoadedInvoices = function (lists) {
        this.invoices = lists;
        this.pendingNo = this.invoices.filter(function (res) { return res.status === 'Pending'; }).length;
        this.financedNo = this.invoices.filter(function (res) { return res.status === 'Financed'; }).length;
        this.refusedNo = this.invoices.filter(function (res) { return res.status === 'Refused'; }).length;
        this.reimbursedNo = this.invoices.filter(function (res) { return res.status === 'Reimbursed'; }).length;
        for (var i = 0; i < this.invoices.length; i++) {
            if (this.invoices[i].status === 'Financed') {
                this.rangeValue = +this.invoices[i].financedAmount;
            }
        }
        this.rangeValue = this.rangeValue / parseInt(this.company.companyLimit) * 100;
    };
    ClientHomeComponent.prototype.disconnectUser = function () {
        this.srvLogin.disconnectUser();
    };
    ClientHomeComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-client-home',
            template: __webpack_require__(/*! ./client-home.component.html */ "./projects/factoring/src/app/components/client/client-home/client-home.component.html"),
            styles: [__webpack_require__(/*! ./client-home.component.scss */ "./projects/factoring/src/app/components/client/client-home/client-home.component.scss")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_login_service__WEBPACK_IMPORTED_MODULE_2__["ServiceLogin"],
            _services_companies_service__WEBPACK_IMPORTED_MODULE_3__["ServiceCompanies"]])
    ], ClientHomeComponent);
    return ClientHomeComponent;
}());



/***/ }),

/***/ "./projects/factoring/src/app/components/client/invoices-list/invoices-list.component.css":
/*!************************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/client/invoices-list/invoices-list.component.css ***!
  \************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9qZWN0cy9mYWN0b3Jpbmcvc3JjL2FwcC9jb21wb25lbnRzL2NsaWVudC9pbnZvaWNlcy1saXN0L2ludm9pY2VzLWxpc3QuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./projects/factoring/src/app/components/client/invoices-list/invoices-list.component.html":
/*!*************************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/client/invoices-list/invoices-list.component.html ***!
  \*************************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table class=\"striped\">\n  <thead>\n    <tr>\n      <th>ID</th>\n      <th>Request Date</th>\n      <th>Invoice No</th>\n      <th>Invoice Amount</th>\n      <th>Financed Amount</th>\n      <th>Status</th>\n      <th>Comment</th>\n\n    </tr>\n  </thead>\n  <tbody>\n    <tr *ngFor=\"let invoice of invoices\">\n      <th>\n        {{invoice._id}}\n      </th>\n      <th>\n        {{invoice.requestDate | date}}\n      </th>\n      <th>\n        {{invoice.invoiceNo}}\n      </th>\n      <th>\n        {{invoice.invoiceAmount}}\n      </th>\n      <th>\n        {{invoice.financedAmount}}\n      </th>\n      <th>\n        {{invoice.status}}\n      </th>\n      <th>\n        {{invoice.comment}}\n      </th>\n      <th>\n          <a (click)=\"deleteInvoice(invoice)\"><i class=\"material-icons\" style=\"color: orangered\">delete</i></a>\n        </th>\n    </tr>\n  </tbody>\n</table>\n\n<br>\n<a class=\"waves-effect col s3 m3 l2 btn uiColorButtons \" (click)=\"openModal()\">Add Invoice</a>\n\n\n<div id=\"modal1\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalActions\">\n  <div class=\"modal-content\">\n    <div style=\"padding-bottom: 20px\">\n      <span class=\"left\" style=\"font-weight:bold;font-size:24px;padding: 10px; \">Add a new invoice</span>\n      <i class=\"material-icons right modal-action modal-close\" (click)=\"closeModal()\">close</i>\n    </div>\n\n    <div *ngIf=\"newInvoice\" class=\"row col s12 m12 l12\" style=\"padding-top: 15px;\">\n      <div class=\"left row col s12 m12 l12\">\n        <span class=\"col s6 m6 l6\"><span style=\"font-weight: bold\">Company Name: </span>\n          {{company.companyName}}</span>\n        <span class=\"col s6 m6 l6\"><span style=\"font-weight: bold\">Status: </span> {{newInvoice.status}}</span>\n      </div>\n\n      <div class=\" row col s12 m12 l12\" [formGroup]=\"inputForm\">\n        <div class=\"col s5 m5 l5\">\n          <label for=\"invoiceAmount\">Invoice Amount</label>\n          <input name=\"invoiceAmount\" [ngModel]=\"newInvoice.invoiceAmount\" formControlName=\"invoiceAmount\">\n        </div> <div class=\"col s1 m1 l1\"></div>\n        <div class=\"col s12 m12 l5\">\n          <label for=\"comment\">Comment</label>\n          <input name=\"comment\" [ngModel]=\"newInvoice.comment\" formControlName=\"comment\">\n        </div><br>\n\n\n        <div class=\"row file-field input-field col s12 m12 l6 \">\n          <div class=\"btn right uiColorSecondary\">\n            <span>Upload File</span>\n            <input (change)=\"handleFileInput($event.target.files)\" type=\"file\">\n          </div>\n\n\n        </div>\n        <div class=\"col s12 m12 l6\">\n          <input *ngIf=\"!fileUploaded\" disabled=\"true\" class=\"left row btn btn-small uiColorButtons\" (click)=\"onSubmit()\" type=\"submit\" value=\"Submit\">\n          <input *ngIf=\"fileUploaded\"  class=\"left row btn btn-small uiColorButtons\" (click)=\"onSubmit()\" type=\"submit\" value=\"Submit\">\n        </div>\n\n      </div>\n\n    </div>\n  </div>\n\n</div>\n"

/***/ }),

/***/ "./projects/factoring/src/app/components/client/invoices-list/invoices-list.component.ts":
/*!***********************************************************************************************!*\
  !*** ./projects/factoring/src/app/components/client/invoices-list/invoices-list.component.ts ***!
  \***********************************************************************************************/
/*! exports provided: InvoicesListComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoicesListComponent", function() { return InvoicesListComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _models_invoice_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../../models/invoice.model */ "./projects/factoring/src/app/models/invoice.model.ts");
/* harmony import */ var _services_invoices_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./../../../services/invoices.service */ "./projects/factoring/src/app/services/invoices.service.ts");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./../../../services/login.service */ "./projects/factoring/src/app/services/login.service.ts");
/* harmony import */ var _services_companies_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./../../../services/companies.service */ "./projects/factoring/src/app/services/companies.service.ts");








var InvoicesListComponent = /** @class */ (function () {
    function InvoicesListComponent(srvInvoices, srvUser, srvCompany) {
        this.srvInvoices = srvInvoices;
        this.srvUser = srvUser;
        this.srvCompany = srvCompany;
        this.modalActions = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.today = Date.now();
        this.fileUploaded = false;
        this.invoicesLoaded = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    InvoicesListComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.user = this.srvUser.getUser();
        this.srvCompany.getCompanyById(this.user.companyId)
            .then(function (result) {
            _this.company = result;
            _this.initNewInvoice();
            _this.getCompanyInvoices();
        });
        this.inputForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            invoiceAmount: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            comment: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
        });
    };
    InvoicesListComponent.prototype.getLoadedInvoices = function (lists) {
        this.invoicesLoaded.emit(lists);
    };
    InvoicesListComponent.prototype.getCompanyInvoices = function () {
        var _this = this;
        this.srvInvoices.getInvoicesByCompany(this.company.companyName).then(function (result) {
            _this.invoices = result;
            _this.getLoadedInvoices(_this.invoices);
        });
    };
    InvoicesListComponent.prototype.initNewInvoice = function () {
        this.newInvoice = new _models_invoice_model__WEBPACK_IMPORTED_MODULE_3__["InvoiceModel"](this.user.companyId, this.company.companyName, '', '', this.today.toString(), '', '', '', '', 'Pending', 'No comment', false, false, false, '');
    };
    InvoicesListComponent.prototype.openModal = function () {
        this.initNewInvoice();
        this.modalActions.emit({ action: 'modal', params: ['open'] });
    };
    InvoicesListComponent.prototype.closeModal = function () {
        this.modalActions.emit({ action: 'modal', params: ['close'] });
    };
    InvoicesListComponent.prototype.handleFileInput = function (files) {
        this.fileToUpload = files.item(0);
        this.fileUploaded = true;
    };
    InvoicesListComponent.prototype.onSubmit = function () {
        this.newInvoice.invoiceFile = this.fileToUpload.name;
        this.newInvoice.invoiceAmount = this.inputForm.controls['invoiceAmount'].value;
        this.newInvoice.comment = this.inputForm.controls['comment'].value;
        this.invoices.push(this.newInvoice);
        this.srvInvoices.postInvoiceByCompany(this.newInvoice);
        // this.getCompanyInvoices();
        this.srvInvoices.postFile(this.fileToUpload);
        this.closeModal();
    };
    InvoicesListComponent.prototype.deleteInvoice = function (invoice) {
        this.srvInvoices.deleteInvoice(invoice);
        var idx = this.invoices.findIndex(function (x) { return x._id === invoice._id; });
        this.invoices.splice(idx, 1);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Output"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", Object)
    ], InvoicesListComponent.prototype, "invoicesLoaded", void 0);
    InvoicesListComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-invoices-list',
            template: __webpack_require__(/*! ./invoices-list.component.html */ "./projects/factoring/src/app/components/client/invoices-list/invoices-list.component.html"),
            styles: [__webpack_require__(/*! ./invoices-list.component.css */ "./projects/factoring/src/app/components/client/invoices-list/invoices-list.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_invoices_service__WEBPACK_IMPORTED_MODULE_4__["ServiceInvoices"],
            _services_login_service__WEBPACK_IMPORTED_MODULE_5__["ServiceLogin"],
            _services_companies_service__WEBPACK_IMPORTED_MODULE_6__["ServiceCompanies"]])
    ], InvoicesListComponent);
    return InvoicesListComponent;
}());



/***/ }),

/***/ "./projects/factoring/src/app/components/login/login.component.css":
/*!*************************************************************************!*\
  !*** ./projects/factoring/src/app/components/login/login.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.full-body{\r\n  height:100%;\r\n  overflow: auto;\r\n  -webkit-transform-origin: left bottom;\r\n          transform-origin: left bottom;\r\n  background-size:100% 100%;\r\n  background-size: cover;\r\n  background-repeat: no-repeat;\r\n}\r\n\r\n\r\n.loginForm {\r\n  /* margin-top:12%; */\r\n  padding:30px;\r\n  min-width:450px;\r\n}\r\n\r\n\r\n.row {\r\n  margin-bottom: 0px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2ZhY3RvcmluZy9zcmMvYXBwL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLHFDQUE2QjtVQUE3Qiw2QkFBNkI7RUFDN0IseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0Qiw0QkFBNEI7QUFDOUI7OztBQUdBO0VBQ0Usb0JBQW9CO0VBQ3BCLFlBQVk7RUFDWixlQUFlO0FBQ2pCOzs7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQiIsImZpbGUiOiJwcm9qZWN0cy9mYWN0b3Jpbmcvc3JjL2FwcC9jb21wb25lbnRzL2xvZ2luL2xvZ2luLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyJcclxuLmZ1bGwtYm9keXtcclxuICBoZWlnaHQ6MTAwJTtcclxuICBvdmVyZmxvdzogYXV0bztcclxuICB0cmFuc2Zvcm0tb3JpZ2luOiBsZWZ0IGJvdHRvbTtcclxuICBiYWNrZ3JvdW5kLXNpemU6MTAwJSAxMDAlO1xyXG4gIGJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgYmFja2dyb3VuZC1yZXBlYXQ6IG5vLXJlcGVhdDtcclxufVxyXG5cclxuXHJcbi5sb2dpbkZvcm0ge1xyXG4gIC8qIG1hcmdpbi10b3A6MTIlOyAqL1xyXG4gIHBhZGRpbmc6MzBweDtcclxuICBtaW4td2lkdGg6NDUwcHg7XHJcbn1cclxuXHJcbi5yb3cge1xyXG4gIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./projects/factoring/src/app/components/login/login.component.html":
/*!**************************************************************************!*\
  !*** ./projects/factoring/src/app/components/login/login.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"full-body valign-wrapper\">\n\n\n  <div class=\"center-align z-depth-3 row loginForm galaxy\">\n    <div class=\"row\">\n      <!-- <img src=\"/assets/images/logo.jpg\" style=\"width:7rem; vertical-align:bottom; \" class=\"center\" /> -->\n      <span style=\"font-weight: bold; font-size:20px;\" class=\"valign-wrapper\">Factoring App</span>\n    </div><br>\n\n    <div class=\"row\">\n      <div [formGroup]=\"inputForm\">\n        <div class=\"input-field\">\n          <i class=\"material-icons prefix\">email</i>\n          <input id=\"userName\" type=\"text\" class=\"validate\" class=\"validate\" formControlName=\"userName\">\n          <label for=\"userName\">User name</label>\n        </div>\n\n        <div class=\"input-field\">\n          <i class=\"material-icons prefix \">lock</i>\n          <input id=\"password\" type=\"password\" class=\"validate\" class=\"validate\" formControlName=\"password\">\n          <label for=\"password\">Password</label>\n        </div>\n\n        <div *ngIf=\"showError\" class=\"row\">\n          <span style=\"color:red\">incorrect credentials</span>\n        </div>\n        <div class=\"row\">\n          <button class=\"btn uiColorButtons btn-primary\" (click)=\"onSubmit()\">login </button>\n        </div>\n      </div>\n    </div>\n\n  </div>\n\n\n</div>"

/***/ }),

/***/ "./projects/factoring/src/app/components/login/login.component.ts":
/*!************************************************************************!*\
  !*** ./projects/factoring/src/app/components/login/login.component.ts ***!
  \************************************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../../services/login.service */ "./projects/factoring/src/app/services/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");





var LoginComponent = /** @class */ (function () {
    function LoginComponent(loginSrv, router) {
        this.loginSrv = loginSrv;
        this.router = router;
        this.showError = false;
        this.modalActions = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.password = '';
        this.confirmPassword = '';
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.inputForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormGroup"]({
            companyName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            userName: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_2__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_2__["Validators"].required)
        });
    };
    LoginComponent.prototype.onSubmit = function () {
        var _this = this;
        var user = this.inputForm.get('userName').value;
        var pass = this.inputForm.get('password').value;
        this.loginSrv.checkUser(user, pass).subscribe(function (res) {
            _this.user = res[0];
            if (_this.user) {
                _this.loginSrv.setUser(_this.user);
                _this.router.navigateByUrl('factoring/' + _this.user.type);
            }
            else {
                _this.showError = true;
                _this.inputForm.reset();
            }
        });
    };
    LoginComponent.prototype.openModal = function () {
        this.modalActions.emit({ action: 'modal', params: ['open'] });
    };
    LoginComponent.prototype.closeModal = function () {
        this.modalActions.emit({ action: 'modal', params: ['close'] });
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./projects/factoring/src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./projects/factoring/src/app/components/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_login_service__WEBPACK_IMPORTED_MODULE_3__["ServiceLogin"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./projects/factoring/src/app/components/nav-bar/nav-bar.component.css":
/*!*****************************************************************************!*\
  !*** ./projects/factoring/src/app/components/nav-bar/nav-bar.component.css ***!
  \*****************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".brand-logo{\r\n  padding-left: 20px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL2ZhY3RvcmluZy9zcmMvYXBwL2NvbXBvbmVudHMvbmF2LWJhci9uYXYtYmFyLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxrQkFBa0I7QUFDcEIiLCJmaWxlIjoicHJvamVjdHMvZmFjdG9yaW5nL3NyYy9hcHAvY29tcG9uZW50cy9uYXYtYmFyL25hdi1iYXIuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5icmFuZC1sb2dve1xyXG4gIHBhZGRpbmctbGVmdDogMjBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./projects/factoring/src/app/components/nav-bar/nav-bar.component.html":
/*!******************************************************************************!*\
  !*** ./projects/factoring/src/app/components/nav-bar/nav-bar.component.html ***!
  \******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav>\n  <div class=\"nav-wrapper uiColorSecondary\">\n    <a class=\"brand-logo\">{{title}}</a>\n    <ul id=\"nav-mobile\" class=\"right hide-on-med-and-down\">\n      <li><a (click)=\"goToProfile()\"> <i class=\"material-icons\">account_circle</i></a></li>\n      <li><a  (click)=\"disconnectUser()\">Logout</a></li>\n\n    </ul>\n  </div>\n</nav>\n"

/***/ }),

/***/ "./projects/factoring/src/app/components/nav-bar/nav-bar.component.ts":
/*!****************************************************************************!*\
  !*** ./projects/factoring/src/app/components/nav-bar/nav-bar.component.ts ***!
  \****************************************************************************/
/*! exports provided: NavBarComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "NavBarComponent", function() { return NavBarComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_login_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../../services/login.service */ "./projects/factoring/src/app/services/login.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");




var NavBarComponent = /** @class */ (function () {
    function NavBarComponent(srvLogin, router) {
        this.srvLogin = srvLogin;
        this.router = router;
    }
    NavBarComponent.prototype.ngOnInit = function () {
        console.log(this.title);
    };
    NavBarComponent.prototype.goToProfile = function () {
        if (this.type === 'bank') {
            console.log(this.type);
            this.router.navigateByUrl('bankProfile');
        }
    };
    NavBarComponent.prototype.disconnectUser = function () {
        this.srvLogin.disconnectUser();
        this.router.navigate(['..']);
    };
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NavBarComponent.prototype, "title", void 0);
    tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Input"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:type", String)
    ], NavBarComponent.prototype, "type", void 0);
    NavBarComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-nav-bar',
            template: __webpack_require__(/*! ./nav-bar.component.html */ "./projects/factoring/src/app/components/nav-bar/nav-bar.component.html"),
            styles: [__webpack_require__(/*! ./nav-bar.component.css */ "./projects/factoring/src/app/components/nav-bar/nav-bar.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_login_service__WEBPACK_IMPORTED_MODULE_2__["ServiceLogin"],
            _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"]])
    ], NavBarComponent);
    return NavBarComponent;
}());



/***/ }),

/***/ "./projects/factoring/src/app/models/companies.model.ts":
/*!**************************************************************!*\
  !*** ./projects/factoring/src/app/models/companies.model.ts ***!
  \**************************************************************/
/*! exports provided: CompanyModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CompanyModel", function() { return CompanyModel; });
var CompanyModel = /** @class */ (function () {
    function CompanyModel(companyName, _id, companyLogo, companyLimit, buyerRisk, bankId) {
        this._id = _id;
        this.companyName = companyName;
        this.companyLogo = companyLogo;
        this.companyLimit = companyLimit;
        this.buyerRisk = buyerRisk;
        this.bankId = bankId;
    }
    return CompanyModel;
}());



/***/ }),

/***/ "./projects/factoring/src/app/models/invoice.model.ts":
/*!************************************************************!*\
  !*** ./projects/factoring/src/app/models/invoice.model.ts ***!
  \************************************************************/
/*! exports provided: InvoiceModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InvoiceModel", function() { return InvoiceModel; });
var InvoiceModel = /** @class */ (function () {
    function InvoiceModel(companyId, companyName, sellerName, buyerName, requestDate, invoiceDate, invoiceNo, invoiceAmount, financedAmount, status, comment, buyerRisk, creditBureau, duplicity, invoiceFile) {
        this.companyId = companyId;
        this.companyName = companyName;
        this.sellerName = sellerName;
        this.buyerName = buyerName;
        this.requestDate = requestDate;
        this.invoiceDate = invoiceDate;
        this.invoiceNo = invoiceNo;
        this.invoiceAmount = invoiceAmount;
        this.financedAmount = financedAmount;
        this.status = status;
        this.comment = comment;
        this.buyerRisk = buyerRisk;
        this.creditBureau = creditBureau;
        this.duplicity = duplicity;
        this.invoiceFile = invoiceFile;
    }
    return InvoiceModel;
}());



/***/ }),

/***/ "./projects/factoring/src/app/models/user.model.ts":
/*!*********************************************************!*\
  !*** ./projects/factoring/src/app/models/user.model.ts ***!
  \*********************************************************/
/*! exports provided: UserModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserModel", function() { return UserModel; });
var UserModel = /** @class */ (function () {
    function UserModel(userName, password, companyId, type) {
        this.userName = userName;
        this.password = password;
        this.companyId = companyId;
        this.type = type;
    }
    return UserModel;
}());



/***/ }),

/***/ "./projects/factoring/src/app/services/companies.service.ts":
/*!******************************************************************!*\
  !*** ./projects/factoring/src/app/services/companies.service.ts ***!
  \******************************************************************/
/*! exports provided: ServiceCompanies */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceCompanies", function() { return ServiceCompanies; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");




var ServiceCompanies = /** @class */ (function () {
    function ServiceCompanies(http, _jsonp) {
        this.http = http;
        this._jsonp = _jsonp;
        this.url = 'https://uipath509.westeurope.cloudapp.azure.com:443/api/v1/';
        this.headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]({
            'Content-Type': 'application/json'
        });
    }
    ServiceCompanies.prototype.getCompaniesByBank = function (bankId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('bankId', bankId);
        return this.http
            .get(this.url + 'invoices_companies_bank/', { params: params })
            .toPromise()
            .then(function (resp) { return resp; })
            .catch(this.handleError);
    };
    ServiceCompanies.prototype.getAllCompanies = function () {
        return this.http
            .get(this.url + 'invoices_companies/')
            .toPromise()
            .then(function (resp) { return resp; })
            .catch(this.handleError);
    };
    ServiceCompanies.prototype.getCompanyById = function (companyId) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('_id', companyId);
        return this.http.get(this.url + '/invoices_company', { params: params })
            .toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceCompanies.prototype.getCompanyByName = function (companyName) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set('companyName', companyName);
        return this.http.get(this.url + 'invoices_companyName', { params: params })
            .toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceCompanies.prototype.getCompany = function () {
        if (this.company) {
            return this.company;
        }
        else {
            localStorage.setItem("currentCompany", JSON.stringify(this.company));
            var company = JSON.parse(localStorage.getItem("currentCompany"));
            return company;
        }
    };
    ServiceCompanies.prototype.setComapny = function (company) {
        this.company = company;
        localStorage.setItem("currentCompany", JSON.stringify(this.company));
    };
    ServiceCompanies.prototype.deleteCompany = function (company) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set("_id", company._id);
        return this.http.delete(this.url + 'invoices_companies', { params: params }).toPromise()
            .then(function (resp) {
            return resp;
        });
    };
    ServiceCompanies.prototype.tempCreateUser = function (company) {
        var comp = this.getCompanyByName(company.companyName);
        console.log(comp);
    };
    ServiceCompanies.prototype.postCompany = function (company) {
        if (company.companyLogo.length < 1)
            company.companyLogo = 'genericLogo';
        return this.http.post(this.url + 'invoices_companies/', {
            companyName: company.companyName,
            companyLogo: company.companyLogo,
            companyLimit: company.companyLimit,
            buyerRisk: company.buyerRisk,
            bankId: company.bankId
        }).toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceCompanies.prototype.postUser = function (user) {
        return this.http.post(this.url + 'invoices_users/', {
            userName: user.userName,
            password: user.password,
            companyId: user.companyId,
            type: user.type
        }).toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceCompanies.prototype.postLogo = function (fileToUpload) {
        var endpoint = this.url + 'uploadLogo';
        var formData = new FormData();
        formData.append('fileKey', fileToUpload, fileToUpload.name);
        return this.http
            .post(endpoint, formData)
            .toPromise()
            .then(function (resp) {
        });
    };
    ServiceCompanies.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    ServiceCompanies.prototype.updateCompany = function (company) {
        console.log(company);
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set("id", company._id);
        return this.http.put(this.url + 'invoices_company', {
            companyName: company.companyName,
            companyLogo: company.companyLogo,
            companyLimit: company.companyLimit,
            buyerRisk: company.buyerRisk,
            bankId: company.bankId
        }, { params: params }).toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceCompanies = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_http__WEBPACK_IMPORTED_MODULE_3__["Jsonp"]])
    ], ServiceCompanies);
    return ServiceCompanies;
}());



/***/ }),

/***/ "./projects/factoring/src/app/services/invoices.service.ts":
/*!*****************************************************************!*\
  !*** ./projects/factoring/src/app/services/invoices.service.ts ***!
  \*****************************************************************/
/*! exports provided: ServiceInvoices */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceInvoices", function() { return ServiceInvoices; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");




var ServiceInvoices = /** @class */ (function () {
    function ServiceInvoices(http, _jsonp) {
        this.http = http;
        this._jsonp = _jsonp;
        this.url = 'https://uipath509.westeurope.cloudapp.azure.com:443/api/v1/invoices_list';
    }
    ServiceInvoices.prototype.getInvoicesByCompany = function (companyName) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("companyName", companyName);
        return this.http
            .get(this.url, { params: params })
            .toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceInvoices.prototype.getAllInvoices = function () {
        return this.http
            .get(this.url + "s")
            .toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceInvoices.prototype.deleteInvoice = function (invoice) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("_id", invoice._id);
        return this.http
            .delete(this.url, { params: params })
            .toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceInvoices.prototype.postInvoiceByCompany = function (invoice) {
        return this.http.post(this.url, {
            _id: invoice._id,
            companyId: invoice.companyId,
            companyName: invoice.companyName,
            sellerName: invoice.sellerName,
            buyerName: invoice.buyerName,
            requestDate: invoice.requestDate,
            invoiceDate: invoice.invoiceDate,
            invoiceNo: invoice.invoiceNo,
            invoiceAmount: invoice.invoiceAmount,
            financedAmount: invoice.financedAmount,
            status: invoice.status,
            buyerRisk: invoice.buyerRisk,
            duplicity: invoice.duplicity,
            creditBureau: invoice.creditBureau,
            comment: invoice.comment,
            invoiceFile: invoice.invoiceFile,
        }).toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceInvoices.prototype.postFile = function (fileToUpload) {
        var endpoint = 'https://uipath509.westeurope.cloudapp.azure.com:443/api/v1/uploadFile';
        var formData = new FormData();
        formData.append('fileKey', fileToUpload, fileToUpload.name);
        return this.http
            .post(endpoint, formData)
            .toPromise()
            .then(function (resp) {
        });
    };
    ServiceInvoices.prototype.updateInvoice = function (invoice) {
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]().set("_id", invoice._id);
        return this.http.put(this.url, {
            companyId: invoice.companyId,
            companyName: invoice.companyName,
            sellerName: invoice.sellerName,
            buyerName: invoice.buyerName,
            requestDate: invoice.requestDate,
            invoiceDate: invoice.invoiceDate,
            invoiceNo: invoice.invoiceNo,
            invoiceAmount: invoice.invoiceAmount,
            financedAmount: invoice.financedAmount,
            status: invoice.status,
            duplicity: invoice.duplicity,
            buyerRisk: invoice.buyerRisk,
            creditBureau: invoice.creditBureau,
            comment: invoice.comment,
            invoiceFile: invoice.invoiceFile,
        }, { params: params }).toPromise()
            .then(function (resp) {
            return resp;
        })
            .catch(this.handleError);
    };
    ServiceInvoices.prototype.handleError = function (error) {
        console.error('An error occurred', error);
        return Promise.reject(error.message || error);
    };
    ServiceInvoices = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"], _angular_http__WEBPACK_IMPORTED_MODULE_3__["Jsonp"]])
    ], ServiceInvoices);
    return ServiceInvoices;
}());



/***/ }),

/***/ "./projects/factoring/src/app/services/login.service.ts":
/*!**************************************************************!*\
  !*** ./projects/factoring/src/app/services/login.service.ts ***!
  \**************************************************************/
/*! exports provided: ServiceLogin */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ServiceLogin", function() { return ServiceLogin; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/http */ "./node_modules/@angular/http/fesm5/http.js");
/* harmony import */ var _companies_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./companies.service */ "./projects/factoring/src/app/services/companies.service.ts");






var ServiceLogin = /** @class */ (function () {
    function ServiceLogin(http, _jsonp, srvCompany) {
        this.http = http;
        this._jsonp = _jsonp;
        this.srvCompany = srvCompany;
        this.url = "https://uipath509.westeurope.cloudapp.azure.com:443/api/v1/";
    }
    ServiceLogin.prototype.checkUser = function (userName, password) {
        var headers = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]();
        var params = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpParams"]()
            .set("userName", userName)
            .set("password", password);
        return this.http.get(this.url + "users/", { params: params });
    };
    ServiceLogin.prototype.setUser = function (user) {
        this.user = user;
        localStorage.setItem("currentUser", JSON.stringify(this.user));
    };
    ServiceLogin.prototype.getUser = function () {
        if (this.user) {
            return this.user;
        }
        else {
            var user = JSON.parse(localStorage.getItem("currentUser"));
            return user;
        }
    };
    ServiceLogin.prototype.disconnectUser = function () {
        localStorage.removeItem('currentUser');
    };
    ServiceLogin.prototype.handleError = function (error) {
        console.error("An error occurred", error);
        return Promise.reject(error.message || error);
    };
    ServiceLogin = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"],
            _angular_http__WEBPACK_IMPORTED_MODULE_3__["Jsonp"],
            _companies_service__WEBPACK_IMPORTED_MODULE_4__["ServiceCompanies"]])
    ], ServiceLogin);
    return ServiceLogin;
}());



/***/ }),

/***/ "./projects/ticketing/src/app/app-routing.module.ts":
/*!**********************************************************!*\
  !*** ./projects/ticketing/src/app/app-routing.module.ts ***!
  \**********************************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/login/login.component */ "./projects/ticketing/src/app/components/login/login.component.ts");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./projects/ticketing/src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _components_incidents_incidents_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/incidents/incidents.component */ "./projects/ticketing/src/app/components/incidents/incidents.component.ts");






var APP_ROUTES = [
    { path: 'ticketing/login', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'ticketing', component: _components_login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"] },
    { path: 'ticketing/dashboard', component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_4__["DashboardComponent"] },
    { path: 'ticketing/ServiceRequest', component: _components_incidents_incidents_component__WEBPACK_IMPORTED_MODULE_5__["IncidentsComponent"] }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(APP_ROUTES)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./projects/ticketing/src/app/app.component.css":
/*!******************************************************!*\
  !*** ./projects/ticketing/src/app/app.component.css ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJwcm9qZWN0cy90aWNrZXRpbmcvc3JjL2FwcC9hcHAuY29tcG9uZW50LmNzcyJ9 */"

/***/ }),

/***/ "./projects/ticketing/src/app/app.component.html":
/*!*******************************************************!*\
  !*** ./projects/ticketing/src/app/app.component.html ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\n<div style=\"text-align:center\">\n  <h1>\n    Welcome to {{ title }}!\n  </h1>\n  <img width=\"300\" alt=\"Angular Logo\" src=\"data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCAyNTAgMjUwIj4KICAgIDxwYXRoIGZpbGw9IiNERDAwMzEiIGQ9Ik0xMjUgMzBMMzEuOSA2My4ybDE0LjIgMTIzLjFMMTI1IDIzMGw3OC45LTQzLjcgMTQuMi0xMjMuMXoiIC8+CiAgICA8cGF0aCBmaWxsPSIjQzMwMDJGIiBkPSJNMTI1IDMwdjIyLjItLjFWMjMwbDc4LjktNDMuNyAxNC4yLTEyMy4xTDEyNSAzMHoiIC8+CiAgICA8cGF0aCAgZmlsbD0iI0ZGRkZGRiIgZD0iTTEyNSA1Mi4xTDY2LjggMTgyLjZoMjEuN2wxMS43LTI5LjJoNDkuNGwxMS43IDI5LjJIMTgzTDEyNSA1Mi4xem0xNyA4My4zaC0zNGwxNy00MC45IDE3IDQwLjl6IiAvPgogIDwvc3ZnPg==\">\n</div>\n<h2>Here are some links to help you start: </h2>\n<ul>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://angular.io/tutorial\">Tour of Heroes</a></h2>\n  </li>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://angular.io/cli\">CLI Documentation</a></h2>\n  </li>\n  <li>\n    <h2><a target=\"_blank\" rel=\"noopener\" href=\"https://blog.angular.io/\">Angular blog</a></h2>\n  </li>\n</ul>\n\n<router-outlet></router-outlet>\n"

/***/ }),

/***/ "./projects/ticketing/src/app/app.component.ts":
/*!*****************************************************!*\
  !*** ./projects/ticketing/src/app/app.component.ts ***!
  \*****************************************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'ticketing';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./projects/ticketing/src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./projects/ticketing/src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./projects/ticketing/src/app/app.module.ts":
/*!**************************************************!*\
  !*** ./projects/ticketing/src/app/app.module.ts ***!
  \**************************************************/
/*! exports provided: AppModule, App5SharedModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "App5SharedModule", function() { return App5SharedModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app-routing.module */ "./projects/ticketing/src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./projects/ticketing/src/app/app.component.ts");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _components_login_login_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./components/login/login.component */ "./projects/ticketing/src/app/components/login/login.component.ts");
/* harmony import */ var angular2_materialize__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! angular2-materialize */ "./node_modules/angular2-materialize/dist/index.js");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./projects/ticketing/src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _services_ticket_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./services/ticket.service */ "./projects/ticketing/src/app/services/ticket.service.ts");
/* harmony import */ var _components_incidents_incidents_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./components/incidents/incidents.component */ "./projects/ticketing/src/app/components/incidents/incidents.component.ts");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");












var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _components_login_login_component__WEBPACK_IMPORTED_MODULE_5__["LoginComponent"],
                _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_7__["DashboardComponent"],
                _components_incidents_incidents_component__WEBPACK_IMPORTED_MODULE_9__["IncidentsComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_2__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_10__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["FormsModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_11__["ReactiveFormsModule"],
                angular2_materialize__WEBPACK_IMPORTED_MODULE_6__["MaterializeModule"]
            ],
            providers: [
                _services_ticket_service__WEBPACK_IMPORTED_MODULE_8__["TicketService"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());

var App5SharedModule = /** @class */ (function () {
    function App5SharedModule() {
    }
    App5SharedModule.forRoot = function () {
        return {
            ngModule: AppModule,
            providers: [
                _services_ticket_service__WEBPACK_IMPORTED_MODULE_8__["TicketService"]
            ]
        };
    };
    App5SharedModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_4__["NgModule"])({})
    ], App5SharedModule);
    return App5SharedModule;
}());



/***/ }),

/***/ "./projects/ticketing/src/app/components/dashboard/dashboard.component.css":
/*!*********************************************************************************!*\
  !*** ./projects/ticketing/src/app/components/dashboard/dashboard.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".brand-logo{\r\n    padding-left: 20px;\r\n  }\r\n  \r\n  \r\n  .full-body{\r\n    padding-top: 20px;\r\n    padding-left: 20px;\r\n    padding-right: 20px;\r\n    height:90%;\r\n    overflow: auto;\r\n  }\r\n  \r\n  \r\n  .card {\r\n    padding:20px;\r\n  \r\n  }\r\n  \r\n  \r\n  nav{\r\n      max-height:64px;\r\n  }\r\n  \r\n  \r\n  .row {\r\n    margin-bottom: 0px;\r\n  }\r\n  \r\n  \r\n  .side-nav {\r\n    margin-top: 65px;\r\n    width: 200px;\r\n}\r\n  \r\n  \r\n  li.active {\r\n  background-color: #ee6e73\r\n}\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL3RpY2tldGluZy9zcmMvYXBwL2NvbXBvbmVudHMvZGFzaGJvYXJkL2Rhc2hib2FyZC5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0VBQ3BCOzs7RUFHQTtJQUNFLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixjQUFjO0VBQ2hCOzs7RUFFQTtJQUNFLFlBQVk7O0VBRWQ7OztFQUVBO01BQ0ksZUFBZTtFQUNuQjs7O0VBQ0E7SUFDRSxrQkFBa0I7RUFDcEI7OztFQUdGO0lBQ0ksZ0JBQWdCO0lBQ2hCLFlBQVk7QUFDaEI7OztFQUVBO0VBQ0U7QUFDRiIsImZpbGUiOiJwcm9qZWN0cy90aWNrZXRpbmcvc3JjL2FwcC9jb21wb25lbnRzL2Rhc2hib2FyZC9kYXNoYm9hcmQuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5icmFuZC1sb2dve1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4gIH1cclxuICBcclxuICBcclxuICAuZnVsbC1ib2R5e1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gICAgaGVpZ2h0OjkwJTtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gIH1cclxuICBcclxuICAuY2FyZCB7XHJcbiAgICBwYWRkaW5nOjIwcHg7XHJcbiAgXHJcbiAgfVxyXG4gIFxyXG4gIG5hdntcclxuICAgICAgbWF4LWhlaWdodDo2NHB4O1xyXG4gIH1cclxuICAucm93IHtcclxuICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICB9XHJcblxyXG4gIFxyXG4uc2lkZS1uYXYge1xyXG4gICAgbWFyZ2luLXRvcDogNjVweDtcclxuICAgIHdpZHRoOiAyMDBweDtcclxufVxyXG5cclxubGkuYWN0aXZlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWU2ZTczXHJcbn1cclxuICAiXX0= */"

/***/ }),

/***/ "./projects/ticketing/src/app/components/dashboard/dashboard.component.html":
/*!**********************************************************************************!*\
  !*** ./projects/ticketing/src/app/components/dashboard/dashboard.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav>\n    <div class=\"nav-wrapper blue darken-4\">\n        <a class=\"brand-logo\">Your Ticketing System</a>\n\n    </div>\n</nav>\n\n<ul id=\"slide-out\" class=\"side-nav fixed\">\n    <li>\n        <a [routerLink]=\"ServiceDesk\"> Service Desk </a>\n    </li>\n    <li>\n        <a [routerLink]=\"Incidents\">Incidents</a>\n    </li>\n    <li>\n        <a routerLink=\"../ServiceRequest\" routerLinkActive=\"active\">Service Request</a>\n    </li>\n    <li>\n        <a [routerLink]=\"\">Settings</a>\n    </li>\n\n</ul>\n<a materialize=\"sideNav\" [materializeParams]=\"[{edge:'left'}]\" href=\"#\" data-activates=\"slide-out\"\n    class=\"button-collapse show-on-large\"><i class=\"mdi-navigation-menu\"></i></a>\n\n\n\n<div class=\"full-body row \">\n\n    <div class=\"col s1 m1 l1\" style=\"margin:20px;\">\n    </div>\n    <div class=\"col s9 m9 l10\">\n        <h4>Service Desk Dashboard </h4>\n\n        <div class=\"z-depth-2 card col s5 m5 l3\" style=\"margin-top:50px;\">\n            <h5>Welcome, John!</h5><br>\n            <span>You have: </span><br><span style=\"color :green\">3 incidents submitted</span> <br>\n            <span style=\"color :blue\">{{tickets.length}} requests</span>\n        </div>\n        <div class=\"col s1 m1 l1\" style=\"margin-top:50px;\">\n\n        </div>\n        <div class=\"z-depth-2 card col s5 m5 l3 \" style=\"margin-top:50px; text-align: center;\">\n            <br>\n            <a>\n                <h4>New time off request</h4><br>\n            </a>\n\n        </div>\n        <div class=\"col s1 m1 l1\" style=\"margin-top:50px;\">\n        </div>\n        <div class=\"z-depth-2 card col s5 m5 l3 \" style=\"margin-top:50px; text-align: center;\">\n\n            <h4>Performance review</h4><br>\n            <a class=\"waves-effect waves-light btn left col s12 m12 l5  blue darken-3\" style=\"margin-right: 10px;\">Check\n                Previous</a>\n            <a class=\"waves-effect waves-light btn right col s12 m12 l5 blue darken-3\">Submit new</a>\n\n\n        </div>\n        <br><br><br><br><br><br>\n        <div class=\"row col s12 m12 l7 \" style=\"margin-top: 100px;\">\n\n            <blockquote>\n                Welcome to your Ticketing System. You can add new dashboards and configure your home screen by clicking\n                the plus button <br>\n                Please feel free to contact us at <a>support@ticketing@org</a> for any questions\n                <a class=\" row btn-floating btn-large waves-effect waves-light  button right red\"><i\n                        class=\"material-icons\">add</i></a>\n            </blockquote>\n        </div>\n    </div>\n\n</div>"

/***/ }),

/***/ "./projects/ticketing/src/app/components/dashboard/dashboard.component.ts":
/*!********************************************************************************!*\
  !*** ./projects/ticketing/src/app/components/dashboard/dashboard.component.ts ***!
  \********************************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_ticket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/ticket.service */ "./projects/ticketing/src/app/services/ticket.service.ts");



var DashboardComponent = /** @class */ (function () {
    function DashboardComponent(ticketService) {
        this.ticketService = ticketService;
    }
    DashboardComponent.prototype.ngOnInit = function () {
        this.tickets = this.ticketService.getAllTickets();
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./projects/ticketing/src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./projects/ticketing/src/app/components/dashboard/dashboard.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_ticket_service__WEBPACK_IMPORTED_MODULE_2__["TicketService"]])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./projects/ticketing/src/app/components/incidents/incidents.component.css":
/*!*********************************************************************************!*\
  !*** ./projects/ticketing/src/app/components/incidents/incidents.component.css ***!
  \*********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".brand-logo{\r\n    padding-left: 20px;\r\n  }\r\n  \r\n  \r\n  .full-body{\r\n    padding-top: 20px;\r\n    padding-left: 20px;\r\n    padding-right: 20px;\r\n    height:90%;\r\n    overflow: auto;\r\n  }\r\n  \r\n  \r\n  .card {\r\n    padding:20px;\r\n  \r\n  }\r\n  \r\n  \r\n  nav{\r\n      max-height:64px;\r\n  }\r\n  \r\n  \r\n  .row {\r\n    margin-bottom: 0px;\r\n  }\r\n  \r\n  \r\n  .side-nav {\r\n    margin-top: 65px;\r\n    width: 200px;\r\n}\r\n  \r\n  \r\n  li.active {\r\n  background-color: #ee6e73\r\n}\r\n  \r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL3RpY2tldGluZy9zcmMvYXBwL2NvbXBvbmVudHMvaW5jaWRlbnRzL2luY2lkZW50cy5jb21wb25lbnQuY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFBO0lBQ0ksa0JBQWtCO0VBQ3BCOzs7RUFHQTtJQUNFLGlCQUFpQjtJQUNqQixrQkFBa0I7SUFDbEIsbUJBQW1CO0lBQ25CLFVBQVU7SUFDVixjQUFjO0VBQ2hCOzs7RUFFQTtJQUNFLFlBQVk7O0VBRWQ7OztFQUVBO01BQ0ksZUFBZTtFQUNuQjs7O0VBQ0E7SUFDRSxrQkFBa0I7RUFDcEI7OztFQUdGO0lBQ0ksZ0JBQWdCO0lBQ2hCLFlBQVk7QUFDaEI7OztFQUVBO0VBQ0U7QUFDRiIsImZpbGUiOiJwcm9qZWN0cy90aWNrZXRpbmcvc3JjL2FwcC9jb21wb25lbnRzL2luY2lkZW50cy9pbmNpZGVudHMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIi5icmFuZC1sb2dve1xyXG4gICAgcGFkZGluZy1sZWZ0OiAyMHB4O1xyXG4gIH1cclxuICBcclxuICBcclxuICAuZnVsbC1ib2R5e1xyXG4gICAgcGFkZGluZy10b3A6IDIwcHg7XHJcbiAgICBwYWRkaW5nLWxlZnQ6IDIwcHg7XHJcbiAgICBwYWRkaW5nLXJpZ2h0OiAyMHB4O1xyXG4gICAgaGVpZ2h0OjkwJTtcclxuICAgIG92ZXJmbG93OiBhdXRvO1xyXG4gIH1cclxuICBcclxuICAuY2FyZCB7XHJcbiAgICBwYWRkaW5nOjIwcHg7XHJcbiAgXHJcbiAgfVxyXG4gIFxyXG4gIG5hdntcclxuICAgICAgbWF4LWhlaWdodDo2NHB4O1xyXG4gIH1cclxuICAucm93IHtcclxuICAgIG1hcmdpbi1ib3R0b206IDBweDtcclxuICB9XHJcblxyXG4gIFxyXG4uc2lkZS1uYXYge1xyXG4gICAgbWFyZ2luLXRvcDogNjVweDtcclxuICAgIHdpZHRoOiAyMDBweDtcclxufVxyXG5cclxubGkuYWN0aXZlIHtcclxuICBiYWNrZ3JvdW5kLWNvbG9yOiAjZWU2ZTczXHJcbn1cclxuICAiXX0= */"

/***/ }),

/***/ "./projects/ticketing/src/app/components/incidents/incidents.component.html":
/*!**********************************************************************************!*\
  !*** ./projects/ticketing/src/app/components/incidents/incidents.component.html ***!
  \**********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<nav>\n    <div class=\"nav-wrapper blue darken-4\">\n        <a class=\"brand-logo\">Your Ticketing System</a>\n\n    </div>\n</nav>\n\n<ul id=\"slide-out\" class=\"side-nav fixed\">\n    <li>\n        <a routerLink=\"\" routerLinkActive=\"active\"> Service Desk </a>\n    </li>\n    <li>\n        <a routerLink=\"../\" routerLinkActive=\"\">Incidents</a>\n    </li>\n    <li>\n        <a routerLink=\"../ServiceRequest\" routerLinkActive=\"active\">Service Request</a>\n    </li>\n    <li>\n        <a routerLink=\"\">Settings</a>\n    </li>\n\n</ul>\n<a materialize=\"sideNav\" [materializeParams]=\"[{edge:'left'}]\" href=\"#\" data-activates=\"slide-out\"\n    class=\"button-collapse show-on-large\"><i class=\"mdi-navigation-menu\"></i></a>\n\n\n\n<div class=\"full-body row \">\n    <div class=\"col s1 m1 l1\" style=\"margin:20px;\">\n    </div>\n    <div class=\"z-depth-2 card col s9 m9 l10\">\n        <h4>Your Service requests </h4>\n\n        <table class=\"striped\" style=\"margin-top: 50px;\">\n            <thead>\n                <tr>\n                    <th>ID</th>\n                    <th>Rquest </th>\n                    <th>employeeEmail</th>\n                    <th>Status</th>\n                    <th>More Details</th>\n                </tr>\n            </thead>\n            <tbody>\n                <tr *ngFor=\"let ticket of tickets ; let i=index\">\n                    <th> {{ticket.id}}</th>\n                    <th> {{ticket.item}}</th>\n                    <th> {{ticket.employeeEmail}}</th>\n                    <th> {{ticket.status}}</th>\n                    <th> <button class=\"btn small\" (click)=\"onMoreDetails(ticket)\">more details</button></th>\n                </tr>\n            </tbody>\n        </table>\n        <a class=\" row btn-floating btn-large waves-effect waves-light  button right red\" (click)=\"onNewTicket()\"><i\n                class=\"material-icons\">add</i></a>\n    </div>\n\n\n\n\n\n\n    <div id=\"modal2\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"newModalActions\">\n        <div class=\"modal-content\">\n            <div style=\"padding-bottom: 20px\">\n                <span style=\"font-weight:bold;font-size:24px;padding: 13px; \">Service request</span>\n\n            \n\n            </div>\n            \n        </div>\n    </div>\n\n    <div  id=\"modal1\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalActions\">\n        <div class=\"modal-content\">\n            <div style=\"padding-bottom: 20px\">\n                <span style=\"font-weight:bold;font-size:24px;padding: 13px; \">Service request</span>\n\n                <i class=\"material-icons right modal-action modal-close \" (click)=\"closeModal()\">close</i>\n\n            </div>\n            <div [formGroup]=\"inputForm\" class=\"row\">\n                <div class=\"col s12 m12 l12\">\n                    <div class=\"row col s12 m12 l12\">\n                        <div class=\"col s5 m5 l5\">\n                            <span style=\"font-weight: bold; padding-right: 15px;\">Ticket id</span> <span\n                                id=\"ticketId\">{{selectedTicket.id}}</span>\n                        </div>\n                        <div class=\"col s5 m5 l5\">\n                            <span style=\"font-weight: bold; padding-right: 15px;\">Due Date: </span> <span\n                                id=\"dueDate\">{{selectedTicket.dueDate}}</span>\n                        </div>\n                        <br><br>\n                        <div class=\"col s5 m5 l5\">\n                            <span style=\"font-weight: bold; padding-right: 15px;\">Item ordered: </span> <span\n                                id=\"item\">{{selectedTicket.item}}</span>\n                        </div>\n                        <div class=\"col s5 m5 l5\">\n                            <span style=\"font-weight: bold; padding-right: 15px;\">Quantity: </span> <span\n                                id=\"quantity\">{{selectedTicket.quantity}}</span>\n                        </div>\n                        <br><br>\n                        <div class=\"col s5 m5 l5\">\n                            <span style=\"font-weight: bold; padding-right: 15px;\">Employee: </span> <span\n                                id=\"employeeEmail\">{{selectedTicket.employeeEmail}}</span>\n                        </div>\n                        <div class=\"col s5 m5 l5\">\n                            <span style=\"font-weight: bold; padding-right: 15px;\">Description: </span> <span\n                                id=\"description\">{{selectedTicket.description}}</span>\n                        </div>\n\n                        <br><br><br>\n                        <div class=\"col s5 m5 l5\">\n                            <span style=\"font-weight: bold; padding-right: 15px;\">Ticket status: </span> <span\n                                id=\"status\">{{selectedTicket.status}}</span>\n                        </div>\n\n\n                    </div>\n                </div>\n            </div>\n        </div>\n    </div>\n</div>"

/***/ }),

/***/ "./projects/ticketing/src/app/components/incidents/incidents.component.ts":
/*!********************************************************************************!*\
  !*** ./projects/ticketing/src/app/components/incidents/incidents.component.ts ***!
  \********************************************************************************/
/*! exports provided: IncidentsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IncidentsComponent", function() { return IncidentsComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _services_ticket_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../../services/ticket.service */ "./projects/ticketing/src/app/services/ticket.service.ts");
/* harmony import */ var _models_ticket_model__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../models/ticket.model */ "./projects/ticketing/src/app/models/ticket.model.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");





var IncidentsComponent = /** @class */ (function () {
    function IncidentsComponent(ticketService) {
        this.ticketService = ticketService;
        this.modalActions = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.newModalActions = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    IncidentsComponent.prototype.ngOnInit = function () {
        this.newTicketID = Date.now();
        this.newTicket = new _models_ticket_model__WEBPACK_IMPORTED_MODULE_3__["TicketModel"](this.newTicketID, "", "", "", 0, "", "");
        this.tickets = this.ticketService.getAllTickets();
        this.inputForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormGroup"]({
            dueDate: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            status: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            item: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            quantity: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            employeeEmail: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
            description: new _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_4__["Validators"].required),
        });
    };
    IncidentsComponent.prototype.onMoreDetails = function (ticket) {
        this.selectedTicket = ticket;
        this.modalActions.emit({ action: 'modal', params: ['open'] });
    };
    IncidentsComponent.prototype.onNewTicket = function () {
        this.newTicketID = Date.now();
        //  this.newTicket= new TicketModel(this.newTicketID,"","","",0,"","")
        this.newModalActions.emit({ action: 'modal2', params: ['open'] });
    };
    IncidentsComponent.prototype.closeModal = function () {
        this.modalActions.emit({ action: 'modal', params: ['close'] });
    };
    IncidentsComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-incidents',
            template: __webpack_require__(/*! ./incidents.component.html */ "./projects/ticketing/src/app/components/incidents/incidents.component.html"),
            styles: [__webpack_require__(/*! ./incidents.component.css */ "./projects/ticketing/src/app/components/incidents/incidents.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_services_ticket_service__WEBPACK_IMPORTED_MODULE_2__["TicketService"]])
    ], IncidentsComponent);
    return IncidentsComponent;
}());



/***/ }),

/***/ "./projects/ticketing/src/app/components/login/login.component.css":
/*!*************************************************************************!*\
  !*** ./projects/ticketing/src/app/components/login/login.component.css ***!
  \*************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n.full-body{\r\n  height:100%;\r\n  overflow: auto;\r\n  -webkit-transform-origin: left bottom;\r\n          transform-origin: left bottom;\r\n  background-size:100% 100%;\r\n  background-size: cover;\r\n  background-repeat: no-repeat;\r\n}\r\n\r\n.loginForm {\r\n  /* margin-top:12%; */\r\n  padding:30px;\r\n  min-width:450px;\r\n}\r\n\r\n.row {\r\n  margin-bottom: 0px;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInByb2plY3RzL3RpY2tldGluZy9zcmMvYXBwL2NvbXBvbmVudHMvbG9naW4vbG9naW4uY29tcG9uZW50LmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQ0E7RUFDRSxXQUFXO0VBQ1gsY0FBYztFQUNkLHFDQUE2QjtVQUE3Qiw2QkFBNkI7RUFDN0IseUJBQXlCO0VBQ3pCLHNCQUFzQjtFQUN0Qiw0QkFBNEI7QUFDOUI7O0FBRUE7RUFDRSxvQkFBb0I7RUFDcEIsWUFBWTtFQUNaLGVBQWU7QUFDakI7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEIiLCJmaWxlIjoicHJvamVjdHMvdGlja2V0aW5nL3NyYy9hcHAvY29tcG9uZW50cy9sb2dpbi9sb2dpbi5jb21wb25lbnQuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiXHJcbi5mdWxsLWJvZHl7XHJcbiAgaGVpZ2h0OjEwMCU7XHJcbiAgb3ZlcmZsb3c6IGF1dG87XHJcbiAgdHJhbnNmb3JtLW9yaWdpbjogbGVmdCBib3R0b207XHJcbiAgYmFja2dyb3VuZC1zaXplOjEwMCUgMTAwJTtcclxuICBiYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIGJhY2tncm91bmQtcmVwZWF0OiBuby1yZXBlYXQ7XHJcbn1cclxuXHJcbi5sb2dpbkZvcm0ge1xyXG4gIC8qIG1hcmdpbi10b3A6MTIlOyAqL1xyXG4gIHBhZGRpbmc6MzBweDtcclxuICBtaW4td2lkdGg6NDUwcHg7XHJcbn1cclxuXHJcbi5yb3cge1xyXG4gIG1hcmdpbi1ib3R0b206IDBweDtcclxufVxyXG4iXX0= */"

/***/ }),

/***/ "./projects/ticketing/src/app/components/login/login.component.html":
/*!**************************************************************************!*\
  !*** ./projects/ticketing/src/app/components/login/login.component.html ***!
  \**************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"full-body valign-wrapper\">\r\n\r\n\r\n  <div class=\"center-align z-depth-3 row loginForm galaxy\">\r\n    <div class=\"row\">\r\n        <!-- <img src=\"/assets/images/logo.jpg\" style=\"width:7rem; vertical-align:bottom; \" class=\"center\" /> -->\r\n        <span style=\"font-weight: bold; font-size:20px;\" class=\"valign-wrapper\" >Your Ticketing System</span>\r\n    </div><br>\r\n\r\n    <div class=\"row\">\r\n      <div [formGroup]=\"inputForm\">\r\n        <div class=\"input-field\">\r\n          <i class=\"material-icons prefix\">email</i>\r\n          <input id=\"userName\" type=\"text\" class=\"validate\" class=\"validate\" formControlName=\"userName\">\r\n          <label for=\"userName\">User name</label>\r\n        </div>\r\n\r\n        <div class=\"input-field\">\r\n          <i class=\"material-icons prefix \">lock</i>\r\n          <input id=\"password\" type=\"password\" class=\"validate\" class=\"validate\" formControlName=\"password\">\r\n          <label for=\"password\">Password</label>\r\n        </div>\r\n\r\n        <div *ngIf=\"showError\" class=\"row\">\r\n          <span style=\"color:red\">incorrect credentials</span>\r\n        </div>\r\n\r\n        <button class=\"btn uiColorButtons btn-primary\" (click)=\"onSubmit()\">login </button>\r\n      </div>\r\n    </div>\r\n\r\n  </div>\r\n</div>\r\n"

/***/ }),

/***/ "./projects/ticketing/src/app/components/login/login.component.ts":
/*!************************************************************************!*\
  !*** ./projects/ticketing/src/app/components/login/login.component.ts ***!
  \************************************************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");




var LoginComponent = /** @class */ (function () {
    function LoginComponent(router) {
        this.router = router;
        this.showError = false;
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.inputForm = new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormGroup"]({
            userName: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required),
            password: new _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormControl"](null, _angular_forms__WEBPACK_IMPORTED_MODULE_3__["Validators"].required)
        });
    };
    LoginComponent.prototype.onSubmit = function () {
        var user = this.inputForm.get('userName').value;
        var pass = this.inputForm.get('password').value;
        if ((user === 'user') && (pass === 'user')) {
            this.showError = false;
            this.router.navigateByUrl('ticketing/dashboard');
        }
        else {
            this.showError = true;
        }
    };
    LoginComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./projects/ticketing/src/app/components/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./projects/ticketing/src/app/components/login/login.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["Router"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./projects/ticketing/src/app/models/ticket.model.ts":
/*!***********************************************************!*\
  !*** ./projects/ticketing/src/app/models/ticket.model.ts ***!
  \***********************************************************/
/*! exports provided: TicketModel */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketModel", function() { return TicketModel; });
var TicketModel = /** @class */ (function () {
    function TicketModel(id, dueDate, status, item, quantity, employeeEmail, description) {
        this.id = id;
        this.dueDate = dueDate;
        this.status = status;
        this.item = item;
        this.quantity = quantity;
        this.description = description;
        this.employeeEmail = employeeEmail;
    }
    return TicketModel;
}());



/***/ }),

/***/ "./projects/ticketing/src/app/services/ticket.service.ts":
/*!***************************************************************!*\
  !*** ./projects/ticketing/src/app/services/ticket.service.ts ***!
  \***************************************************************/
/*! exports provided: TicketService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TicketService", function() { return TicketService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _models_ticket_model__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../models/ticket.model */ "./projects/ticketing/src/app/models/ticket.model.ts");



var TicketService = /** @class */ (function () {
    function TicketService() {
        this.tickets = [];
        this.onInit();
    }
    TicketService.prototype.onInit = function () {
        this.tickets.push(new _models_ticket_model__WEBPACK_IMPORTED_MODULE_2__["TicketModel"]('01', "01/01/2019", "In Progress", "Laptop", 1, "john.doe@company.com", " request for Lenovo laptop"));
        this.tickets.push(new _models_ticket_model__WEBPACK_IMPORTED_MODULE_2__["TicketModel"]('02', "05/01/2020", "Approved", "Monitor", 1, "john.doe@company.com", " request for a new 24 inches Monitor"));
        this.tickets.push(new _models_ticket_model__WEBPACK_IMPORTED_MODULE_2__["TicketModel"]('03', "03/01/2020", "Approved", "Mouse", 1, "john.doe@company.com", " request for standard Mouse"));
        this.tickets.push(new _models_ticket_model__WEBPACK_IMPORTED_MODULE_2__["TicketModel"]('04', "05/22/2020", "Processed", "Laptop", 1, "jane.smith@company.com", " request for Lenovo laptop"));
    };
    TicketService.prototype.getAllTickets = function () {
        return this.tickets;
    };
    TicketService.prototype.addTicket = function (ticket) {
        this.tickets.push(ticket);
    };
    TicketService.prototype.getTicketById = function (ticketId) {
        return this.tickets.find(function (x) { return x.id === ticketId; });
    };
    TicketService = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])(),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], TicketService);
    return TicketService;
}());



/***/ }),

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");




var APP_ROUTES = [
    { path: '', component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"] },
    { path: 'dashboard', component: _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_3__["DashboardComponent"] },
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(APP_ROUTES)],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */"

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<router-outlet></router-outlet>\n\n"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'Portal';
    }
    AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
        })
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _projects_factoring_src_app_app_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../projects/factoring/src/app/app.module */ "./projects/factoring/src/app/app.module.ts");
/* harmony import */ var _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./components/dashboard/dashboard.component */ "./src/app/components/dashboard/dashboard.component.ts");
/* harmony import */ var _projects_creditRequest_src_app_app_module__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../../projects/creditRequest/src/app/app.module */ "./projects/creditRequest/src/app/app.module.ts");
/* harmony import */ var _projects_ticketing_src_app_app_module__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../../projects/ticketing/src/app/app.module */ "./projects/ticketing/src/app/app.module.ts");
/* harmony import */ var angular2_materialize__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! angular2-materialize */ "./node_modules/angular2-materialize/dist/index.js");










var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _components_dashboard_dashboard_component__WEBPACK_IMPORTED_MODULE_6__["DashboardComponent"],
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
                angular2_materialize__WEBPACK_IMPORTED_MODULE_9__["MaterializeModule"],
                _projects_factoring_src_app_app_module__WEBPACK_IMPORTED_MODULE_5__["App3SharedModule"].forRoot(),
                _projects_creditRequest_src_app_app_module__WEBPACK_IMPORTED_MODULE_7__["App4SharedModule"].forRoot(),
                _projects_ticketing_src_app_app_module__WEBPACK_IMPORTED_MODULE_8__["App5SharedModule"].forRoot()
            ],
            providers: [],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        })
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.css":
/*!**************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.css ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".full-body{\r\n  padding-top: 20px;\r\n  padding-bottom: 40px;\r\n  height:100%;\r\n  overflow: auto;\r\n}\r\n\r\n.mainForm {\r\n  padding:30px;\r\n\r\n  max-width:95%;\r\n}\r\n\r\n.row {\r\n  margin-bottom: 0px;\r\n}\r\n\r\na{\r\n  cursor: pointer;\r\n  color: black;\r\n}\r\n\r\n.card-image{\r\n  padding:5px;\r\n}\r\n\r\n.disabled{\r\n  color:grey;\r\n}\r\n\r\nul{\r\n  list-style-type: circle;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvY29tcG9uZW50cy9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBaUI7RUFDakIsb0JBQW9CO0VBQ3BCLFdBQVc7RUFDWCxjQUFjO0FBQ2hCOztBQUVBO0VBQ0UsWUFBWTs7RUFFWixhQUFhO0FBQ2Y7O0FBRUE7RUFDRSxrQkFBa0I7QUFDcEI7O0FBRUE7RUFDRSxlQUFlO0VBQ2YsWUFBWTtBQUNkOztBQUVBO0VBQ0UsV0FBVztBQUNiOztBQUVBO0VBQ0UsVUFBVTtBQUNaOztBQUVBO0VBQ0UsdUJBQXVCO0FBQ3pCIiwiZmlsZSI6InNyYy9hcHAvY29tcG9uZW50cy9kYXNoYm9hcmQvZGFzaGJvYXJkLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIuZnVsbC1ib2R5e1xyXG4gIHBhZGRpbmctdG9wOiAyMHB4O1xyXG4gIHBhZGRpbmctYm90dG9tOiA0MHB4O1xyXG4gIGhlaWdodDoxMDAlO1xyXG4gIG92ZXJmbG93OiBhdXRvO1xyXG59XHJcblxyXG4ubWFpbkZvcm0ge1xyXG4gIHBhZGRpbmc6MzBweDtcclxuXHJcbiAgbWF4LXdpZHRoOjk1JTtcclxufVxyXG5cclxuLnJvdyB7XHJcbiAgbWFyZ2luLWJvdHRvbTogMHB4O1xyXG59XHJcblxyXG5he1xyXG4gIGN1cnNvcjogcG9pbnRlcjtcclxuICBjb2xvcjogYmxhY2s7XHJcbn1cclxuXHJcbi5jYXJkLWltYWdle1xyXG4gIHBhZGRpbmc6NXB4O1xyXG59XHJcblxyXG4uZGlzYWJsZWR7XHJcbiAgY29sb3I6Z3JleTtcclxufVxyXG5cclxudWx7XHJcbiAgbGlzdC1zdHlsZS10eXBlOiBjaXJjbGU7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.html":
/*!***************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.html ***!
  \***************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"full-body\">\n  <div class=\"z-depth-2 row mainForm header\">\n    <div class=\"row\">\n      <h4 class=\"left\" style=\"font-weight: bold;\">RPA Challenges - internal use only</h4>\n      <div class=\"col s12 m12 l12\">\n        <div class=\"row\" style=\"padding-top: 20px !important;\">\n          <div class=\"col s12 m6 l3 \">\n            <div class=\"card\">\n              <div class=\"card-image\">\n                <img src=\"./../../../assets/images/dashboard/inputFields.PNG\">\n              </div>\n              <div class=\"card-content\">\n                <a href='http://rpachallenge.com/'>\n                  <span class=\"card-title\">Input Fields</span>\n                  <div style=\" font-size: 16px;   margin-top:10px; margin-bottom:10px;\">\n                    <span style=\"border: 2px solid grey; border-radius: 5px;padding:5px; \">UI Automation</span>\n                  </div>\n                  <p>Test your speed while entering data from an Excel file in an online form with a twist.</p>\n                </a>\n              </div>\n              <div class=\"card-action\">\n                <a href='https://uipath-my.sharepoint.com/:u:/p/mihai_andrei/EeMj4r0tLMtGi_6tn2S0IngB964YZbqLCVfA_cF_Uy_6VQ?e=f71btc'\n                  target=\"_blank\">\n                  <i class=\"material-icons\">cloud_download</i>\n                </a>\n                <a href=\"https://uipath-my.sharepoint.com/:v:/p/mihai_andrei/Ea835kwAqBdMmVadSvL_kXQBlepVdobJZurpTb6uaxBI-w?e=abdyFp\"\n                  target=\"_blank\">\n                  <i class=\"material-icons\">movie</i>\n                </a>\n              </div>\n            </div>\n          </div>\n          <div class=\"col s12 m6 l3\">\n            <div class=\"card\">\n              <div class=\"card-image\">\n                <img src=\"./../../../assets/images/dashboard/ocr.png\">\n              </div>\n              <div class=\"card-content\">\n                <a href=\"https://rpachallengeocr.azurewebsites.net/\">\n                  <span class=\"card-title\">Invoice Extraction</span>\n                  <div style=\" font-size: 16px;   margin-top:10px; margin-bottom:10px;\">\n                    <span\n                      style=\"border: 2px solid grey; border-radius: 5px;padding:5px; margin-right:10px;\">Digitization</span>\n                    <span style=\"border: 2px solid grey; border-radius: 5px;padding:5px; \">ABBYY</span>\n                  </div>\n\n                  <p>Extract information from various invoices by using OCR technologies.</p>\n                </a>\n              </div>\n              <div class=\"card-action\">\n                <a href='https://uipath-my.sharepoint.com/:u:/p/mihai_andrei/EY5lEs5N6kdDowg8QfxEjosBrnjvXQhRKeWK_gZSaraWkw?e=SaxifG'\n                  target=\"_blank\">\n                  <i class=\"material-icons\">cloud_download</i>\n                </a>\n                <a href=\"https://uipath.sharepoint.com/sites/Sales_migrated/Shared%20Documents/00.%20Business%20Development/RFx/Snap%20Inc_/POC/Recordings/RPA%20Challenge.mp4\"\n                  target=\"_blank\">\n                  <i class=\"material-icons disabled\">movie</i>\n                </a>\n              </div>\n            </div>\n          </div>\n          <div class=\"col s12 m6 l3\">\n            <div class=\"card\">\n              <div class=\"card-image\">\n                <img src=\"./../../../assets/images/dashboard/shortestPath.PNG\">\n              </div>\n              <div class=\"card-content\">\n                <a href=\"http://rpachallenge.com/assets/shortestPath/public/shortestpath.html\">\n                  <span class=\"card-title\">Shortest Path</span>\n                  <div style=\" font-size: 16px;   margin-top:10px; margin-bottom:10px;\">\n                    <span style=\"border: 2px solid grey; border-radius: 5px;padding:5px; margin-right:10px;\">UI\n                      Automation</span>\n                    <span style=\"border: 2px solid grey; border-radius: 5px;padding:5px; \">OCR</span>\n                  </div>\n                  <p>Find the shortest path in order to complete ship deliveries as fast as you can.</p>\n                </a>\n              </div>\n              <div class=\"card-action\">\n                <a href='https://uipath-my.sharepoint.com/:u:/p/mihai_andrei/Edpkpw7uhfpMjM-FYoi3I0gBSRe8KUzPgsYJ7J0KBgAWkg?e=c1CVhH'\n                  target='_blank'>\n                  <i class=\"material-icons\">cloud_download</i></a>\n                <a href='https://uipath-my.sharepoint.com/:v:/p/mihai_andrei/EZAmbX3W-wxKhS_ZAWHVkKMBDmqfIPb7RlP6EC4fD14-WA?e=R9wI5D'\n                  target='_blank'><i class=\"material-icons \">movie</i></a>\n                <a (click)=\"openModalShortestPath()\"><i class=\"material-icons\">feedback</i></a>\n\n              </div>\n            </div>\n          </div>\n          <div class=\"col s12 m6 l3\">\n            <div class=\"card\">\n              <div class=\"card-image\">\n                <img src=\"./../../../assets/images/dashboard/movieSearch.PNG\">\n              </div>\n              <div class=\"card-content\">\n                <a href='http://rpachallenge.com/movieSearch'>\n                  <span class=\"card-title\">Movie Search</span>\n                  <div style=\" font-size: 16px;   margin-top:10px; margin-bottom:10px;\">\n                    <span style=\"border: 2px solid grey; border-radius: 5px;padding:5px; margin-right:10px;\">UI\n                      Automation</span>\n                    <span\n                      style=\"border: 2px solid grey; border-radius: 5px;padding:5px; margin-right:10px;\">Python</span>\n                    <div class=\"hide-on-med-and-up row\"><br></div>\n                    <span style=\"border: 2px solid grey; border-radius: 5px;padding:5px; \">ML</span>\n\n                  </div>\n                  <p>Rate user's reviews for different movies and check which ones are good and which are bad.</p>\n                </a>\n              </div>\n              <div class=\"card-action\">\n                <a target=\"_blank\"\n                  href=\"https://uipath-my.sharepoint.com/:u:/p/mihai_andrei/ET-BA2QuhENKjj-I4Gw_5w8B8q4EMuMfiruL3BrFh4xwBg?e=7dycIz\">\n                  <i class=\"material-icons\">cloud_download</i></a>\n                <a target=\"_blank\"\n                  href=\"https://uipath-my.sharepoint.com/:v:/p/mihai_andrei/Ee5gbeKpNstBrOYBXSOYh2cBDNH_PhnlKZ68nO2s6ktmkQ?e=QcxPgV\">\n                  <i class=\"material-icons\">movie</i></a>\n                <a (click)=\"openModalMovie()\"><i class=\"material-icons\">feedback</i></a>\n\n              </div>\n            </div>\n          </div>\n        </div>\n      </div>\n      <div class=\"row\" style=\"padding-top:20px !important;\"></div>\n      <div class=\"col s12\">\n        <h4 class=\"left\">Banking Solutions</h4>\n      </div>\n      <div class=\"col s12 m12 l12\" style=\"padding-top: 20px !important;\">\n        <div class=\"col s3\">\n          <div class=\"card\">\n            <a [routerLink]=\"['/creditRequest']\">\n              <div class=\"card-image\">\n                <img src=\"./../../../assets/images/dashboard/creditRequest.PNG\">\n              </div>\n              <div class=\"card-content\">\n                <span class=\"card-title\">Credit Request</span>\n                <p>Make new credit requests or connect as a bank agent and approve the requests.</p>\n              </div>\n            </a>\n            <div class=\"card-action\">\n              <a href='https://uipath-my.sharepoint.com/:u:/r/personal/mihai_andrei_uipath_com/Documents/RPA%20Portal/Credit%20Request/CreditRequest.zip?csf=1&e=mIJAYS'\n                target='_blank'><i class=\"material-icons\">cloud_download</i></a>\n              <a><i class=\"material-icons\">movie</i></a>\n              <a (click)=\"openModalCreditRequest()\"><i class=\"material-icons\">feedback</i></a>\n\n            </div>\n          </div>\n\n        </div>\n\n        <div class=\"col s3\">\n          <div class=\"card\">\n            <a [routerLink]=\"['/ticketing']\">\n              <div class=\"card-image\">\n                <img src=\"./../../../assets/images/dashboard/ticketing.png\">\n              </div>\n              <div class=\"card-content\">\n                <span class=\"card-title\">Ticketing Application</span>\n                <p>Create new Service Requests in a ticketing system that will then be replicated in Service Now.</p>\n              </div>\n            </a>\n            <div class=\"card-action\">\n              <a href='https://uipath-my.sharepoint.com/:u:/p/mihai_andrei/Ee2-uetLpB1BoMDdtU5BXXkBWDkpkNL_y1pnrGNuusri3Q?e=u4PFeL'\n                target='_blank'><i class=\"material-icons\">cloud_download</i></a>\n              <a href=\"https://uipath-my.sharepoint.com/:v:/p/mihai_andrei/ESR_nsVkVCVHi4xaFyZ4aE4Bte3Yv2jmZILfafvejpkxSA?e=VUg0pw\"\n              target=\"_blank\"><i class=\"material-icons\">movie</i></a>\n              <a ><i class=\"material-icons disabled\">feedback</i></a>\n\n            </div>\n          </div>\n\n        </div>\n\n        <div class=\"col s3\">\n\n          <div class=\"card\">\n            <a [routerLink]=\"['/factoring']\">\n              <div class=\"card-image\">\n                <img src=\"./../../../assets/images/dashboard/factoring.PNG\">\n              </div>\n              <div class=\"card-content\">\n                <span class=\"card-title\">Factoring</span>\n                <p>Upload new invoices to be financed and check if the company meets the expectations. </p>\n              </div>\n            </a>\n            <div class=\"card-action\">\n              <a href=\"https://uipath-my.sharepoint.com/:f:/r/personal/mihai_andrei_uipath_com/Documents/RPA%20Portal/Factoring/UI_Factoring?csf=1&e=MZrJcD\"\n                target=\"_blank\"><i class=\"material-icons\">cloud_download</i></a>\n              <a target=\"_blank\"\n                href=\"https://uipath-my.sharepoint.com/:v:/r/personal/mihai_andrei_uipath_com/Documents/RPA%20Portal/Factoring/factoring_video.mp4?csf=1&e=YwPyRy\"><i\n                  class=\"material-icons\">movie</i></a>\n              <a (click)=\"openModalFactoring()\"><i class=\"material-icons\">feedback</i></a>\n            </div>\n          </div>\n        </div>\n      </div>\n    </div>\n  </div>\n</div>\n\n<div id=\"modalMovie\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalActionsMovie\">\n  <div class=\"modal-content\">\n    <div style=\"padding-bottom: 20px\">\n      <span style=\"font-weight:bold;font-size:24px;padding: 13px; \">Implement Movie Search</span>\n      <i class=\"material-icons right modal-action modal-close \" (click)=\"closeModalMovie()\">close</i>\n    </div>\n    <div class=\"row\">\n      <span>!!! Changes need to be made in order to run it under personal machinge. Work in progress !!!</span><br>\n      <span>In order to run the Movie Search process you will need to have the following things installed:\n        <ul>\n          <li>\n            <a href=\"https://www.python.org/downloads/release/python-360/\" target=\"_blank\">\n              - <span style=\"text-decoration: underline !important;\">Python 3.5 x64</span> </a>\n          </li>\n          <li>\n            - install the necessary dependencies by running the below .bat file\n          <li>- depending on your Environment Configuration:</li>\n          <li><b>a) First case (without Visual Studio installation)</b><br>\n            install conda: https://repo.anaconda.com/archive/Anaconda3-2019.07-MacOSX-x86_64.pkg<br><br>\n\n            from pytorch website, download the appropiate version of pytorch: https://pytorch.org/<br>\n\n            to test that the installation of all dependencies was performed correctly, run in a cmd python\n            main.py<br><br>\n\n            the output should be:<br>\n            C:\\Users\\uipathadmin\\Desktop\\sentiment\\SentimentAnalysis>python main.py<br>\n            <pre>\n        Switch model from GPU to CPU\n        WordLSTM1\n        0 0.814129\n        Name: predict, dtype: float32</pre><br>\n          </li>\n          <li>\n            <b>b) Second Case (Visual Studio installed):</b><br>\n            select appropriate version of pytorch: <br>\n            pip install torch-version (version number here).whl<br>\n            pip install torchvision<br>\n          </li>\n        </ul>\n        <div class=\"row\">\n          <a class=\"btn teal\" style=\"color:white;\"\n            href=\"https://uipath-my.sharepoint.com/:u:/r/personal/mihai_andrei_uipath_com/Documents/RPA%20Portal/RPA%20Challenge%20-%20Movie%20Search/installDependenciesMovieSearch.bat?csf=1&e=oN2DrI\"\n            target=\"_blank\">\n            Download dependencies</a>\n        </div>\n      </span>\n    </div>\n  </div>\n</div>\n\n\n<div id=\"modalShortestPath\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalShortestPath\">\n  <div class=\"modal-content\">\n    <div style=\"padding-bottom: 20px\">\n      <span style=\"font-weight:bold;font-size:24px;padding: 13px; \">Implement Shortest Path</span>\n      <i class=\"material-icons right modal-action modal-close \" (click)=\"closeModalMovie()\">close</i>\n    </div>\n    <div class=\"row\">\n      <span>The attached workflow has been developed to run on Internet Explorer with a zoom of 110%. <br>\n        This is an attended use case, so you will need to have the page open on the Shortest Path website.\n        The process must be working in the background before pressing on Start.\n        <br><br>\n        As the process is using OCR, you might have to retake the images for the Find relative Image activites in the\n        Get and Enter Data Workflow.\n      </span>\n    </div>\n  </div>\n</div>\n\n\n\n<div id=\"creditRequest\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalActionsCreditRequest\">\n  <div class=\"modal-content\">\n    <div style=\"padding-bottom: 20px\">\n      <span style=\"font-weight:bold;font-size:24px;padding: 13px; \">Implement Credit Request workFlow</span>\n      <i class=\"material-icons right modal-action modal-close \" (click)=\"closeModalMovie()\">close</i>\n    </div>\n    <div class=\"row\">\n      <span>\n        The Credit Request workflow was built to showcase some of the tasks a bank agent does in the front office.\n        In this workflow you can access only a credit option (Mortgage). <br>\n        From there you can do the following tasks:\n\n        <ul>\n          <li>\n            - search for users. For the sake of the Demo, only one user exists: John Doe.\n          </li>\n          <li>\n            - update the user's data. The changed data will be saved locally and will restore after session refresh\n          </li>\n          <li>\n            - ask for a new credit request.\n          </li>\n        </ul>\n        This Showcase is made to integrate with a Druid Chatbot. You can personalize and run the\n        workflow on your machine, but you will need to connect the Druid Platform to your orchestrator and to your\n        robot. You can see a showcase of the integration on this the platform. The workflow will run on the below VM\n        <ul>\n          <li>\n            Druid Platform: https://www.druidplatform.com/\n          </li>\n          <li></li>\n          <li style=\"padding-left:10px\">\n            account: demo1\n          </li>\n          <li style=\"padding-left:10px\">\n            password: Druid12345!\n          </li>\n        </ul>\n\n        Please make sure to check the attached video in order to understand the demo capabilities.<br>\n        <div class=\"row\">\n          <a class=\"btn blue\" href=\"./../../../assets/downloadFiles/DruidVM.rdp\" target=\"_blank\">Download RDP\n            Connection</a>\n        </div>\n      </span>\n    </div>\n  </div>\n</div>\n\n\n<div id=\"modalFactoring\" class=\"modal\" materialize=\"modal\" [materializeActions]=\"modalActionsFactoring\">\n  <div class=\"modal-content\">\n    <div style=\"padding-bottom: 20px\">\n      <span style=\"font-weight:bold;font-size:24px;padding: 13px; \">Implement Invoice Factoring</span>\n      <i class=\"material-icons right modal-action modal-close \" (click)=\"closeModalMovie()\">close</i>\n    </div>\n    <div class=\"row\">\n      <span>\n        The purpose of this app is to showcase a invoice factoring application.\n        <br>You can connect as a company (user/user) or as a bank (bank/bank).\n        <br><br> As a user, you can check your current requests and upload new invoices that need to be processed.\n        <br><br> As a bank you can finance the user's requests taking in consideration some factors.\n        <ul>\n          <li>\n            - the invoice cannot be a duplicate\n          </li>\n          <li>\n            - for each company and buyer you will have a specific limit under which you can allow the requested invoice\n          </li>\n\n          <li>\n            - you can finance only 85% of the requested amount\n          </li>\n\n          <li>\n            - the company cannot be on a fraud list\n          </li>\n        </ul>\n\n      </span>\n    </div>\n  </div>\n</div>"

/***/ }),

/***/ "./src/app/components/dashboard/dashboard.component.ts":
/*!*************************************************************!*\
  !*** ./src/app/components/dashboard/dashboard.component.ts ***!
  \*************************************************************/
/*! exports provided: DashboardComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DashboardComponent", function() { return DashboardComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");


var DashboardComponent = /** @class */ (function () {
    function DashboardComponent() {
        this.modalActionsMovie = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.modalActionsCreditRequest = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.modalActionsFactoring = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
        this.modalShortestPath = new _angular_core__WEBPACK_IMPORTED_MODULE_1__["EventEmitter"]();
    }
    DashboardComponent.prototype.ngOnInit = function () {
    };
    DashboardComponent.prototype.openModalMovie = function () {
        this.modalActionsMovie.emit({ action: 'modal', params: ['open'] });
    };
    DashboardComponent.prototype.closeModalMovie = function () {
        this.modalActionsMovie.emit({ action: 'modal', params: ['close'] });
    };
    DashboardComponent.prototype.openModalShortestPath = function () {
        this.modalShortestPath.emit({ action: 'modal', params: ['open'] });
    };
    DashboardComponent.prototype.closeModalShortestPath = function () {
        this.modalShortestPath.emit({ action: 'modal', params: ['close'] });
    };
    DashboardComponent.prototype.openModalCreditRequest = function () {
        this.modalActionsCreditRequest.emit({ action: 'modal', params: ['open'] });
    };
    DashboardComponent.prototype.closeModalCreditRequest = function () {
        this.modalActionsCreditRequest.emit({ action: 'modal', params: ['close'] });
    };
    DashboardComponent.prototype.openModalFactoring = function () {
        console.log('test');
        this.modalActionsFactoring.emit({ action: 'modal', params: ['open'] });
    };
    DashboardComponent.prototype.closeModalFactoring = function () {
        this.modalActionsFactoring.emit({ action: 'modal', params: ['close'] });
    };
    DashboardComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
            selector: 'app-dashboard',
            template: __webpack_require__(/*! ./dashboard.component.html */ "./src/app/components/dashboard/dashboard.component.html"),
            styles: [__webpack_require__(/*! ./dashboard.component.css */ "./src/app/components/dashboard/dashboard.component.css")]
        }),
        tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
    ], DashboardComponent);
    return DashboardComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.error(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\mihai.andrei\OneDrive - UiPath\UiPath\projects\_INTERNAL_RPAPortal\Portal\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map