import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';

import { DashboardComponent } from './components/dashboard/dashboard.component';
import { IncidentsComponent } from './components/incidents/incidents.component';


const APP_ROUTES: Routes = [
  { path: 'ticketing/login', component: LoginComponent },
  { path: 'ticketing', component: LoginComponent },
  { path: 'ticketing/dashboard', component: DashboardComponent },
  { path: 'ticketing/ServiceRequest', component: IncidentsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(APP_ROUTES)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
