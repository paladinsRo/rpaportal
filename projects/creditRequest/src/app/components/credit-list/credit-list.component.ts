import { Component, OnInit, EventEmitter } from '@angular/core';
import { ServiceCredit } from './../../services/credit.service';
import { CreditModel } from './../../models/credit.model';
import { CreditOfferModel } from './../../models/creditOffer.model';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MaterializeAction } from 'angular2-materialize';


@Component({
  selector: 'app-credit-list',
  templateUrl: './credit-list.component.html',
  styleUrls: ['./credit-list.component.css']
})
export class CreditListComponent implements OnInit {

  creditList: CreditModel[];
  selectedCredit: CreditModel = new CreditModel('', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '');
  creditOffer: CreditOfferModel = new CreditOfferModel('200', '10', '5', '150');
  constructor(private srvCredit: ServiceCredit) { };
  modalActionsOffer = new EventEmitter<string | MaterializeAction>();

  inputForm: FormGroup;
  offerForm: FormGroup;

  ngOnInit() {

    this.srvCredit.getCreditsList().then(result => {
      this.creditList = result;
    });
    console.log(this.creditList)
    this.inputForm = new FormGroup({
      analysisFee: new FormControl(null, Validators.required),
      insuranceRate: new FormControl(null, Validators.required),
      dae: new FormControl(null, Validators.required),
      otherFees: new FormControl(null, Validators.required)
    });

    this.offerForm = new FormGroup({
      analysisFee: new FormControl(null, Validators.required),
      insuranceRate: new FormControl(null, Validators.required),
      dae: new FormControl(null, Validators.required),
      otherFees: new FormControl(null, Validators.required),
      monthlyPayment: new FormControl(null, Validators.required),
      totalPaymentAmount: new FormControl(null, Validators.required),
      totalReceivedAmount: new FormControl(null, Validators.required),
    });

  }

  acceptCredit(credit) {
    credit.status = 'Offer accepted';
    this.srvCredit.updateCreditLocal(credit);
    const idx = this.creditList.findIndex(x => x.creditId === credit.creditId);
    this.creditList[idx].status = 'Offer accepted';
  }

  deleteCredit(credit) {
    this.srvCredit.deleteCreditLocal(credit);
    this.creditList= this.srvCredit.getCreditsStorage();
  }

  openModalOffer(credit) {
    this.selectedCredit = credit;
    this.selectedCredit.dae = this.creditOffer.dae;
    this.selectedCredit.insuranceRate = this.creditOffer.insuranceRate;
    this.selectedCredit.dae = this.creditOffer.dae;
    this.selectedCredit.otherFees = this.creditOffer.otherFees;
    this.selectedCredit.analysisFee = this.creditOffer.analysisFee;

    this.selectedCredit.totalReceivedAmount = Math.round((parseInt(this.selectedCredit.totalSum) - parseInt(this.selectedCredit.analysisFee) - parseInt(this.selectedCredit.otherFees))).toString();

    let mRate: any = Math.round((parseInt(this.selectedCredit.totalSum) / (parseInt(this.selectedCredit.period) * 12)));
    const commision = Math.round(mRate * (parseInt(this.selectedCredit.dae) / 100));
    this.selectedCredit.monthlyPayment = mRate;
    this.selectedCredit.totalPaymentAmount = Math.round(((parseInt(mRate) + commision) * (parseInt(this.selectedCredit.period) * 12) + parseInt(this.selectedCredit.analysisFee) + parseInt(this.selectedCredit.otherFees))).toString();

    this.modalActionsOffer.emit({ action: 'modal', params: ['open'] });
  }

  closeModalOffer() {
    this.modalActionsOffer.emit({ action: 'modal', params: ['close'] });
  }

  createOffer() {

    this.selectedCredit.analysisFee = this.offerForm.controls['analysisFee'].value;
    this.selectedCredit.dae = this.offerForm.controls['dae'].value;
    this.selectedCredit.otherFees = this.offerForm.controls['otherFees'].value;
    this.selectedCredit.insuranceRate = this.offerForm.controls['insuranceRate'].value;
    this.selectedCredit.monthlyPayment = this.offerForm.controls['monthlyPayment'].value;
    this.selectedCredit.totalPaymentAmount = this.offerForm.controls['totalPaymentAmount'].value;
    this.selectedCredit.totalReceivedAmount = this.offerForm.controls['totalReceivedAmount'].value;

    this.selectedCredit.status = 'Offer made';

    //this.srvCredit.updateCredit(this.selectedCredit);

    const idx = this.creditList.findIndex(x => x.creditId === this.selectedCredit.creditId);
    this.creditList[idx].status = 'Offer made';
    this.closeModalOffer();


  }
}
