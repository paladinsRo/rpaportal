import { Component, OnInit } from '@angular/core';
import { UserModel } from './../../../models/user.model';
import { CompanyModel } from './../../../models/companies.model';
import { ServiceLogin } from './../../../services/login.service';
import { ServiceCompanies } from './../../../services/companies.service';
import { FormGroup, FormControl, Validators, } from '@angular/forms';

@Component({
  selector: 'app-admin-home',
  templateUrl: './admin-home.component.html',
  styleUrls: ['./admin-home.component.scss']
})
export class AdminHomeComponent implements OnInit {

  appTitle: string= "Admin Control Room";
  user: UserModel;
  companyList: CompanyModel [];
  public inputForm: FormGroup;


  constructor(
    private srvLogin: ServiceLogin,
    private srvCompany: ServiceCompanies) { }

  ngOnInit() {
    this.user = this.srvLogin.getUser();
    this.srvCompany.getAllCompanies();
    this.inputForm = new FormGroup({
      userName: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
      companyName: new FormControl(null, Validators.required),
      
    });

  }

  disconnectUser() {
    this.srvLogin.disconnectUser();
  }

}
