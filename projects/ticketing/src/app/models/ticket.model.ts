
export class TicketModel {
    id: string;
    dueDate: string;
    status: string;
    item: string;
    quantity: Number;
    description: string;
    employeeEmail: string;
   


    constructor(
        id: string,
        dueDate: string,
        status: string,
        item:string,
        quantity: Number,
        employeeEmail : string,
        description: string,
       
       
    ) {
        this.id = id;
        this.dueDate = dueDate;
        this.status = status;
        this.item = item;
        this.quantity =quantity;
        this.description = description;

        this.employeeEmail = employeeEmail;
    }
}
