export class UserModel {
  _id:string;
  userName: string;
  password: string;
  companyId: string;
  type: string;

  constructor(
    userName: string,
    password: string,
    companyId: string,
    type: string
  ) {
    this.userName = userName;
    this.password = password;
    this.companyId = companyId;
    this.type = type;
  }
}

