import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ClientComponent } from './components/client/client.component';
import { CreditIpotecarComponent } from './components/credit-ipotecar/credit-ipotecar.component';
import { CreditListComponent } from './components/credit-list/credit-list.component';

const routes: Routes = [
  { path: 'creditRequest/client', component: ClientComponent },
  { path: 'creditRequest', component: ClientComponent },
  { path: 'creditRequest/ipotecar', component: CreditIpotecarComponent },
  { path: 'creditRequest/creditList', component: CreditListComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
