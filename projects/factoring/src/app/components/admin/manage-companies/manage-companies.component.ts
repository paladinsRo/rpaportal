import { Component, OnInit } from '@angular/core';
import { ServiceCompanies } from './../../../services/companies.service';
import { CompanyModel } from './../../../models/companies.model';
import { EventEmitter } from '@angular/core';
import { MaterializeAction } from 'angular2-materialize';
import { FormGroup, FormControl, Validators, } from '@angular/forms';
import { UserModel } from '../../../models/user.model';

@Component({
  selector: 'app-manage-companies',
  templateUrl: './manage-companies.component.html',
  styleUrls: ['./manage-companies.component.scss']
})
export class ManageCompaniesComponent implements OnInit {

  companies: CompanyModel[];
  modalActions = new EventEmitter<string | MaterializeAction>();

  updateCompanyActions = new EventEmitter<string | MaterializeAction>();
  selectedCompany: CompanyModel = new CompanyModel('','','','','','');
  labBank: Boolean = false;
  password = '';
  confirmPassword = '';
  companyExists = false;
  public inputCompanyForm: FormGroup;
  public inputUserForm: FormGroup;
  fileUploaded: Boolean = false;
  fileToUpload: File;
  logoImage: String='https://uipath509.westeurope.cloudapp.azure.com/api/v1/getLogo?logoName=';

  constructor(
    private srvCompanies: ServiceCompanies
  ) { }

  ngOnInit() {
    this.getAllCompanies();

    this.inputCompanyForm = new FormGroup({
      companyName: new FormControl(null, Validators.required),
      companyLogo: new FormControl(null, Validators.required),
      companyLimit: new FormControl(null, Validators.required),
      buyerRisk: new FormControl(null, Validators.required),
      bankId: new FormControl(null, Validators.required),
    });

    this.inputUserForm = new FormGroup({
      userName: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    });
  }

  getAllCompanies() {
    this.srvCompanies.getAllCompanies().then(result => {
      this.companies = result;
    });
    
  }

  openCompany(company: CompanyModel) {
    this.selectedCompany=company;
    this.fileUploaded = false;

    this.updateCompanyActions.emit({ action: 'modal', params: ['open'] });
  }

  closeModalUpdate(){
    this.updateCompanyActions.emit({ action: 'modal', params: ['close'] })
  }


  openModal() {
    this.labBank = false;
    this.inputCompanyForm.reset();
    this.fileUploaded=false;
    this.modalActions.emit({ action: 'modal', params: ['open'] });
  }

  closeModal() {
    this.modalActions.emit({ action: 'modal', params: ['close'] });

  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
    this.fileUploaded = true;
   
  }

  createCompany() {
    const compName=this.inputCompanyForm.controls['companyName'].value;

    if (this.companies.filter(x => x.companyName == compName).length > 0) {
      this.companyExists = true;
    } else {
      this.companyExists = false;
      var compLogo='default.png';

      if(this.fileUploaded)
        compLogo=this.fileToUpload.name;
      
      this.srvCompanies.postLogo(this.fileToUpload);
      let bankId=0;
      if(this.labBank){
        bankId=1;
      }

      this.srvCompanies.postCompany(
        new CompanyModel(
          compName,
          '',
          compLogo,
          this.inputCompanyForm.controls['companyLimit'].value,
          this.inputCompanyForm.controls['buyerRisk'].value,
          bankId.toString(),
        )
      ).then(res => {
        this.getAllCompanies();
        this.inputCompanyForm.reset();
        this.closeModal();
        this.fileUploaded=false;

        this.srvCompanies.getCompanyByName(compName).then((res:CompanyModel) => { 
          const user:UserModel= new UserModel(
            compName,
            compName,
              res._id,
              'client',
              );
              console.log(user)
            this.srvCompanies.postUser(user);
        })
      });
     
    }
  }

  deleteCompany(){
    this.srvCompanies.deleteCompany(this.selectedCompany);
    this.getAllCompanies();
  }

  updateCompany(){

    if(this.fileUploaded)
     {
       const compLogo=this.fileToUpload.name;
       this.selectedCompany.companyLogo=compLogo;
       this.srvCompanies.postLogo(this.fileToUpload);
     }
     this.closeModalUpdate();
    this.srvCompanies.updateCompany(this.selectedCompany);

  }
}


