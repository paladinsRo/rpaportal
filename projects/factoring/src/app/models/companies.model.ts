export class CompanyModel {
  _id: string;
  companyName: string;
  companyLogo: string;
  companyLimit: string;
  buyerRisk: string;
  bankId: string;

  constructor(
    companyName: string,
    _id: string,
    companyLogo: string,
    companyLimit: string,
    buyerRisk: string,
    bankId: string,
  ) {
    this._id = _id;
    this.companyName = companyName;
    this.companyLogo = companyLogo;
    this.companyLimit = companyLimit;
    this.buyerRisk = buyerRisk;
    this.bankId = bankId;
  }
}
