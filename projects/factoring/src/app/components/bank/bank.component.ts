import { Component, OnInit, EventEmitter, ɵConsole } from '@angular/core';
import { ServiceInvoices } from './../../services/invoices.service';
import { InvoiceModel } from './../../models/invoice.model';
import { ServiceCompanies } from './../../services/companies.service';
import { MaterializeAction } from 'angular2-materialize';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CompanyModel } from './../../models/companies.model';
import { ServiceLogin } from '../../services/login.service';
import { UserModel } from '../../models/user.model';

@Component({
  selector: 'app-bank',
  templateUrl: './bank.component.html',
  styleUrls: ['./bank.component.scss']
})
export class BankComponent implements OnInit {

  clientType = 'bank';
  invoices: InvoiceModel[];
  serverInvoices: InvoiceModel[];
  searchStr: String = '';
  showError: Boolean = false;
  pendingNo: Number;
  financedNo: Number;
  forApproval: Number;
  refusedNo: Number;
  reimbursedNo: Number;
  companyNo: Number;
  modalActions = new EventEmitter<string | MaterializeAction>();
  inputForm: FormGroup;
  selectedInvoice: InvoiceModel = new InvoiceModel('', '', '', '', '', '', '', '', '', '', '', false, false, false, '');
  companyLimit: String;
  appTitle: String = 'Your Banking Solution';
  companies: CompanyModel[];
  allCompanies: CompanyModel[];
  logoImage: String = 'https://uipath509.westeurope.cloudapp.azure.com/api/v1/getLogo?logoName=';
  user: UserModel;
  selectedCompanyName : String;

  constructor(
    private srvInvoices: ServiceInvoices,
    private srvCompany: ServiceCompanies,
    private srvLogin: ServiceLogin) { }

  ngOnInit() {
    this.user = this.srvLogin.getUser();

    this.srvCompany.getCompaniesByBank(this.user.companyId).then((res: CompanyModel[]) => {
      this.allCompanies = res;
      this.companies = this.allCompanies;
      this.getAllInvoices();
    }
    );


    this.inputForm = new FormGroup({
      invoiceNo: new FormControl(null, Validators.required),
      buyerName: new FormControl(null, Validators.required),
      sellerName: new FormControl(null, Validators.required),
      invoiceAmount: new FormControl(null, Validators.required),
      financedAmount: new FormControl(null, Validators.required),
      comment: new FormControl(null, Validators.required),
      invoiceDate: new FormControl(null, Validators.required),
    });
  }

  searchCompany(searchCompanyValue) {
    this.selectedCompanyName = searchCompanyValue;
    this.invoices = this.invoices.filter(x => x.companyName === this.selectedCompanyName)
    this.searchStr = '';
    //this.companies = this.allCompanies;
    this.getInvoicesStatus();
    this.companies = this.companies.filter(x => x.companyName === searchCompanyValue)
  }

  getAllInvoices() {
    let filteredInvoices: InvoiceModel[] = [new InvoiceModel('', '', '', '', '', '', '', '', '', '', '', false, false, false, '')];
    this.srvInvoices.getAllInvoices()
      .then((result: InvoiceModel[]) => {

        for (let i = 0; i < this.allCompanies.length; i++) {
          filteredInvoices=filteredInvoices.concat(result.filter((x: InvoiceModel) => 
            x.companyName === this.allCompanies[i].companyName
          ));
        }
        if(filteredInvoices[0].companyName.length<1)
          filteredInvoices.splice(0,1);

        this.serverInvoices = filteredInvoices
        this.invoices = this.serverInvoices;
        this.companies = this.allCompanies;
        this.getInvoicesStatus();
        this.searchStr = '';
      });

  }

  sortInvoiceList(sortStatus: String) {
    this.invoices=this.serverInvoices;
    if(this.selectedCompanyName)
      this.invoices = this.invoices.filter(x => x.companyName === this.selectedCompanyName)
    
    this.invoices = this.invoices.filter(x => x.status === sortStatus);
  }

  getInvoicesStatus() {
    this.pendingNo = this.serverInvoices.filter(res => res.status === 'Pending').length;
    this.financedNo = this.serverInvoices.filter(res => res.status === 'Financed').length;
    this.refusedNo = this.serverInvoices.filter(res => res.status === 'Refused').length;
    this.reimbursedNo = this.serverInvoices.filter(res => res.status === 'Reimbursed').length;
    this.forApproval = this.serverInvoices.filter(res => res.status === 'ForApproval').length;


  }

  editInvoice(invoice: InvoiceModel) {
    this.srvCompany.getCompanyById(invoice.companyId).then((res: CompanyModel) => {
      this.companyLimit = res.companyLimit;
      this.modalActions.emit({ action: 'modal', params: ['open'] });
      this.selectedInvoice = invoice;
    });

  }

  closeModal() {
    this.modalActions.emit({ action: 'modal', params: ['close'] });
    this.getInvoicesStatus();
  }


  submitOffer() {
    this.selectedInvoice.status = 'Financed';
    this.srvInvoices.updateInvoice(this.selectedInvoice);
    this.closeModal();
  }

  saveChanges() {

    this.srvInvoices.updateInvoice(this.selectedInvoice);
    this.closeModal();

  }

  reimbursedOFfer(invoice) {
    invoice.status = 'Reimbursed';
    this.srvInvoices.updateInvoice(invoice);    this.getInvoicesStatus();

  }

  cancelOffer() {
    this.selectedInvoice.status = 'Refused';
    this.srvInvoices.updateInvoice(this.selectedInvoice);
    this.closeModal();
  }

  sentToApproval() {
    this.selectedInvoice.status = 'ForApproval';
    this.srvInvoices.updateInvoice(this.selectedInvoice);
    this.closeModal();
  }

 


}
