import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { HttpParams } from "@angular/common/http";
import { ClientModel } from "../models/client.model";

@Injectable()
export class ServiceClient {
  urlClient ="https://uipath509.westeurope.cloudapp.azure.com/api/v1/credit_client/";

  client: ClientModel;
  constructor(private http: HttpClient) {}

  public setClient(client) {
    this.client = client;
  }

  public getClient(lastName) {
    const params = new HttpParams().set("lastName", lastName);
    return this.http.get(this.urlClient, { params });
  }

  public getSavedClient(){
    return this.client;
  }

  public updateClient(client) {
    console.log(client)
    const params = new HttpParams().set("id", client._id);
    return this.http.put(this.urlClient,
      {
        lastName: client.lastName,
        firstName: client.firstName,
        city :  client.city,
        address: client.address,
        phone: client.phone,
        email: client.email,
        taxIdNo: client.taxIdNo,
        serialNo: client.serialNo,
        validity: client.validity,
        accountRon: client.accountRon,
        accountEur: client.accountEur,
        maximumAllowed: client.maximumAllowed
      }, {  params }
    ).toPromise()
    .then(resp => {
      return resp;
    })
    .catch(this.handleError);
  }


  private handleError(error: any): Promise<any> {
    console.error("An error occurred", error);
    return Promise.reject(error.message || error);
  }
}
