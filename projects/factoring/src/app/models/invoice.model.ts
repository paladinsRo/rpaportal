export class InvoiceModel {
  _id: string;
  companyId: string;
  companyName:string;
  sellerName:string;
  buyerName:string;
  requestDate: string;
  invoiceDate: string;
  invoiceNo: string;
  invoiceAmount: string;
  financedAmount: string;
  status: string;
  comment: string;
  buyerRisk: Boolean;
  creditBureau: Boolean;
  duplicity: Boolean;
  invoiceFile: string;

  constructor(
    companyId: string,
    companyName:string,
    sellerName:string,
    buyerName:string,
    requestDate: string,
    invoiceDate: string,
    invoiceNo: string,
    invoiceAmount: string,
    financedAmount: string,
    status: string,
    comment: string,
    buyerRisk: Boolean,
    creditBureau: Boolean,
    duplicity: Boolean,
    invoiceFile: string
  ) {

    this.companyId = companyId;
    this.companyName = companyName;
    this.sellerName = sellerName;
    this.buyerName = buyerName;
    this.requestDate = requestDate;
    this.invoiceDate = invoiceDate;
    this.invoiceNo = invoiceNo;
    this.invoiceAmount = invoiceAmount;
    this.financedAmount = financedAmount;
    this.status = status;
    this.comment = comment;
    this.buyerRisk = buyerRisk;
    this.creditBureau = creditBureau;
    this.duplicity = duplicity;
    this.invoiceFile = invoiceFile;
  }
}
