import { Component, OnInit, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ServiceLogin } from './../../services/login.service';
import { Observable } from 'rxjs';
import { UserModel } from './../../models/user.model';
import { Router } from '@angular/router';
import { MaterializeAction } from 'angular2-materialize';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public inputForm: FormGroup;
  user: UserModel;
  showError: Boolean = false;
  modalActions = new EventEmitter<string | MaterializeAction>();
  password='';
  confirmPassword='';

  constructor(
    private loginSrv: ServiceLogin,
    private router: Router) { }

  ngOnInit() {
    this.inputForm = new FormGroup({
      companyName: new FormControl(null, Validators.required),
      userName: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required)
    });
  }

  onSubmit() {
    const user = this.inputForm.get('userName').value;
    const pass = this.inputForm.get('password').value;

    this.loginSrv.checkUser(user, pass).subscribe((res: UserModel) => {
      this.user = res[0];
      if (this.user) {
        this.loginSrv.setUser(this.user);
        this.router.navigateByUrl('factoring/'+this.user.type);
      } else {
        this.showError = true;
        this.inputForm.reset();
      }
    });
  }

  openModal() {
    this.modalActions.emit({ action: 'modal', params: ['open'] });
  }
  closeModal() {
    this.modalActions.emit({ action: 'modal', params: ['close'] });

  }

}
