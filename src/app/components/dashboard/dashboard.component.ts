import { Component, OnInit, EventEmitter } from '@angular/core';
import { MaterializeAction } from 'angular2-materialize';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor() { }

  modalActionsMovie = new EventEmitter<string | MaterializeAction>();
  modalActionsCreditRequest = new EventEmitter<string | MaterializeAction>();
  modalActionsFactoring = new EventEmitter<string | MaterializeAction>();
  modalShortestPath = new EventEmitter<string | MaterializeAction>();

  ngOnInit() {
  }

  openModalMovie() {
    this.modalActionsMovie.emit({ action: 'modal', params: ['open'] });
  }

  closeModalMovie() {
    this.modalActionsMovie.emit({ action: 'modal', params: ['close'] });
  }

  openModalShortestPath() {
    this.modalShortestPath.emit({ action: 'modal', params: ['open'] });
  }

  closeModalShortestPath() {
    this.modalShortestPath.emit({ action: 'modal', params: ['close'] });
  }


  openModalCreditRequest() {
    this.modalActionsCreditRequest.emit({ action: 'modal', params: ['open'] });
  }

  closeModalCreditRequest() {
    this.modalActionsCreditRequest.emit({ action: 'modal', params: ['close'] });
  }

  openModalFactoring() {
    console.log('test')
    this.modalActionsFactoring.emit({ action: 'modal', params: ['open'] });
  }

  closeModalFactoring() {
    this.modalActionsFactoring.emit({ action: 'modal', params: ['close'] });
  }
}
