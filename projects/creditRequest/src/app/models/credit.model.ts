export class CreditModel {
  _id: string;
  creditId: string;
  clientName:string;
  income: string;
  personsNo: string;
  incomeType: string;
  taxes: string;
  period: string;
  totalSum: string;
  status: string;
  analysisFee: string;
  currencyCode: string;
  insuranceRate: string;
  dae: string;
  monthlyPayment: string;
  otherFees: string;
  totalPaymentAmount: string;
  totalReceivedAmount: string;


  constructor(
    _id: string,
    creditId: string,
    clientName,
    income: string,
    personsNo: string,
    incomeType: string,
    taxes: string,
    analysisFee: string,
    currencyCode: string,
    insuranceRate: string,
    dae: string,
    monthlyPayment: string,
    otherFees: string,
    period: string,
    quotationId: string,
    totalPaymentAmount: string,
    totalReceivedAmount: string,
    status: string,

  ) {
    this._id = _id;
    this.creditId = creditId;
    this.clientName = clientName;
    this.income = income;
    this.personsNo = personsNo;
    this.incomeType = incomeType;
    this.taxes = taxes;
    this.period = period;
    this.totalSum = this.totalSum;
    this.status = this.status;
    this.analysisFee = analysisFee;
    this.currencyCode = currencyCode;
    this.insuranceRate = insuranceRate;
    this.dae = dae;
    this.monthlyPayment = monthlyPayment;
    this.otherFees = otherFees;
    this.totalPaymentAmount = totalPaymentAmount;
    this.totalReceivedAmount = totalReceivedAmount;

  }
}

