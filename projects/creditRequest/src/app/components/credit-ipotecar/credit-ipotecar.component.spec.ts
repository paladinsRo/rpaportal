import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreditIpotecarComponent } from './credit-ipotecar.component';

describe('CreditIpotecarComponent', () => {
  let component: CreditIpotecarComponent;
  let fixture: ComponentFixture<CreditIpotecarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreditIpotecarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreditIpotecarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
