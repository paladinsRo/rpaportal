import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Jsonp } from '@angular/http';
import { CompanyModel } from '../models/companies.model';
import { UserModel } from '../models/user.model';

@Injectable()
export class ServiceCompanies {
  url = 'https://uipath509.westeurope.cloudapp.azure.com:443/api/v1/';
  headers = new HttpHeaders({
    'Content-Type': 'application/json'
  });
  public company: CompanyModel;

  constructor(private http: HttpClient, private _jsonp: Jsonp) { }

  public getCompaniesByBank(bankId) {
    const params = new HttpParams()
      .set('bankId', bankId);
    return this.http
      .get(this.url+'invoices_companies_bank/', { params })
      .toPromise()
      .then((resp : CompanyModel[]) => resp)
      .catch(this.handleError);
  }

  public getAllCompanies() {
    return this.http
      .get(this.url+'invoices_companies/')
      .toPromise()
      .then((resp : CompanyModel[]) => resp)
      .catch(this.handleError);
  }

  public getCompanyById(companyId) {
    const params = new HttpParams()
      .set('_id', companyId);
    return this.http.get(this.url+'/invoices_company', { params })
      .toPromise()
      .then(resp => {
        return resp;
      })
      .catch(this.handleError);
  }


  public getCompanyByName(companyName:string) {
    const params = new HttpParams()
      .set('companyName', companyName);
    return this.http.get(this.url+'invoices_companyName', { params })
      .toPromise()
      .then(resp => {
        return resp;
      })
      .catch(this.handleError);
  }

  public getCompany() {
    if (this.company) {
      return this.company;
    } else {
      localStorage.setItem("currentCompany", JSON.stringify(this.company));
      let company: CompanyModel = JSON.parse(localStorage.getItem("currentCompany"));
      return company;
    }
  }

  public setComapny(company) {
    this.company = company;
    localStorage.setItem("currentCompany", JSON.stringify(this.company));

  }

  public deleteCompany(company: CompanyModel){
    const params = new HttpParams().set("_id", company._id);
    return this.http.delete(this.url+'invoices_companies',{  params }
    ).toPromise()
    .then(resp => {
      return resp;
    })
  }

  public tempCreateUser(company: CompanyModel){
    const comp= this.getCompanyByName(company.companyName);
    console.log(comp)
  }

  public postCompany(company: CompanyModel) {
    if(company.companyLogo.length<1)
      company.companyLogo='genericLogo';
    return this.http.post(this.url+'invoices_companies/', {
      companyName: company.companyName,
      companyLogo: company.companyLogo,
      companyLimit: company.companyLimit,
      buyerRisk: company.buyerRisk,
      bankId: company.bankId
    }).toPromise()
      .then(resp => {
        return resp;
      })
      .catch(this.handleError);
  }

  
  public postUser(user: UserModel) {
    return this.http.post(this.url+'invoices_users/', {
      userName: user.userName,
      password: user.password,
      companyId: user.companyId,
      type: user.type 
    }).toPromise()
      .then(resp => {
        return resp;
      })
      .catch(this.handleError);
  }

  
  postLogo(fileToUpload: File) {
    const endpoint = this.url+'uploadLogo';
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);

    return this.http
      .post(endpoint, formData)
      .toPromise()
      .then(resp => {
      });
}

  private handleError(error: any): Promise<any> {
    console.error('An error occurred', error);
    return Promise.reject(error.message || error);
  }

  public updateCompany(company:CompanyModel) {
    console.log(company)
    const params = new HttpParams().set("id", company._id);
    return this.http.put(this.url+'invoices_company',
      {
        companyName: company.companyName,
        companyLogo: company.companyLogo,
        companyLimit :  company.companyLimit,
        buyerRisk: company.buyerRisk,
        bankId : company.bankId
        
      }, {  params }
    ).toPromise()
    .then(resp => {
      return resp;
    })
    .catch(this.handleError);
  }
}
