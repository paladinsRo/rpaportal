import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { JsonpModule } from '@angular/http';
import { MaterializeModule } from 'angular2-materialize';
import { ServiceCompanies } from './services/companies.service';
import { ServiceInvoices } from './services/invoices.service';
import { ServiceLogin } from './services/login.service';
import { LoginComponent } from './components/login/login.component';
import { ClientHomeComponent } from './components/client/client-home/client-home.component';
import { AdminHomeComponent } from './components/admin/admin-home/admin-home.component';
import { ManageCompaniesComponent } from './components/admin/manage-companies/manage-companies.component';
import { BankComponent } from './components/bank/bank.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { InvoicesListComponent } from './components/client/invoices-list/invoices-list.component';


import 'materialize-css';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ClientHomeComponent,
    AdminHomeComponent,
    ManageCompaniesComponent,
    BankComponent,
    NavBarComponent,
    InvoicesListComponent,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    JsonpModule,
    ReactiveFormsModule,
    MaterializeModule
  ],
  providers: [
    ServiceCompanies,
    ServiceInvoices,
    ServiceLogin
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

@NgModule({})
export class App3SharedModule{
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: AppModule,
      providers: [
        ServiceCompanies,
        ServiceInvoices,
        ServiceLogin
      ]
    };
  }
}
