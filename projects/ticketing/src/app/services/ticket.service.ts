import { Injectable } from '@angular/core';
import { TicketModel } from '../models/ticket.model';

@Injectable()
export class TicketService {

    public tickets: TicketModel[] =[];

    constructor()
    {
        this.onInit();
    }


    onInit(){
        this.tickets.push(new TicketModel('01', "01/01/2019","In Progress", "Laptop", 1, "john.doe@company.com", " request for Lenovo laptop"));
        this.tickets.push(new TicketModel('02', "05/01/2020","Approved", "Monitor", 1, "john.doe@company.com", " request for a new 24 inches Monitor"));
        this.tickets.push(new TicketModel('03', "03/01/2020","Approved", "Mouse", 1,"john.doe@company.com", " request for standard Mouse" ));
        this.tickets.push(new TicketModel('04', "05/22/2020","Processed", "Laptop", 1, "jane.smith@company.com", " request for Lenovo laptop"));
      }

      getAllTickets(){
          return this.tickets;
      }

      addTicket(ticket:TicketModel){
          this.tickets.push(ticket);
      }

      getTicketById(ticketId){
        return this.tickets.find(x => x.id === ticketId);
      }
}