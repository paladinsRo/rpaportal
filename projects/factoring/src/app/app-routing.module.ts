import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { ClientHomeComponent } from './components/client/client-home/client-home.component';
import { AdminHomeComponent } from './components/admin/admin-home/admin-home.component';
import { BankComponent } from './components/bank/bank.component';

const routes: Routes = [
{ path: 'factoring/login', component: LoginComponent },
{ path: 'factoring', component: LoginComponent },
{ path: 'factoring/client', component: ClientHomeComponent },
{ path: 'factoring/admin', component: AdminHomeComponent },
{ path: 'factoring/bank', component: BankComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
