import { Component, OnInit, Input } from '@angular/core';
import { ServiceLogin } from './../../services/login.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {
  @Input() title: String;
  @Input() type: String;

  constructor(
    private srvLogin: ServiceLogin,
    private router: Router) { }

  ngOnInit() {
    console.log(this.title)
  }

  goToProfile(){
    if(this.type === 'bank'){
      console.log(this.type)
      this.router.navigateByUrl('bankProfile');
    }
  }

  disconnectUser() {
    this.srvLogin.disconnectUser();
    this.router.navigate(['..']);
  }



}
