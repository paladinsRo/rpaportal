import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Jsonp } from "@angular/http";
import { HttpParams } from "@angular/common/http";
import { UserModel } from "../models/user.model";
import { CompanyModel } from '../models/companies.model';
import { ServiceCompanies } from './companies.service';

@Injectable()
export class ServiceLogin {
  url = "https://uipath509.westeurope.cloudapp.azure.com:443/api/v1/";
  user: UserModel;

  constructor(
    private http: HttpClient,
    private _jsonp: Jsonp,
    private srvCompany: ServiceCompanies) {}

  public checkUser(userName, password) {
    const headers = new HttpHeaders()
    const params = new HttpParams()
      .set("userName", userName)
      .set("password", password);
    return this.http.get(this.url +"users/", { params });
  }

  public setUser(user) {
    this.user = user;
    localStorage.setItem("currentUser", JSON.stringify(this.user));
  }

  public getUser() {
    if (this.user) {
      return this.user;
    } else {
      let user : UserModel =JSON.parse(localStorage.getItem("currentUser"));
      return user;
    }
  }


  public disconnectUser() {
   localStorage.removeItem('currentUser');
  }



  private handleError(error: any): Promise<any> {
    console.error("An error occurred", error);
    return Promise.reject(error.message || error);
  }
}
